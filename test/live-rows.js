const Row = require( '../tool/core/lib/row/Row' ),
 groupExe = require( '../tool/core/lib/regex/patternGroup' ),
 exeChain = require( '../tool/core/lib/bin/exechain/exechain' ),
  OTHER_1 = require( '../tool/core/lib/regex/patterns/other/_other1' ),
  OTHER_2 = require( '../tool/core/lib/regex/patterns/other/_other2' ),
    POSTC = require( '../tool/core/lib/regex/patterns/postalcode/_postalcode' ),
 GOptions = require( '../tool/cli/lib/bin/options/globalOptions' ),
  statsProcessing = require('../tool/core/lib/stat/Stats-Processing');

const Mutator = require( '../tool/core/lib/regex/classes/Mutators' );

//! If you have your own dataset, change the path below.
//! Note: It can accept .JSON/.js. Do not add the extension.
const testRows = require( './data/test-set' );

let ROWS = [],
  REPORT = [],
   STATS = [],
     RAW = [];

// const Cleaner = require('../tool/core/src/regex/classes/Cleaner');
// const Extractor = require('../tool/core/src/regex/classes/Extractor');
// const Expander = require('../tool/core/src/regex/classes/Expander');
// const XRegExp = require('xregexp');

const opts = new GOptions( testRows, { caps: false } ),
         f = opts.fields;

for ( let row of testRows ) {

  row = new Row( opts, row );

  Mutator.Caser = new Mutator.Casers( opts.caps );

  //! NOTE THAT RR NEEDS TO COME BEFORE UNIT/SUITE
  groupExe(POSTC, row, 'POSTAL_CODE');

  for ( let g of exeChain ) {
    groupExe( g, row, [ f.StreetAddress, f.AddressLine2 ] );
  }

  groupExe( OTHER_1, row, f.StreetAddress );
  groupExe( OTHER_2, row, f.AddressLine2 );

  row.appendShadows( f.StreetAddress, [ 'Street' ] );

  if ( row.shadows.includes( 'RR' ) ) {
    if (row.valueOf( f.StreetAddress ) == '')
      row.appendShadows( f.StreetAddress, [ 'RR' ]);
    else
      row.appendShadows( f.AddressLine2, [ 'RR' ] );
  }

  row.appendShadows(
    f.AddressLine2,
    [
      'Lot',
      'Concession',
      'Unit',
      'Suite',
      'Building',
      'Section',
      'Wing',
      'Level',
      'Floor',
      'Room',
      'Postal_Box',
      'Postal_Bag',
      'Code_Postal',
      'Reservation',
      'Island',
      'Other_1',
      'Other_2'
    ]
  );

  Mutator.Caser.exec
  (
    row,
    [
      f.Name,
      f.City
    ]
  );

  //! DEBUGGING DISPLAY: EACH RAW ROW
  // console.log(row);

  if ( row.hasChange )
    REPORT.push( row.summary.toString );

  ROWS
    .push( row.data );
  RAW
    .push( row );

//! DEBUGGING DISPLAYS
//~> [00] Display: Row Stats
// console.log( row.stats.toString() );

//~> [01] Display: Operation list per changed field
  /* // Get array of keys for the object:
  Object.keys(
    // Current row's stats -> Fields
    row.stats.fields
  )
  // Filter on...
  .filter(
    // Whether a field has a change property as non-zero
    field => row.stats.fields[ field ].change
  )
  // For each of the filtered keys..
  .forEach( field => {
    // Log its operation list
    console.log( row.stats.fields[field].operations )
  }); */

  STATS
    .push( row.stats );


}

let lastRowIndex = ROWS.length - 1;

//! DEBUGGING DISPLAYS
//~> [02] Final Row Data
  // /*First*/ console.log( ROWS[0] );
  /*Last*/  console.log( ROWS[lastRowIndex] );

//~> [02.1] RAW ROW
  // /*First*/ console.log( RAW[0] );
  /*Last*/  console.log( RAW[lastRowIndex] );

//~> [03] Raw History (Street)
//~~> You can change the target to any field (shadow or otherwise by changing the name)
  // /*First*/ console.log( RAW[0].getTarget('Street') );
  // /*Last*/  console.log( RAW[lastRowIndex].getTarget('Street') );

//~> [04] Stats (All Fields)
  // /*First*/ console.log( STATS[0] );
  // /*Last*/  console.log( STATS[lastRowIndex] );

//~> [05] Stats (Only fields with an operation list)
  // /*First*/ console.log( STATS[0].fields.filter(v=>v.hasOwnProperty('operations')));
  // /*Last*/  console.log( STATS[lastRowIndex].fields.filter( v => v.hasOwnProperty('operations') ));

//~> [06] Row Operation List
  // /*First*/ console.log( STATS[0].operations );
  // /*Last*/  console.log( STATS[lastRowIndex].operations );

//~> [07] Row Class-Operation List
  // /*First*/ console.log( STATS[0].classes );
  // /*Last*/  console.log( STATS[lastRowIndex].classes )

//~> [08] Row Report
  // /*First*/ console.log( REPORT[0] );
  // /*Last*/  console.log( REPORT[lastRowIndex] );

//~> [09] Street Shadow
  // /*First*/ console.log( RAW[0].shadow.Street );
  // /*Last*/  console.log( RAW[lastRowIndex].shadow.Street );

//~> [10] Final ADDRESS_LINE_1 Value
//~~> If that's not what the field is named, you need to change it
  // /*First*/ console.log( ROWS[0].ADDRESS_LINE_1 );
  // /*Last*/  console.log( ROWS[lastRowIndex].ADDRESS_LINE_1 );

//~> [11] Processed Stats Debug
  // let processedStats = statsProcessing(STATS);
  // console.log( processedStats.field.category );
  // console.log( processedStats.fieldCategory );
  // console.log( processedStats.field.name.COMMUNITY );
  // console.log( processedStats.operation.class.Cleaner );
  // console.log( processedStats.field.name.Unit.operations );