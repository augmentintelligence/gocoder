module.exports = function( grunt ) {

  const browsersOpt = { browsers: [ "last 2 versions", "> 0.5%", "ie 11" ] };
  require( "load-grunt-tasks" )( grunt );

  grunt.initConfig({
    less: {
      dev: {
        options: {
          compress: true,
          plugins: [
            new ( require( "less-plugin-autoprefix" ) )( browsersOpt ),
            new ( require( "less-plugin-clean-css"  ) )( "" )
          ]
        },
        files: {
          "dist/css/app/interface.css": [ "src/less/components/interface/*.less", "src/less/components/interface/**/*.less" ],
               "dist/css/app/main.css": "src/less/global/*.less",
              "dist/css/app/login.css": "src/less/components/login/*.less"
        }
      }
    },
    postcss: {
      options: {
        map: true,
        processors: [
          // fallbacks for rem units
          require( "pixrem" )(),
          // vendor prefixes
          require( "autoprefixer" )( browsersOpt ),
          // css babel
          require( "postcss-preset-env" )( browsersOpt ),
          // normalizer
          require( "postcss-normalize" )( browsersOpt ),
          // minify
          require( "cssnano" )()
        ]
      },
      dist: {
        src: "dist/css/**/*.css"
      },
      dev: {
        src: [
          "dist/css/app/main.css",
          "dist/css/app/login.css",
          "dist/css/app/interface.css"
        ]
      }
    },
    concat: {
      options: {
        separator: ";\n"
      },
      vendor: {
        src: [
          "vendor/js/ajaxify/ajaxify.min.js",
        ],
        dest: "dist/js/vendor/vendor.js"
      }
    },
    browserify: {
      vendor: {
        files: {
          "dist/js/vendor/vendor.js": [
            "vendor/js/vendor.js",
            "vendor/js/ajaxify/ajaxify.min.js",
            "vendor/js/history/history.js",
            "vendor/js/jquery/scrollto/jquery-scrollto.min.js"
          ]
        },
        options: {
          browserifyOptions: { debug: false },
          transform: [
            [
              "babelify",
              {
                "presets": [ "@babel/preset-env" ]
              }
            ]
          ],
          plugin: [
            [
              "minifyify",
              {
                map: false
              }
            ]
          ],
        }
      },
      dev: {
        files: {
          "dist/js/app/interface.js": "src/js/interface/interface.js"
        },
        options: {
          browserifyOptions: {
            debug: true
          },
          transform: [
            [
              "babelify",
              {
                "presets": [ "@babel/preset-env" ]
              }
            ]
          ],
        }
      },
      dist: {
        files: {
          "dist/js/app/interface.js": "src/js/interface/interface.js"
        },
        options: {
          browserifyOptions: { debug: false },
          transform: [
            [
              "babelify",
              {
                "presets": [ "@babel/preset-env" ]
              }
            ]
          ],
          plugin: [
            [
              "minifyify",
              {
                map: false
              }
            ]
          ],
        }
      }
    },
    watch: {
      dev: {
        files: [ "src/less/**/*.less", "src/js/**/*.js" ],
        tasks: [ "dev" ]
      },
      dev_css: {
        files: [ "src/less/**/*.less" ],
        tasks: [ "dev_css" ]
      },
      dev_js: {
        files: [ "src/js/**/*.js" ],
        tasks: [ "dev_js" ]
      }
    }
  });
  grunt.registerTask("dist", [
    "less:dev",
    "postcss:dist",
    "browserify:dist"
  ]);
  grunt.registerTask("dev", [
    "less:dev",
    "postcss:dev",
    "browserify:dev"
  ]);
  // grunt.option('stack', true);
  grunt.registerTask("dev_css", [ "less:dev" ]);
  grunt.registerTask("dev_js",  [ "browserify:dev" ]);
  grunt.registerTask("default", [ "dev" ]);
};
