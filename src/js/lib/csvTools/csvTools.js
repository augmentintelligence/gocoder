(function() {

  // Local Dependencies
  const arr = require('../arrayTools/arrayTools');

  // Custom Errors
  const EInvalidSyntax = class EInvalidSyntax extends Error {},
  EInsufficientColumns = class EInsufficientColumns extends Error {},
     EDuplicateColumns = class EDuplicateColumns extends Error {};

  function csvValidate( csv ) {

    // Get Column Names
    let csvCols = csvColumns( csv );

    // If csvColumns doesn't return an array, something is up.
    if ( !Array.isArray( csvCols ) )
      return new EInvalidSyntax('Invalid CSV Syntax');

    // If there aren't at least 4 columns (required mapping)
    if ( csvCols.length < 4 )
      return new EInsufficientColumns('Insufficient Columns');

    // If there are duplicate columns
    if ( arr.unique( csvCols ).length !== csvCols.length )
      return new EDuplicateColumns('File Contains Duplicate Columns');

    //!ToDo: do some extra checks with the csvCols array?
    return csvCols;
  }

  function csvColumns(csv) {
    // Get position of first line ending
    let posLineEnd = csv.indexOf('\n') !== -1
      ? csv.indexOf('\n')
      : csv.indexOf('\r');
    return !posLineEnd
      ? false
      : csv
          // Get the first line
          .substr(0, posLineEnd)
          // Split values by Comma
          .split(',')
          // Alter each and return the array
          .map(c =>
            c
              // Trim it
              .trim()
              // Replace quotes with nothing
              .replace(/['"]/g, '')
          );
  }

  module.exports = {
    csvValidate: csvValidate,
     csvColumns: csvColumns,
          error: {
      EInsufficientColumns: EInsufficientColumns,
            EInvalidSyntax: EInvalidSyntax,
         EDuplicateColumns: EDuplicateColumns
    }
  };

}).call(this);