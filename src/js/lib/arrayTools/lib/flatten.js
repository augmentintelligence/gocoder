(function () {

  /**
   * Given an array, if there are any nested
   * arrays, flattens it by bringing all
   * elements to the top-level.
   *
   * @param {*[]} array -
   *   The array (or array of arrays) to force
   *   into a single depth-level.
   *
   * @returns {*[]} -
   *   A single depth-level array of values
   *   containing all the values of the original.
   */
  const flatten = function (array) {
    return array.reduce((acc, val) => Array.isArray(val) ?
      acc.concat(flatten(val)) : acc.concat(val), []);
  };

  module.exports = flatten;

}).call(this);