const flatten = require('./flatten');

function unique( ...items ) {
  items = flatten( items );
  let list = new Set();
  for ( let item of items ) {
    list.add( item );
  }
  return Array.from( list );
}

module.exports = unique;