module.exports = {
  unique: require('./lib/unique'),
  flatten: require('./lib/flatten')
};