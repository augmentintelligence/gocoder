(function() {

  let exports = {};

  // Test for known HTML5 Elements
  const HTMLELMS = new RegExp(/(?:^a(?:bbr|cronym|ddress|pplet|r(?:ea|ticle)|side|udio)?$)|(?:^b(?:ase(?:font)?|d[io]|ig|lockquote|ody|r|utton)?$)|(?:^c(?:a(?:nvas|ption)|enter|ite|o(?:de|l(?:group)?))$)|(?:^d(?:ata(?:list)?|d|e(?:l|tails)|fn|i(?:alog|[rv])|[lt])$)|(?:^em(?:bed)?$)|(?:^f(?:i(?:eldset|g(?:caption|ure))|o(?:nt|oter|rm)|rame(?:set)?)$)|(?:^h(?:[1-6]|ead(?:er)?|r|tml)$)|(?:^i(?:frame|mg|n(?:put|s))?$)|(?:^kbd$)|(?:^l(?:abel|egend|i(?:nk)?)$)|(?:^m(?:a(?:in|p|rk)|et(?:a|er))$)|(?:^n(?:av|o(?:frames|script))$)|(?:^o(?:bject|l|pt(?:group|ion)|utput)$)|(?:^p(?:aram|icture|r(?:e|ogress))?$)|(?:^q$)|(?:^r(?:[pt]|uby)$)|(?:^s(?:amp|cript|e(?:ction|lect)|mall|ource|pan|t(?:r(?:ike|ong)|yle)|u(?:[bp]|mmary)|vg)?$)|(?:^t(?:able|body|[dt]|e(?:mplate|xtarea)|foot|h(?:ead)?|i(?:me|tle)|r(?:ack)?)$)|(?:^ul?$)|(?:^v(?:ar|ideo)$)|(?:^wbr$)/);

  // Custom Error Types
  exports.e = {};
  class ElementNotFound extends Error {}
  exports.e.ElementNotFound = ElementNotFound;

  class NodeListNotFound extends Error {}
  exports.e.NodeListNotFound = NodeListNotFound;

  /**
   * Given a `NodeList`, `HTMLElement`, or a `string`
   * representing either of those after being passed to
   * `querySelector`; coerces it to a single`HTMLElement`,
   * and returns the result of a `callback` being passed that
   * `HTMLElement`.
   *
   * If an `HTMLElement` or `NodeList` cannot be coerced;
   * returns the result of a `callback` being passed a
   * `ElementNotFound` error.
   *
   * @param {NodeList|HTMLElement|string} e -
   *  A `NodeList`, `HTMLElement`, or a `string` that would
   *  produce those after being passed to `querySelectorAll`.
   *
   * @param {function} callback -
   *  A callback function that uses the result of this
   *  function's coersion. Is passed two arguments; `error`
   *  and `result`, following Node.js conventions.
   *
   * @note If a string is passed as `e`, and it is not a
   * known HTML5 element, and does not begin with `.` or
   * `#` - a `#` will be prepended.
   *
   * @returns {void|*}
   */
  function HTMElement ( e, callback ) {
    let str = e;
    // If e is a known HTMLElement, or begins with #/.
    // pass as-is to querySelector. Otherwise, prepend a '#'.
    e = typeof e === 'string'
      ? document.querySelector( ( HTMLELMS.test( e ) || e.charAt() === '#' || e.charAt() === '.' ? e : '#' + e ) )
      : e;
    // If e is a NodeList, convert it to an array,
    // and set it to the first element.
    if ( e instanceof NodeList ) e = nodeListToArray( e )[0];
    return e instanceof HTMLElement
      ? callback( null, e )
      : callback( new ElementNotFound( 'Could not find an HTMLElement from ' + str ), null );
  }
  exports.HTMElement = HTMElement;

  /**
   * Given a `NodeList`, `HTMLElement`, or a `string`
   * representing either of those after being passed to
   * `querySelectorAll`; coerces it to an array of
   * `HTMLElement`s, and returns the result of a `callback`
   * being passed that `array`.
   *
   * If an `HTMLElement` or `NodeList` cannot be coerced;
   * returns the result of a `callback` being passed a
   * `NodeListNotFound` error.
   *
   * @param {NodeList|HTMLElement|string} e -
   *  A `NodeList`, `HTMLElement`, or a `string` that would
   *  produce those after being passed to `querySelectorAll`.
   *
   * @param {function} callback -
   *  A callback function that uses the result of this
   *  function's coersion. Is passed two arguments; `error`
   *  and `result`, following Node.js conventions.
   *
   * @note If a string is passed as `e`, and it is not a
   * known HTML5 element, and does not begin with `.` or
   * `#` - a `.` will be prepended.
   *
   * @returns {void|*}
   */
  function HTMElements ( e, callback ) {
    let str = e;
    // If e is a known HTMLElement, or begins with #/.
    // pass as-is to querySelectorAll. Otherwise, prepend a '.'.
    e = typeof e === 'string'
      ? document.querySelectorAll( ( HTMLELMS.test( e ) || e.charAt() === '#' || e.charAt() === '.' ? e : '.' + e ) )
      : e;
    return !( e instanceof NodeList ) && !( e instanceof HTMLElement )
      ? callback( new NodeListNotFound( 'Could not find a HTMLElements from ' + str ), null )
      : callback( null, e instanceof NodeList ? nodeListToArray( e ) : [ e ] );
  }
  exports.HTMElements = HTMElements;

  /**
   * Given a `NodeList`, returns its contents as
   * an array. If the `nodeList` argument is a
   * string, it will try to get a `NodeList`
   * using the argument in a call to
   * `querySelectorAll`.
   *
   * @param {nodeList} nodeList -
   *   A nodeList
   *
   * @returns {Node[]} -
   *   The nodeList in the form of an array.
   */
  function nodeListToArray ( nodeList ) {
    return nodeList instanceof NodeList ?
      [].slice.call( nodeList ) :
        typeof nodeList === 'string' ?
          nodeListToArray( document.querySelectorAll( nodeList ) ) :
          console.error( `[nodetools.nodeListToArray] Notice: Cannot get NodeList from: ${ nodeList }` );
  }
  exports.nodeListToArray = nodeListToArray;




  module.exports = exports;

}).call( this );