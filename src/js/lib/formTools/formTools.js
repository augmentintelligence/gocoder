(function() {

  const html = require( '@3lessthan/htmltools' );

  let exports = {};

  function statusImg ( imgClass, activeID ) {

    let imgs = html.getElements( imgClass ),
      current = imgs.filter(
       img => img.classList.contains( 'active' )
          || ( !(img.classList.contains( 'active' )) && !(img.classList.contains( 'd-none' )) )
      ),
      active = imgs.filter( img => img.getAttribute( 'id' ) === activeID );

    if ( !active.length )
      throw new Error( '[statusImg] Cannot find img element with class "' + imgClass + '" and ID "' + activeID + '".' );

    active = active[0];

    if ( current.length ) {
      current = current[0];
      if ( current.getAttribute( 'id' ) !== activeID ) {
        current.classList.remove( 'active' );
        setTimeout(
          () => {
            current.classList.add( 'd-none' );
            active.classList.remove( 'd-none' );
            active.classList.add( 'active' );
          },
          500
        );
      } else current.classList.add( 'active' );
    } else {
      active.classList.remove( 'd-none' );
      active.classList.add( 'active' );
    }
  }

  exports.statusImg = statusImg;

  /**
   * Given an `HTMLElement`, an array of `HTMLElement`s,
   * a `NodeList`, or a string that will result in any of
   * the previous types when passed to
   * `document.querySelectorAll`;
   *
   * Will loop through each element, and either add a class
   * of `valid` and remove an `invalid` class if it exists,
   * or vice-versa depending on the value of `isValid`.
   *
   * **Note**: If `e` is a string, is not a vanilla HTML5
   * tag, and does not begin with either a `.` or `#`, it
   * will be converted to a class (`.`).
   *
   * @param {HTMLElement|NodeList|string} e -
   *  An `HTMLElement`, an array of `HTMLElement`s,
   *  a `NodeList`, or a string that will result in any of
   *  the previous types when passed to
   *  `document.querySelectorAll`.
   *
   * @param {Boolean} [isValid=true] -
   *  If `true`, will remove any existing `invalid` classes,
   *  and add `valid` classes if not already present on each
   *  element found/provided.
   *
   * @returns {void}
   */
  function setValidity( e, setValid=true ) {
    html.eachElement( e, element => {
      element.classList.remove( setValid ? 'invalid' : 'valid' );
      element.classList.add( setValid ? 'valid' : 'invalid' );
    });
  }
  exports.setValidity = setValidity;

  /**
   * Given an `HTMLImageElement`, an array of `HTMLElement`s,
   * a `NodeList`, or a string that will result in any of
   * the previous types when passed to
   * `document.querySelectorAll`;
   *
   * Will loop through each element, and if it is an
   * `HTMLImageElement`, will:
   *
   * - Set its `src` attribute to it's current path with
   *   `(in)valid` appended to the filename.
   * - Change its `alt` attribute to reflect the state.
   * - Add/remove `(in)valid` classes appropriately.
   *
   * **Note**: If `e` is a string, is not a vanilla HTML5
   * tag, and does not begin with either a `.` or `#`, it
   * will be converted to a class (`.`).
   *
   * @param {HTMLImageElement|NodeList|string} e -
   *  An `HTMLImageElement`, an array of `HTMLElement`s,
   *  a `NodeList`, or a string that will result in any of
   *  the previous types when passed to
   *  `document.querySelectorAll`.
   *
   * @param {Boolean} [isValid=true] -
   *  If `true`, will change each image's source to
   *  `<currentPath>valid<fileType>`, set its `alt` to
   *  `Valid Input`, remove any existing `invalid` class,
   *  and add a `valid` class.
   *
   * @returns {void}
   */
  function setImgValidity( e, setValid=true ) {
    html.eachElement( e, element => {
      if ( element instanceof HTMLImageElement ) {
        let validity = setValid ? 'Valid' : 'Invalid';
        element.src = validityImageToggle( element.src, setValid );
        element.alt = `${validity} Input`;
        element.classList.remove( setValid ? 'invalid' : 'valid' );
        element.classList.add( setValid ? 'valid' : 'invalid' );
      }
    });
    function validityImageToggle( path, setValid ) {
      let validity = setValid ? 'valid' : 'invalid',
              type = /\.(png|jpg|jpeg)$/i.exec( path )[1];
      return type
        ? path.replace( /(?:(?:in)?valid)?\.png$/i, `${validity}.${type}` )
        : path;
    }
  }
  exports.setImgValidity = setImgValidity;

  function eValidity(e, isValid) {
    if (!(e instanceof HTMLElement)) e = document.getElementById(e);
    e.classList.remove(isValid ? 'invalid' : 'valid');
    e.classList.add(isValid ? 'valid' : 'invalid');
  }
  exports.eValidity = eValidity;

  function imgValidity(e, isValid) {
    if (!(e instanceof HTMLElement)) e = document.getElementById(e);
    if (!(e instanceof HTMLImageElement))
      throw new Error('imgValidity called on a non-image element.');
    let validity = isValid ? 'Valid' : 'Invalid';
    e.src = `/img/form/${validity.toLowerCase()}.png`;
    e.alt = `${validity} Input`;
    eValidity(e, isValid);
  }
  exports.imgValidity = imgValidity;

  function getFiles(e) {
    if (!(e instanceof HTMLElement)) e = document.getElementById(e);
    if (!(e instanceof HTMLInputElement))
      throw new Error('getFiles called on a non-input element.');
    if (!e.files.length)
      throw new Error('getFiles called on an empty fileList.');
    return e.files;
  }
  exports.getFiles = getFiles;

  /**
   * Given an HTMLElement, or the ID of one;
   * returns a boolean indicating whether it
   * has a `disabled` attribute.
   *
   * @param {string|HTMLElement} e -
   *  An HTMLElement, or the ID of one in the
   *  form of a string.
   *
   * @returns {boolean}
   */
  function isDisabled( e ) {
    return html.getElement( e, ( err, el ) => {
      if ( err ) throw err;
      return el.hasAttribute( 'disabled' );
    });
  } exports.isDisabled = isDisabled;

  /**
   * Given an `HTMLElement`, or the ID of one;
   * adds or removes its `disabled` attribute
   * given the value of the argument of the
   * same name. Only manipulates the Element
   * attribute if it needs to.
   *
   * @param {string|HTMLElement|NodeList} e -
   *  An `HTMLElement`, `NodeList`, or a `string`
   *  representing one of the former types after
   *  being passed to `querySelectorAll`.
   *
   * @param {boolean} [disabled=true] -
   *  If `true`, adds or ensures the element(s)
   *  have/has the `disabled` attribute.
   */
  function setDisabled( e, disabled=true ) {
    html.getElements( e, ( err, els ) => {
      if ( err ) throw err;
      for ( let el of els ) {
        if ( disabled && !el.hasAttribute( 'disabled' ) )
          el.setAttribute( 'disabled', '' );
        if ( !disabled && el.hasAttribute( 'disabled' ) )
          el.removeAttribute( 'disabled' );
      }
    });
  } exports.setDisabled = setDisabled;


  module.exports = exports;
}.call(this));
