(function() {
  const fieldSet = require('../fieldMapping/formFieldSet');
  const FieldMapOptions = require('../nodeComponents/form/fieldMaps');
  const htmel = require( '@3lessthan/htmltools' );
  const bst = require('../bsTools/bootstrapTools');
  const node = require('@3lessthan/nodetools');

  let exports = {};

  // Form Element IDs
  const ID = {
    cardButton: i => `cardButton${i}`,
    file: 'dataInput',
    maps: ['mapStreetAddress', 'mapAddressLine2', 'mapCommunity', 'mapPostal', 'mapPrimaryKey', 'mapEnglishName']
  };

  const fieldSetMaps = {
    PrimaryKey: 'mapPrimaryKey',
    Name: 'mapEnglishName',
    StreetAddress: 'mapStreetAddress',
    AddressLine2: 'mapAddressLine2',
    City: 'mapCommunity',
    PostalCode: 'mapPostal'
  };

  function fileOk( cols ) {

    // Get Guessed Field Maps
    let fieldMap = fieldSet( cols );

    // Populate Map Selects ans auto-select guessed maps
    Object.keys( fieldSetMaps ).forEach( key => {
      node.appendComponent( new FieldMapOptions( fieldSetMaps[ key ], [''].concat(cols), fieldMap[ key ] ) );
    });

    // Enable Card 2
    document.getElementById( `cardButton${2}` ).classList.remove( 'disabled' );

    // Delay Auto-Toggle of Card 2
    window.setTimeout( toggleCard, 900, 2 );

  } exports.fileOk = fileOk;

  function resetForm() {

    // If necessary; toggle Card 1
    toggleCard(1);

    // Clear Mapping Select Options
    node.removeGroupChildren(htmel.getElements(Object.keys(fieldSetMaps).map(map => '#' + fieldSetMaps[map])));

    // Reset Operation Radios to 'Both'
    document.getElementById('radioOperationBoth').click();

    // Reset Checkboxes
    // ~> Force-Casing
    if ( !bst.checkboxRadioState('switchForceCasingLabel') ) bst.checkboxRadioToggle('switchForceCasingLabel');
    // ~> Ontario-Only
    if ( !bst.checkboxRadioState('switchONOnlyLabel') ) bst.checkboxRadioToggle('switchONOnlyLabel');

    // Clear Casing Multi-Select Options
    node.removeChildren('multiSelectCasing');

    // Disable all cards other than Card 1
    [2,3,4].forEach(i => {
      let card = document.getElementById(ID.cardButton(i));
      if ( !card.classList.contains('disabled') ) card.classList.add('disabled');
    });

  } exports.resetForm = resetForm;

  function toggleCard( i ) {

    // Ensure the card number exists
    if (typeof i !== 'number' || i < 1 || i > 4)
      throw new Error('toggleCard: i is out of range');
    // Otherwise, use bootstrapTools
    else bst.openCard( 'cardButton'+i );

  } exports.toggleCard = toggleCard;


  function getOptions() {

    this.caps = document.getElementById( 'switchForceCasingLabel' ).classList.contains('ui-state-active')
      ? false
      : '';

    let ops = bst.radioGroupValue( [ 'radioOperationClean', 'radioOperationBoth', 'radioOperationValidate' ] );
    console.log( 'Operations Selected: ' + ops );
    let validate = ops === 'both' || ops === 'validate',
           clean = ops === 'both' || ops === 'clean';

    this.validate = validate;
    this.clean = clean;
    this.fields = {
      PrimaryKey: node.getSelectValue( document.getElementById( 'mapPrimaryKey' ) ),
      Name: node.getSelectValue( document.getElementById( 'mapEnglishName' ) ),
      StreetAddress: node.getSelectValue( document.getElementById( 'mapStreetAddress' ) ),
      AddressLine2: node.getSelectValue( document.getElementById( 'mapAddressLine2' ) ),
      City: node.getSelectValue( document.getElementById( 'mapCommunity' ) ),
      PostalCode: node.getSelectValue( document.getElementById( 'mapPostal' ) )
    };

    if ( validate ) {
      this.fields.Lat = document.getElementById( 'txtLatitudeField' ).getAttribute( 'value' );
      this.fields.Lat = this.fields.Lat === '' ? 'GEO_LAT' : this.fields.Lat;
      this.fields.Lon = document.getElementById( 'txtLongitudeField' ).getAttribute( 'value' );
      this.fields.Lon = this.fields.Lon === '' ? 'GEO_LON' : this.fields.Lon;
    }

    if ( clean && !this.caps ) {
      this.casing = function (select) {
        var result = [];
        var options = select && select.options;
        var opt;

        for (var i=0, iLen=options.length; i<iLen; i++) {
          opt = options[i];

          if (opt.selected) {
            result.push(opt.value || opt.text);
          }
        }
        return result;
      }(document.getElementById('multiSelectCasing'));
    }

    this.postalCodeLimit = document.getElementById( 'switchONOnlyLabel' )
      .classList.contains( 'ui-state-active' )
        ? /[KLMNP]/i
        : undefined;

  } exports.getOptions = getOptions;


  module.exports = exports;
}).call(this);