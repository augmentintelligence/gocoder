const arrayFlatten = require( '../../arrayTools/lib/flatten' ),
       tokenizeSet = ( set ) => arrayFlatten( set.map( words => tokenize( words ) ) );

module.exports = {

  PrimaryKey: {
    names: tokenizeSet (
      [
        'Key',
        'Primary Key',
        'MOH Service Provider Ident'
      ]
    ),
    valueExceptions: []
  },

  Name: {
    names: tokenizeSet (
      [
        'Name',
        'English Name',
        'Organization Name'
      ]
    ),
    valueExceptions: [
      '',
      'N/A'
    ]
  },

  StreetAddress: {
    names: tokenizeSet (
      [
        'Street Address',
        'Address Line 1',
        'Address 1'
      ]
    ),
    valueExceptions: []
  },

  AddressLine2: {
    names: tokenizeSet (
      [
        'Address Line 2',
        'Address 2'
      ]
    ),
    valueExceptions: []
  },

  City: {
    names: tokenizeSet (
      [
        'City',
        'Community'
      ]
    ),
    valueExceptions: []
  },

  PostalCode: {
    names: tokenizeSet (
      [
        'Postal Code',
        'Code Postal'
      ]
    ),
    valueExceptions: []
  }

};

function tokenize ( words ) {

  if ( typeof words === 'string' ) {
    if ( words.indexOf( ' ' ) < 0 )
      return [ words, words.toUpperCase() ];
    else words = words.split( ' ' );
  }

  let joins = [ '_', '-', ' ', '' ];

  if ( words.length === 1 )
    return [ words[0], words[0].toUpperCase() ];
  else
    return joinSet( words, joins ).concat( joinSet( capitalizeAll( words ), joins ) );

  function capitalizeAll( words ) {
    return words.map( word => word.toUpperCase() );
  }

  function joinSet ( words, joins ) {
    return joins.map( join => words.join( join ) );
  }

}