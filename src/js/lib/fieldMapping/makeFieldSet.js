const mapFields = require('./lib/mapObjFields');

const makeFieldSet = function ( obj, [ latName, lonName ] ) {
  let fieldset = {
       PrimaryKey: '',
             Name: '',
    StreetAddress: '',
     AddressLine2: '',
             City: '',
       PostalCode: '',
  };
  mapFields( fieldset, obj );
  fieldset.Lat = latName;
  fieldset.Lon = lonName;
  return fieldset;
};

module.exports = makeFieldSet;