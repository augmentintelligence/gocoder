const mapFields = require('./lib/mapArrFields');

const formFieldSet = function ( cols ) {
  let fieldset = {
       PrimaryKey: '',
             Name: '',
    StreetAddress: '',
     AddressLine2: '',
             City: '',
       PostalCode: '',
  };
  mapFields( fieldset, cols );
  return fieldset;
};

module.exports = formFieldSet;