const fieldSets = require('../data/fieldSets');

const mapArrFields = function ( fieldSet, columns ) {

  let mapKeys = Object.keys( fieldSet );

  mapKeys.forEach( key => {

    let mapNames, fieldNameMap;

    mapNames     = fieldSets[ key ].names;
    fieldNameMap = mapNames.filter( name => columns.some( col => col === name ) )[0];

    if ( fieldNameMap )
      fieldSet[ key ] = fieldNameMap;

  });

};

module.exports = mapArrFields;