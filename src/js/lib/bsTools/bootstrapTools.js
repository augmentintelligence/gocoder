(function() {

  const arrayTools = require('../arrayTools/arrayTools');
  let exports = {};

  exports.openCard = openCard;
  function openCard ( cardButton ) {

    // Coerce String ID to HTMLElement
    if ( typeof cardButton === 'string' )
      cardButton = document.getElementById(cardButton);

    // If it's not an Anchor or Button, throw an error
    if ( !(cardButton instanceof HTMLButtonElement) && !(cardButton instanceof HTMLAnchorElement) )
      throw new Error( 'toggleCard called on a non-button element' );

    let card = document.querySelector( cardButton.getAttribute('data-target') );

    // Check to see if we even need to toggle it first
    if ( !card.classList.contains( 'show' ) && !cardButton.classList.contains( 'disabled' ) )
      // If it does, use Bootstrap's collapse method (Requires jQuery unfortunately)
      $( `#${card.getAttribute('id')}` ).collapse( 'show' );

  }

  exports.checkboxRadioIsInit = checkboxRadioIsInit;
  function checkboxRadioIsInit ( cbRadio ) {
    // Coerce String ID to HTMLElement
    if ( typeof cbRadio === 'string' )
      cbRadio = document.getElementById( cbRadio );

    if ( cbRadio instanceof HTMLInputElement ) {
      if ( cbRadio.hasAttribute('type') ) {
        let type = cbRadio.getAttribute('type');
        if ( type === 'radio' || type === 'checkbox' ) {
          return cbRadio.classList.contains( 'ui-checkboxradio' );
        } else console.error( 'checkboxRadioIsInit: Called on non-radio/checkbox input element' );
      } else console.error ( 'checkboxRadioIsInit: Called on input element without a type attribute' );
    } else console.error( 'checkboxRadioIsInit: Called on non-input element' );
  }

  exports.checkboxRadioInit = checkboxRadioInit;
  function checkboxRadioInit ( query ) { $(query).checkboxradio(); }

  exports.checkboxRadioState = checkboxRadioState;
  function checkboxRadioState ( label ) {

    // Coerce String ID to HTMLElement
    if ( typeof label === 'string' )
      label = document.getElementById( label );

    // If it's not a label/input, throw an error
    if ( !( label instanceof HTMLLabelElement ) && !( label instanceof HTMLInputElement ) )
      throw new Error( 'checkboxState called on a non-label/input element' );

    // If it's a label...
    if ( label instanceof HTMLLabelElement ) {

      // Get the input element.
      let input = labelToInput( label );

      // If input is nothing, log error.
      if ( !input ) console.error( "checkboxRadioState: Could not find input from label" );

      // If input is not a radio or checkbox, log error.
      if ( !isRadio( input ) && !isCheckbox( input ) ) console.error( "checkboxRadioState: called on a non-checkbox/radio input element" );


      let isInit = checkboxRadioIsInit( input );

      if ( isInit ) return label.classList.contains( 'ui-state-active' );
      else if ( isInit === false ) {
        checkboxRadioInit( '#' + input.getAttribute( 'id' ) );
        return label.classList.contains( 'ui-state-active' );
      }

    } else if ( label instanceof HTMLInputElement ) {
      if ( !isRadio( label ) && !isCheckbox( label ) )
        return console.error( "checkboxRadioState: called on a non-checkbox/radio input element" );
      let isInit = checkboxRadioIsInit( label );
      if ( isInit )
        return label.classList.contains( 'ui-state-active' );
      else if ( isInit === false ) console.error( 'checkboxRadioState: Called on uninitialized element.' );
    } else console.error( "checkboxRadioState: Called on a non-label/input element" );
  }

  exports.checkboxRadioToggle = checkboxRadioToggle;
  function checkboxRadioToggle ( label ) {

    // Coerce String ID to HTMLElement
    if ( typeof label === 'string' )
      label = document.getElementById( label );

    // If label is actually a label, get it's input element
    if ( label instanceof HTMLLabelElement ) {

      let cbRadio = document.getElementById( label.getAttribute( 'for' ) ),
           isInit = checkboxRadioIsInit( cbRadio );

      if ( isInit ) cbRadio.click();
      else if ( isInit === false ) console.error( 'checkboxRadioToggle: Called on uninitialized element.' );

    // If label is the checkbox/radio itself;
    //   - check if it's initialized, and if so...
    //   - click it
    } else if ( label instanceof HTMLInputElement ) {
      let isInit = checkboxRadioIsInit( label );
      if ( isInit ) label.click();
      else if ( isInit === false ) console.error( 'checkboxRadioToggle: Called on uninitialized element.' );

    } else throw new Error( 'checkboxRadioToggle: Called on invalid element.' );

  }

  exports.radioGroupValue = radioGroupValue;
  function radioGroupValue ( ...groupLabelIDs ) {

    // Flatten groupLabelIDs in case it was already an array.
    if ( Array.isArray( groupLabelIDs[ 0 ] ) )
      groupLabelIDs = arrayTools.flatten( groupLabelIDs );

    return groupLabelIDs.reduce( ( acc, id ) => {

      // Coerce String ID to HTMLElement
      if ( typeof id === 'string' )
        id = document.getElementById( id );

      if ( id instanceof HTMLLabelElement ) {
        let input = labelToInput( id );
        if ( isRadio( input ) && checkboxRadioState( id ) )
          return acc + input.getAttribute( 'value' );
        else return acc;
      } else if ( id instanceof HTMLInputElement ) {
        let label = inputToLabel( id );
        if ( isRadio( id ) && checkboxRadioState( label ) )
          return acc + id.getAttribute( 'value' );
        else return acc;
      } else return acc;
    }, '' );

  }

  exports.labelToInput = labelToInput;
  function labelToInput ( label ) {

    // Coerce String to HTMLElement
    if ( typeof label === 'string' )
      label = document.getElementById( label );

    if ( label instanceof HTMLLabelElement ) {
      if ( label.hasAttribute( 'for' ) ) {
        let forID = label.getAttribute( 'for' ),
             forE = document.getElementById( forID );
        if ( forE ) {
          if ( forE instanceof HTMLInputElement ) return forE;
          else console.error( "labelToInput: Label's for attribute refers to a non-input element." );
        } else console.error( "labelToInput: Could not locate label's for element." );
      } else console.error( "labelToInput: Label does not have a for attribute." );
    } else if ( label instanceof HTMLInputElement ) {
      return label;
    } else console.error( "labelToInput: label is not a label element." );

  }

  exports.inputToLabel = inputToLabel;
  function inputToLabel ( input ) {

    // Coerce String to HTMLElement
    if ( typeof label === 'string' )
      input = document.getElementById( input );

    if ( input instanceof HTMLInputElement ) {
      if ( input.hasAttribute( 'id' ) ) {
        let label = $(`label[for='${ input.getAttribute( 'id' ) }']`);
        if ( label ) return label[0];
        else console.error( "inputToLabel: Could not find label for given input's id." );
      } else console.error( "inputToLabel: Input doesn't have an ID" );
    } else if ( input instanceof HTMLLabelElement ) {
      return input;
    } else console.error( "inputToLabel: input is not an input or label element." );

  }

  exports.isRadio = isRadio;
  function isRadio ( input ) {

    // Coerce String to HTMLElement
    if ( typeof input === 'string' )
      input = document.getElementById( input );

    if ( !( input instanceof HTMLElement ) )
      throw new Error( `isRadio: invalid argument ${input}` );

    return input instanceof HTMLInputElement
        && input.hasAttribute( 'type' )
        && input.getAttribute( 'type' ) === 'radio';

  }

  exports.isCheckbox = isCheckbox;
  function isCheckbox ( input ) {

    // Coerce String to HTMLElement
    if ( typeof input === 'string' )
      input = document.getElementById( input );

    if ( !( input instanceof HTMLElement ) )
      throw new Error( `isCheckbox: invalid argument ${input}` );

    return input instanceof HTMLInputElement
        && input.hasAttribute( 'type' )
        && input.getAttribute( 'type' ) === 'checkbox';

  }

  module.exports = exports;

}).call(this);