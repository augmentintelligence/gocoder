(function() {

  function Progress( e, total ) {
    this.e     = e;
    this.total = total;
    this.init  = () => {
      this.e.setAttribute( 'style', 'width: 0%' );
      this.e.setAttribute( 'aria-valuenow', '0' );
      this.e.setAttribute( 'aria-valuemin', '0' );
      this.e.setAttribute( 'aria-valuemax', String( this.total ) );
      this.e.innerText = '0/' + this.total;
    };
    this.percent = current => current / this.total;
    this.update  = current => {
      this.e.setAttribute( 'style', `width: ${this.percent( current )*100}%` );
      this.e.setAttribute( 'aria-valuenow', String( current ) );
      this.e.innerText = `${current}/${this.total}`;
    };
    this.skip = () => {
      this.e.setAttribute( 'style', 'width: 100%' );
      this.e.setAttribute( 'aria-valuenow', String( this.total ) );
      this.e.setAttribute( 'aria-valuemin', '0' );
      this.e.setAttribute( 'aria-valuemax', String( this.total ) );
      this.e.classList.add( 'skip' );
      this.e.innerText = 'Skipped';
    };
    this.error = () => {
      this.e.setAttribute( 'style', 'width: 100%' );
      this.e.setAttribute( 'aria-valuenow', String( this.total ) );
      this.e.setAttribute( 'aria-valuemin', '0' );
      this.e.setAttribute( 'aria-valuemax', String( this.total ) );
      this.e.classList.add( 'error' );
      this.e.innerText = this.e.innerText + ' [ERROR]';
    };
  }

  module.exports = Progress;

}).call(this);