(function() {
  module.exports = function FieldMapMulti (parentID, fields, map) {

    console.log(fields);
    this.option = fields
      .filter( field => field !== map.Postal && ( map.PrimaryKey === '' || field !== map.PrimaryKey ))
      .map( ( field, i ) => {
        let opt = {
          parent: parentID,
          e: 'option',
          params: {
               id: `${parentID}-option-${i}`,
            value: field
          },
          html: field
        };
        if (
          field === map.StreetAddress
          || field === map.AddressLine2
          || field === map.Community
          || field === map.EnglishName
        ) opt.params.selected = '';
        return opt;
    });

  };
}.call(this));