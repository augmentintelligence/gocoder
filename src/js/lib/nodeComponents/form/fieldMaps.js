(function() {
  module.exports = function FieldMapOptions (parentID, fields, active) {
    this.option = fields.map((field, i) => {
      let opt = {
        parent: parentID,
        e: 'option',
        params: {
          id: `${parentID}-option-${i}`
        },
        html: field
      };
      if ( field !== '' ) opt.params.value = field;
      if ( field === active ) opt.params.selected = '';
      return opt;
    });
  };
}.call(this));