( function () {
  /**
   * Detects the operating system of the client machine
   * by analyzing the value of `navigator.appVersion`.
   *
   * Returns one of the following strings:
   *
   *  - "windows" for all versions of Windows
   *  - "macos"   for all versions of Macintosh OS
   *  - "linux"   for all versions of Linux
   *  - "unix"    for all other UNIX flavors
   *  - "unknown" indicates failure to detect the OS
   *
   * To get more detailed OS information, you can perform
   * a more sophisticated analysis of `navigator.appVersion`
   * or `navigator.userAgent`. Though it should be noted that
   * some clients may *spoof* their `navigator.userAgent`
   * string.
   *
   * @returns {string}
   */
  function getOS () {
    let av = navigator.appVersion;
    return typeof av !== 'string'    ? "unknown"
      : av.indexOf( "Win" ) !== -1   ? "windows"
      : av.indexOf( "Mac" ) !== -1   ? "macos"
      : av.indexOf( "X11" ) !== -1   ? "unix"
      : av.indexOf( "Linux" ) !== -1 ? "linux"
      : "unknown";
  }

  module.exports = getOS;

}).call(this);