const html = require( '@3lessthan/htmltools' );

const csvTools = require('../lib/csvTools/csvTools');
const formTools = require('../lib/formTools/formTools');

const goForm = require('../lib/goCoderForm/goCoderForm');
const goCoder = require('../../../tool/web/gocoder');
const bst = require('../lib/bsTools/bootstrapTools');

const checkMapping = require( './checkMapping' );

const events = {
  mapSelect: require( './events/mapSelect' ),
    opRadio: require( './events/operationRadio' )
};

window.addEventListener('load', () => {
  // File Input
  let fileInput = document.getElementById('dataInput');
  fileInput.value = '';
  fileInput.addEventListener(
    'change',
    () => {

      const inputID = 'dataInput';

      let feedbackText = document.getElementById(inputID + 'FeedbackText'),
                  file = formTools.getFiles(inputID)[0];

      // Set input text to file name
      document.getElementById(inputID + 'Label').innerHTML = file.name;

      // Initialize Radio/Checkboxes
      bst.checkboxRadioInit("input[type='radio']"); // ~> Radios: Operations
      bst.checkboxRadioInit("#switchForceCasing");  // ~> Checkbox: Force-Casing
      bst.checkboxRadioInit("#switchONOnly");       // ~> Checkbox: Ontario-Only

      // Reset Form
      goForm.resetForm();

      // Ensure File has extension .csv
      if (!/\.csv$/.test(file.name)) {

        // Invalidate form
        inputValidity(false, 'Error: Not a .csv file');

      } else {

        // Read the CSV
        let reader = new FileReader();

        // Define Read Action
        reader.onload = ( aFile => e => {
          // Get the response of csvValidate
          let res = csvTools.csvValidate( e.target.result );
          if ( res instanceof Error ) inputValidity( false, res.message );
          else {
            inputValidity( true, 'Looks like a CSV!' );
            goForm.fileOk( res );
            document.getElementById( 'csv' ).dataset.content = e.target.result;
            document.getElementById( 'cols' ).dataset.content = res;
            checkMapping();
          }
        })();
        // Call Read Action
        reader.readAsText(file);
      }
      function inputValidity( isValid, msg ) {
        feedbackText.innerText = msg;
        formTools.eValidity(inputID + 'Form', isValid);
        formTools.eValidity(inputID + 'FeedbackText', isValid);
        formTools.statusImg( '.dataInputStatusImg', 'dataInputStatusImg' + ( isValid ? 'Valid' : 'Invalid' ) );
        // formTools.imgValidity(inputID + 'StatusImg', isValid);
        document
          .getElementById(inputID)
          .setCustomValidity(isValid ? '' : 'Invalid');
      }
    },
    false
  );

  //? Change Events
  //~~> Map-Selects
  html.eachElement( '.map-select', el => el.addEventListener( 'change', events.mapSelect.change, false ));

  //? Click Events
  //~~> Operation Radios
  html.getElement( '#radioOperationValidate' ).addEventListener( 'click', events.opRadio.validate.click, false );
  html.getElement( '#radioOperationClean'    ).addEventListener( 'click', events.opRadio.clean.click, false );
  html.getElement( '#radioOperationBoth'     ).addEventListener( 'click', events.opRadio.both.click, false );

  //~~> Card Button 1
  document.getElementById( 'cardButton1' ).addEventListener( 'click', () => {
    html.eachElement(
      [ '#cardButton4', '#runToolButton' ],
      e => { e.classList.add( 'disabled' ); }
    );
  });

  //~~> Card Button 2
  document.getElementById( 'cardButton2' ).addEventListener( 'click', () => {
    html.eachElement(
      [ '#cardButton4', '#runToolButton' ],
      e => { e.classList.add( 'disabled' ); }
    );
  });

  //~~> Card Button 3
  document.getElementById( 'cardButton3' ).addEventListener( 'click', () => {
    html.eachElement(
      [ '#cardButton4', '#runToolButton' ],
      e => { e.classList.remove( 'disabled' ); }
    );
  });

  document.getElementById( 'runToolButton' ).addEventListener( 'click', () => {
    // Run only if the button isn't disabled
    if ( !document.getElementById( 'runToolButton' ).classList.contains('disabled') ) {
      // Hide/Disable the Button
      html.getElement( '#runToolButton', ( err, e ) => {
        formTools.setDisabled( e );
        e.classList.add( 'd-none' );
      });
      // Disable all Card Buttons
      html.eachElement( '.card-button',  e => { e.classList.add( 'disabled' ); });
      // Unhide the Console
      html.getElement( '#console-container' ).classList.remove('d-none');
      // Prep variables to send to execute function
      let csv = document.getElementById('csv').dataset.content,
          opt = new goForm.getOptions(),
         cols = document.getElementById( 'cols' ).dataset.content.split(','),
        files = goCoder.fileNames( document.getElementById( 'dataInputLabel' ).innerText );
      //! Need to apply checking of OS/Fallback for newline types
      csv = csv.substr(csv.indexOf('\n') + 1 );
      // Run tool
      goCoder.execute( csv, cols, opt, files );
    }
  });

});
