(function(){

  // Module Includes
  const ft = require( '../lib/formTools/formTools' ),
       arr = require( '@3lessthan/arraytools' ),
      node = require( '@3lessthan/nodetools' ),
      html = require( '@3lessthan/htmltools' );

  // App Includes
  const goForm = require( '../lib/goCoderForm/goCoderForm' );

  // Node Components
  const FieldMapMulti = require( '../lib/nodeComponents/form/fieldMapsMulti' );

  // Empty value test
  const notEmpty = value => value !== '';

  function checkMapping () {
    // Feedback Object
    let feedback = new Feedback(),
            maps = new Maps(),
       mandatory = maps.vMandatory,
      duplicates = maps.vDuplicates;
    // Pass Tests
    if ( mandatory && duplicates ) {
      // Update feedback object.
      feedback.valid = true;
      feedback.msg   = 'Looks good!';
      // Apply 'valid', remove 'invalid' classes to each select
      // and feedback text elements.
      maps.elements
        .concat( document.getElementById( 'mappingFeedbackText' ) )
        .forEach( e => {
          e.classList.remove( 'invalid' );
          e.classList.add( 'valid' );
        });
      // Clear ApplyCasing Multi-Select
      node.removeChildren( document.getElementById( 'multiSelectCasing' ) );
      // Populate/make default selections to ApplyCasing Multi-Select
      node.appendComponent(
        new FieldMapMulti(
          'multiSelectCasing',
          document.getElementById( 'cols' ).dataset.content.split(','),
          maps.map
        )
      );
      // Enable Card Button 3
      document.getElementById( 'cardButton3' )
        .classList.remove( 'disabled' );
    } else {
      // Set Feedback Validity
      feedback.valid = false;
      // Disable Card Buttons 3 & 4
      html.eachElement( ['#cardButton3', '#cardButton4'], e => e.classList.add('disabled') );
      // Ensure Card 2 is Open
      goForm.toggleCard( 2 );
      // If mandatory check fails
      if ( !mandatory ) {
        // Push message to feedback
        feedback.msg = 'All mandatory field types must be matched.';
        // Highlight Empty Mandatory Selects
        maps.elements.slice( 0, 4 ).forEach(( e, i ) => {
          let v = maps.values[i];
          e.classList.remove( v === '' ? 'valid' : 'invalid' );
          e.classList.add( v === '' ? 'invalid' : 'valid' );
        });
      }
      // If duplicates check fails
      if ( !duplicates ) {
        // Push message to feedback
        feedback.msg = 'Error: Cannot map the same column to multiple field types.';
        maps.values.forEach( (v,i) => {
          // If the current Select Element's value isn't empty...
          if ( v !== '' ) ft.eValidity(
            // Set its validity to...
            maps.elements[i],
            // Whether its value still exists in the array of values
            // when its own value is removed from that array.
            !maps.values
              .slice( 0, i )
              .concat( maps.values.slice( i + 1 ) )
              .includes( v )
            );
        });
      }
    }
    // Apply Feedback (Always)
    feedback.update();
  }

  class Feedback {

    constructor() {
      this.e = document.getElementById( 'mappingFeedbackText' );
      this.message = [];
      this.valid = null;
    }

    get addClass()  { return this.valid ? 'valid' : 'invalid'; }
    get dropClass() { return this.valid ? 'invalid' : 'valid'; }
    get msg()       { return this.message.join( '\n' ); }

    set msg( str )  { this.message.push( str ); }
    update() {
      this.e.classList.remove( this.dropClass );
      this.e.classList.add( this.addClass );
      this.e.innerText = this.msg;
    }
  }

  class Maps {
    constructor() {
      this.keys     = [ 'StreetAddress', 'AddressLine2', 'Community', 'Postal', 'PrimaryKey', 'EnglishName' ];
      this.ids      = this.keys.map( key => 'map' + key );
      this.elements = this.ids.map( id => document.getElementById( id ) );
      this.values   = this.elements.map( e => node.getSelectValue( e ) );
      let map = {};
      for ( let i = 0, n = this.keys.length; i < n; i++ ) { map[this.keys[i]] = this.values[i]; }
      this.map = map;
    }

    // Check if the first 4 values (which are mandatory) aren't empty
    get vMandatory() { return this.values.slice(0,4).every( notEmpty ); }

    // Filtering out empty values, check if the length of an array of
    // unique selected values is equal to the length of all selected values.
    get vDuplicates() {
      let nonEmpty = this.values.filter( notEmpty );
      return arr.unique( nonEmpty ).length === nonEmpty.length;
    }
  }

  module.exports = checkMapping;

}).call(this);