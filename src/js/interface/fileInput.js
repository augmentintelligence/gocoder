(function(){

  // Toolkits
  const bs = require( '../lib/bsTools/bootstrapTools' ),
        ft = require( '../lib/formTools/formTools');
  // App Functions
  const goForm = require( '../lib/goCoderForm/goCoderForm' ),
      csvTools = require( '../lib/csvTools/csvTools' ),
  checkMapping = require( './checkMapping' );

  // Element IDs
  const feedbackTextID = 'dataInputFeedbackText',
            filenameID = 'dataInputLabel',
               inputID = 'dataInput';

  function inputChange() {

    // Elements
    const filename = document.getElementById( filenameID );

    // File
    let file = ft.getFiles( inputID )[ 0 ];

    // Set input text (label) to file name
    filename.innerText = file.name;

    // Initialize Radio/Checkboxes
    bs.checkboxRadioInit( "input[type='radio']" ); // ~> Radios: Operations
    bs.checkboxRadioInit( "#switchForceCasing" );  // ~> Checkbox: Force-Casing
    bs.checkboxRadioInit( "#switchONOnly" );       // ~> Checkbox: Ontario-Only

    // Reset Form
    goForm.resetForm();

    // Ensure File has extension .csv
    if (!/\.csv$/.test(file.name)) {

      // If not: Invalidate form
      inputValidity(false, 'Error: Not a .csv file');

    } else {

      // Read the CSV
      // Create Reader
      let reader = new FileReader();
      // Define Reader Action
      reader.onload = ( aFile => event => {
          let content = event.content.result;

          // Get the response of csvValidate
          let res = csvTools.csvValidate( content );

          // If it's an error:
          if ( res instanceof Error )
            inputValidity( false, res.message );

          // Otherwise...
          else {
            // Set the
            inputValidity( true, 'Looks like a CSV!' );
            goForm.fileOk( res );
            document.getElementById( 'csv' ).dataset.content = content;
            document.getElementById( 'cols' ).dataset.content = res;
            checkMapping();
          }
      })();
      // Call Read Action
      reader.readAsText(file);
    }
  }



  function inputValidity( isValid, msg ) {
    document.getElementById( feedbackTextID ).innerText = msg;
    ft.eValidity( inputID + 'Form', isValid );
    ft.eValidity( inputID + 'FeedbackText', isValid );
    ft.imgValidity( inputID + 'StatusImg', isValid );
    document
      .getElementById(inputID)
      .setCustomValidity(isValid ? '' : 'Invalid');
  }



  module.exports = {
    event: {
      change: inputChange
    }
  };

}).call(this);