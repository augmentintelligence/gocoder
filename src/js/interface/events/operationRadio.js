(function () {

  const html = require( '@3lessthan/htmltools' );

  function cleanClick () {
    // Remove Selected Class from other inputs
    html.eachElement( '.op-radio-input', e => e.classList.remove( 'selected' ) );
    // Add Selected Class to Clean Input
    html.getElement( '#radioOperationClean' ).classList.add( 'selected' );
    // Ensure cleaning-only options are enabled
    html.eachElement( '.op-clean', e => setDisabled( e, false ));
    // Ensure validation-only options are disabled
    html.eachElement( '.op-validate', e => setDisabled( e, true ));
  }

  function bothClick() {
    // Remove Selected Class from other inputs
    html.eachElement( '.op-radio-input', e => e.classList.remove( 'selected' ) );
    // Add Selected Class to Both Input
    html.getElement( '#radioOperationBoth' ).classList.add( 'selected' );
    // Ensure both options are enabled
    html.eachElement( [ '.op-clean', '.op-validate' ], e => setDisabled( e, false ));
  }

  function validateClick() {
    // Remove Selected Class from other inputs
    html.eachElement( '.op-radio-input', e => e.classList.remove( 'selected' ) );
    // Add Selected Class to Both Input
    html.getElement( '#radioOperationValidate' ).classList.add( 'selected' );
    // Ensure validation-only options are enabled
    html.eachElement( '.op-validate', e => setDisabled( e, false ));
    // Ensure cleaning-only options are disabled
    html.eachElement( '.op-clean', e => setDisabled( e, true ));
  }

  function setDisabled( e, isDisabled=true ) {
    if (isDisabled && !e.hasAttribute('disabled'))
      e.setAttribute('disabled','');
    if (!isDisabled && e.hasAttribute('disabled'))
      e.removeAttribute('disabled');
  }

  module.exports = {
    validate: { click: validateClick },
       clean: { click: cleanClick },
        both: { click: bothClick }
  };

}).call(this);