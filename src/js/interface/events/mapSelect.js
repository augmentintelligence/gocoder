(function(){

  const checkMapping = require( '../checkMapping' );

  function mapSelectOnChange( ) {
    checkMapping();
  }

  module.exports = {
    change: mapSelectOnChange
  };

}).call( this );