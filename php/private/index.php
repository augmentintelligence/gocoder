 <!-- Body -->
  <div class="container-fluid" id="body">
   <!-- Title -->
    <h1 class="display-5 text-center body-title">
      Government of Ontario Geocoder
    </h1>
   <!-- Controls Accordion -->
    <div class="accordion" id="accordionInput">
     <!-- Step 1: Upload Dataset -->
      <div class="card">
       <!-- Header -->
        <div class="card-header" id="heading1">
          <h2 class="mb-0 d-flex justify-content-center">
            <button id="cardButton1" class="btn btn-link card-button" type="button" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
              Step 1: Upload Dataset
            </button>
          </h2>
        </div>
       <!-- Body -->
        <div id="collapse1" class="collapse show" aria-labelledby="heading1" data-parent="#accordionInput">
          <div class="card-body" id="dataInputCard">
            <div class="m-0 p-0 d-flex">
             <!-- Validity Icon -->
              <div id="dataInputStatus" class="form-status">
                <img id="dataInputStatusImgValid" class="form-status-img dataInputStatusImg" alt="Valid File" src="/img/form/valid.png">
                <img id="dataInputStatusImgInvalid" class="form-status-img dataInputStatusImg d-none" alt="Invalid File" src="/img/form/invalid.png">
              </div>
             <!-- Upload Form -->
              <form id="dataInputForm" method="GET" enctype="multipart/form-data" class="form needs-validation flex-fill" novalidate>
                <div class="custom-file">
                  <input type="hidden" name="MAX_FILE_SIZE" value="196412073820061055291122368003" />
                  <input type="file" name="dataset" class="custom-file-input" id="dataInput" accept=".csv" required>
                  <label id="dataInputLabel" class="custom-file-label" for="dataInput">Choose File</label>
                  <div class="invalid-feedback form-feedback" id="dataInputFeedback">
                    <p class="mx-0 mt-0 p-0 form-feedback-text" id="dataInputFeedbackText">
                      Please select a csv file to upload.
                    </p>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
     <!-- Step 2: Map Fields -->
      <div class="card">
       <!-- Header -->
        <div class="card-header" id="heading2">
          <h2 class="mb-0 d-flex justify-content-center">
            <button id="cardButton2" class="btn btn-link card-button collapsed disabled" type="button" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
              Step 2: Map Fields
            </button>
          </h2>
        </div>
       <!-- Body -->
        <div id="collapse2" class="collapse" aria-labelledby="heading2" data-parent="#accordionInput">
          <div class="card-body main-card" id="fieldMappingCard">
           <!-- MANDATORY MAPPING -->
            <h3 class="mb-0 text-center form-subheader">Mandatory Mapping</h3><hr>
             <!-- ROW 1 -->
              <div class="row justify-content-center">
               <!-- Street Address Mapping -->
                <div class="input-group mb-3 col-md-6">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="mapStreetAddress"><span>Street Address</span></label>
                  </div><select class="custom-select map-select mandatory" id="mapStreetAddress"></select>
                </div>
               <!-- Address Line 2 Mapping -->
                <div class="input-group mb-3 col-md-6">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="mapAddressLine2"><span>Address Line 2</span></label>
                  </div><select class="custom-select map-select mandatory" id="mapAddressLine2"></select>
                </div>
              </div>
             <!-- ROW 2 -->
              <div class="row justify-content-center">
               <!-- Community Mapping -->
                <div class="input-group mb-3 col-md-6">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="mapCommunity"><span>Community</span></label>
                  </div><select class="custom-select map-select mandatory" id="mapCommunity"></select>
                </div>
               <!-- Postal Code Mapping -->
                <div class="input-group mb-3 col-md-6">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="mapPostal"><span>Postal Code</span></label>
                  </div><select class="custom-select map-select mandatory" id="mapPostal"></select>
                </div>
            </div>
           <!-- OPTIONAL MAPPING -->
            <h3 class="mb-0 text-center form-subheader">Optional Mapping</h3><hr>
             <!-- ROW 1 -->
              <div class="row justify-content-center mb-0">
               <!-- Primary Key Mapping -->
                <div class="input-group mb-2 col-md-6">
                  <div class="input-group-prepend">
                    <label class="input-group-text" for="mapPrimaryKey"><span>Primary Key</span></label>
                  </div><select class="custom-select map-select" id="mapPrimaryKey"></select>
                </div>
               <!-- English Name Mapping -->
                <div class="input-group mb-2 col-md-6">
                    <div class="input-group-prepend">
                      <label class="input-group-text" for="mapEnglishName"><span>English Name</span></label>
                    </div><select class="custom-select map-select" id="mapEnglishName"></select>
                </div>
            </div>
            <div id="mappingFeedback" class="form-feedback">
              <p class="mx-0 mt-0 mb-1 p-0 form-feedback-text text-center" id="mappingFeedbackText">
                Please map all required fields
              </p>
            </div>
          </div>
        </div>
      </div>
     <!-- Step 3: Set Options -->
      <div class="card">
       <!-- Header -->
        <div class="card-header" id="heading3">
          <h2 class="mb-0 d-flex justify-content-center">
            <button id="cardButton3" class="btn btn-link card-button collapsed disabled" type="button" data-toggle="collapse" data-target="#collapse3" aria-expanded="false" aria-controls="collapse3">
              Step 3: Set Options
            </button>
          </h2>
        </div>
       <!-- Body -->
        <div id="collapse3" class="collapse" aria-labelledby="heading3" data-parent="#accordionInput">
          <div class="card-body main-card" id="setOptionsCard">
           <!-- OPERATIONS -->
            <h3 class="mb-0 text-center form-subheader">Operations</h3><hr>
             <!-- ROW 1 -->
              <div class="row justify-content-center mb-3">
               <!-- Clean-Only -->
                <div class="col-4">
                  <div class="row justify-content-center">
                    <div class="form-check">
                      <input class="form-check-input op-radio-input" type="radio" name="radioOperation" id="radioOperationClean" value="clean">
                      <label class="form-check-label" for="radioOperationClean" id="radioOperationCleanLabel">
                        Clean-Only
                      </label>
                    </div>
                  </div>
                </div>
               <!-- Both -->
                <div class="col-4">
                  <div class="row justify-content-center">
                    <div class="form-check">
                      <input class="form-check-input op-radio-input" type="radio" name="radioOperation" id="radioOperationBoth" value="both">
                      <label class="form-check-label" for="radioOperationBoth" id="radioOperationBothLabel">
                        Both
                      </label>
                    </div>
                  </div>
                </div>
               <!-- Validate-Only -->
                <div class="col-4">
                  <div class="row justify-content-center">
                    <div class="form-check">
                      <input class="form-check-input op-radio-input" type="radio" name="radioOperation" id="radioOperationValidate" value="validate">
                      <label class="form-check-label" for="radioOperationValidate" id="radioOperationValidateLabel">
                        Validate-Only
                      </label>
                    </div>
                  </div>
                </div>
              </div>
           <!-- GEOLOCATION FIELDS -->
            <h3 class="mb-0 text-center form-subheader">Geolocation Fields</h3><hr>
             <!-- ROW 1 -->
              <div class="row justify-content-center">
                <div class="col-md-6 mb-3">
                  <label id="txtLatitudeFieldLabel" for="txtLatitudeField" class="txtLabel">Latitude</label>
                  <input id="txtLatitudeField" type="text" class="form-control op-validate" placeholder="Latitude_Field_Name" value="GEO_LAT" required>
                </div>
                <div class="col-md-6 mb-3">
                  <label id="txtLongitudeFieldLabel" for="txtLongitudeField" class="txtLabel">Longitude</label>
                  <input id="txtLongitudeField" type="text" class="form-control op-validate" placeholder="Longitude_Field_Name" value="GEO_LON" required>
                </div>
              </div>
           <!-- Casing -->
            <h3 class="mb-0 text-center form-subheader">Casing</h3><hr>
             <!-- ROW 1 -->
              <div class="row justify-content-center mb-3 mt-2">
               <!-- Switch: Ontario Only -->
                <div class="col-md-5">
                  <div class="row justify-content-center">
                    <span class="switch switch-sm">
                      <input id="switchForceCasing" type="checkbox" class="switch switch-sm">
                      <label id="switchForceCasingLabel" for="switchForceCasing">Force Sentence-Case</label>
                    </span>
                  </div>
                </div>
               <!-- Multi-Select: Apply Casing To... -->
                <div class="col-md-7">
                    <label id="multiSelectCasingLabel" class="custom-select-label txtLabel" for="multiSelectCasing">Apply Casing To...</label>
                    <select id="multiSelectCasing" class="custom-select op-clean" multiple></select>
                </div>
              </div>
           <!-- MISC. -->
            <h3 class="mb-0 text-center form-subheader">Misc.</h3><hr>
             <!-- ROW 1 -->
              <div class="row justify-content-center mb-3 mt-3">
               <!-- Switch: Ontario Only -->
                <div class="col-12">
                  <div class="row justify-content-center">
                    <span class="switch switch-sm">
                      <input id="switchONOnly" class="switch switch-sm op-clean" type="checkbox">
                      <label id="switchONOnlyLabel" for="switchONOnly">Invalidate Non-Ontario Addresses</label>
                    </span>
                  </div>
                </div>
              </div>
          </div>
        </div>
      </div>
     <!-- Step 4: Run -->
      <div class="card">
       <!-- Header -->
        <div class="card-header" id="heading4">
          <h2 class="mb-0 d-flex justify-content-center">
            <button id="cardButton4" class="btn btn-link card-button collapsed disabled" type="button" data-toggle="collapse" data-target="#collapse4" aria-expanded="false" aria-controls="collapse4">
              Step 4: Run Tool
            </button>
          </h2>
        </div>
       <!-- Body -->
        <div id="collapse4" class="collapse" aria-labelledby="heading4" data-parent="#accordionInput">
          <div class="card-body main-card" id="runCard">
           <!-- Submit Button -->
            <div class="row justify-content-center">
              <button id="runToolButton" class="btn" type="button">Run Tool</button>
            </div>
           <!-- Console -->
           <!-- Re-Add d-none when finished -->
            <div id="console-container" class="container-fluid d-none">
             <!-- CSV2JSON -->
              <div id="csv2json" class="op csv2json row align-items-center">
               <!-- Validity Icon -->
                <div id="csv2jsonStatus" class="op-status csv2json">
                  <img id="csv2jsonValid" class="op-status-img csv2json" alt="CSV file conversion successful" src="/img/form/valid.png">
                  <img id="csv2jsonInvalid" class="op-status-img csv2json d-none" alt="CSV file conversion failed" src="/img/form/invalid.png">
                </div>
               <!-- Title -->
                <span class="op-name csv2json ml-1">CSV to JSON</span>
               <!-- Progress -->
                <div class="progress flex-fill ml-2">
                  <div id="csv2jsonProgress" class="progress-bar"
                        role="progressbar"
                       style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">
                  </div>
                </div>
              </div>
             <!-- SANITIZE -->
              <div id="sanitize" class="op sanitize row align-items-center">
               <!-- Validity Icon -->
                <div id="sanitizeStatus" class="op-status sanitize">
                  <img id="sanitizeValid" class="op-status-img sanitize" alt="Field sanitization successful" src="/img/form/valid.png">
                  <img id="sanitizeInvalid" class="op-status-img sanitize d-none" alt="Field sanitization failed" src="/img/form/invalid.png">
                </div>
               <!-- Title -->
                <span class="op-name sanitize ml-1">Sanitization</span>
               <!-- Progress -->
                <div class="progress flex-fill ml-2">
                  <div id="sanitizeProgress" class="progress-bar"
                        role="progressbar"
                       style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">
                  </div>
                </div>
              </div>
             <!-- AUTOID -->
              <div id="autoid" class="op autoid row align-items-top">
               <!-- Validity Icon -->
                <div id="autoidStatus" class="op-status autoid">
                  <img id="autoidValid" class="op-status-img autoid" alt="AutoID successful" src="/img/form/valid.png">
                  <img id="autoidInvalid" class="op-status-img autoid d-none" alt="AutoID failed" src="/img/form/invalid.png">
                </div>
               <!-- Title -->
                <span class="op-name autoid ml-1">AutoID</span>
               <!-- Action List -->
                <div class="flex-fill ml-2 op-message">
                  <ul id="autoidList"></ul>
                </div>
              </div>
             <!-- CLEAN -->
              <div id="cleaning" class="op cleaning row align-items-center">
               <!-- Validity Icon -->
                <div id="cleaningStatus" class="op-status cleaning">
                  <img id="cleaningValid" class="op-status-img cleaning" alt="Cleaning successful" src="/img/form/valid.png">
                  <img id="cleaningInvalid" class="op-status-img cleaning d-none" alt="Cleaning failed" src="/img/form/invalid.png">
                  <img id="cleaningSkip" class="op-status-img cleaning d-none" alt="Cleaning Skipped" src="/img/form/skip.png">
                </div>
               <!-- Title -->
                <span class="op-name cleaning ml-1">Cleaning</span>
               <!-- Progress -->
                <div class="progress flex-fill ml-2">
                  <div id="cleaningProgress" class="progress-bar"
                        role="progressbar"
                       style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">
                  </div>
                </div>
              </div>
             <!-- VALIDATE -->
              <div id="validate" class="op validate row align-items-center">
               <!-- Validity Icon -->
                <div id="validateStatus" class="op-status validate">
                  <img id="validateValid" class="op-status-img validate" alt="Validation successful" src="/img/form/valid.png">
                  <img id="validateInvalid" class="op-status-img validate d-none" alt="Validation failed" src="/img/form/invalid.png">
                  <img id="validateSkip" class="op-status-img validate d-none" alt="Validation Skipped" src="/img/form/skip.png">
                </div>
               <!-- Title -->
                <span class="op-name validate ml-1">Validation</span>
               <!-- Progress -->
                <div class="progress flex-fill ml-2">
                  <div id="validateProgress" class="progress-bar"
                        role="progressbar"
                       style="width: 0%;" aria-valuenow="0" aria-valuemin="0" aria-valuemax="0">
                  </div>
                </div>
              </div>
             <!-- ERROR -->
              <div id="console-error" class="row d-none">
                <div class="container-fluid">
                  <h2 class="mb-0 text-center form-subheader mt-2 console-error-header">Error</h2><hr>
                </div>
                <div id="console-error-text" class="flex-fill justify-content-center">
                  <p id="console-error-message" class="console error-message"></p>
                </div>
              </div>
            </div>

           <!-- File Output -->
            <div id="output-container" class="container-fluid mb-2 d-none">
              <h3 class="mb-0 text-center form-subheader mt-3">Files</h3><hr>
              <div id="file-output-container" class="row mt-2 d-flex justify-content-center">

               <!-- Clean CSV -->
                <a id="cleaned-csv" class="download-link mx-1" href="" alt="Download" download="">
                  <img id="cleaned-csv-icon" class="download-link-icon" src="/img/file/cleaned-csv.png" alt="Download">
                </a>
               <!-- Clean JSON -->
                <a id="cleaned-json" class="download-link mx-1" href="" alt="Download" download="">
                  <img id="cleaned-json-icon" class="download-link-icon" src="/img/file/cleaned-json.png" alt="Download">
                </a>
               <!-- Report TXT -->
                <a id="report" class="download-link mx-1" href="" alt="Download" download="">
                  <img id="report-icon" class="download-link-icon" src="/img/file/report.png" alt="Download">
                </a>
                <!-- Stats CSV -->
                <a id="stats" class="download-link mx-1" href="" alt="Download" download="">
                  <img id="stats-icon" class="download-link-icon" src="/img/file/stats.png" alt="Download">
                </a>
               <!-- Logs CSV -->
                <a id="logs" class="download-link mx-1" href="" alt="Download" download="">
                  <img id="logs-icon" class="download-link-icon" src="/img/file/logs.png" alt="Download">
                </a>

              </div>
             <!-- ERROR -->
              <div id="output-error" class="row d-none">
                <div class="container-fluid">
                  <h2 class="mb-0 text-center form-subheader mt-2 output-error-header">Output Error</h2><hr>
                </div>
                <div id="output-error-text" class="flex-fill justify-content-center">
                  <p id="output-error-message" class="output error-message"></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="csv" class="d-none"></div>
    <div id="cols" class="d-none"></div>
  </div>
<!-- JavaScript -->
  <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
  <script src="../vendor/js/jquery/jquery-3.3.1.min.js"></script>
  <script src="../vendor/js/bootstrap/bootstrap-4.1.0.min.js"></script>
  <script src="../vendor/js/popper/popper.min.js"></script>
  <script src="../vendor/js/jquery/jquery-ui.min.js"></script>
  <!-- <script src="./dist/js/app/interface.min.js"></script> -->
  <script src="../dist/js/app/interface.js"></script>