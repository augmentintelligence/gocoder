<!doctype html>
<html lang="en">
<head>
  <!--Meta: Required-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, height=device-height, initial-scale=0.6, shrink-to-fit=no, maximum-scale=0.6, minimum-scale=0.6, user-scalable=no">
  <!--Meta: Optional-->
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="apple-touch-icon" sizes="57x57" href="./favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="./favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="./favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="./favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="./favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="./favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="./favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="./favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="./favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="./favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="./favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="./favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="./favicon/favicon-16x16.png">
    <link rel="manifest" href="./manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="./favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
  <!--Page Information-->
    <title>GOcoder - Government of Ontario Geocoder</title>
  <!--Stylesheets-->
    <link rel="stylesheet" type="text/css" media="screen" href="./dist/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" media="screen" href="./dist/css/app/main.css">
    <link rel="stylesheet" type="text/css" media="screen" href="./dist/css/app/login.css">
    <link rel="stylesheet" type="text/css" media="screen" href="./dist/css/app/interface.css">
</head>
<body class="container-fluid p-0">
  <nav class="navbar navbar-dark navbar-expand-md">
    <span class="navbar-brand h1 m-0">
      <a href="/" alt="Home"><img src="./img/logo/48/trillium/wht.png" class="navbar-brand-img" width="32" height="32"></a>
      <span class="nav-title-text">GOcoder</span>
    </span>
  </nav>
    <?php
    $user = ""; // Prevent the "no index" error from $_POST.
    $pass = "";
    if (isset($_POST['user'])) { // Check for them and set them so.
      $user = $_POST['user'];
    }
    if (isset($_POST['pass'])) { // So that they don't return errors.
      $pass = $_POST['pass'];
    }

    $useroptions = ['cost' => 8,]; // All up to you.
    $pwoptions   = ['cost' => 8,]; // All up to you.
    $userhash    = password_hash($user, PASSWORD_BCRYPT, $useroptions); // Hash entered user.
    $passhash    = password_hash($pass, PASSWORD_BCRYPT, $pwoptions);  // Hash entered pw.
    $hasheduser  = file_get_contents("./php/private/user.txt"); // This is our stored user.
    $hashedpass  = file_get_contents("./php/private/pass.txt"); // And our stored password.


    if ((password_verify($user, $hasheduser)) && (password_verify($pass, $hashedpass))) {
      // The password verify is how we actually login here.
      // the $userhash and $passhash are the hashed user-entered credentials
      // password verify now compares our stored user and pw with entered user and pw.
      include "./php/private/index.php";

      // If it was invalid it'll just display the form, if there was never a $_POST
      // then it'll also display the form. that's why I set $user to "" instead of a $_POST
      // this is the right place for comments, not inside html.
    } else { ?>

    <section id="secureLoginBody" class="container-fluid">
      <div class="row justify-content-center">
        <div id="main" class="col-10 col-sm-7">
          <h4 class="pr-3">Restricted Access</h4>
          <hr>
          <form method="POST" action="./index.php" class="needs-validation" novalidate>
            <div class="form-group">
              <input type="text" name="user" class="form-control" id="inputUsername" aria-describedby="usernameHelp" placeholder="Username" value="<?php if (isset($_POST['user'])) { echo $_POST['user']; } ?>" required></input>
              <div class="invalid-feedback">
                Please provide a valid username.
              </div>
            </div>
            <div class="form-group">
              <input type="password" name="pass" class="form-control <?php if (isset($_POST['user']) && isset($_POST['pass'])) { ?> is-invalid <?php } ?>" id="inputPassword" placeholder="Password" required>
              <div class="invalid-feedback">
                <?php if (isset($_POST['user']) && isset($_POST['pass'])) { ?>
                  The username and/or password provided was invalid.
                <?php } else { ?>
                  Please provide a password.
                <?php } ?>
              </div>
            </div>
            <button type="submit" name="submit" class="btn btn-primary mx-auto">
              Log In
            </button>
          </form>
        </div>
      </div>
    </section>
    <!--JavaScript-->
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script> -->
    <script src="./vendor/js/jquery/jquery-3.3.1.min.js"></script>
    <script src="./dist/js/vendor/vendor.min.js"></script>
    <script src='./vendor/js/jquery/jquery-ui.min.js'></script>
    <!-- <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCbBI6VMeEB3f2uRemX9lHZgpquuF89VOM"></script> -->
    <!-- <script src="./dist/js/app/secure.js"></script> -->
    <?php } ?>
</body>
</html>