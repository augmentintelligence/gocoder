(function(){

  const async = require('async');
  // Imports - Application
  const scan = require('./src/scan'),
       clean = require('./src/clean'),
    validate = require('./src/validate'),
     compile = require('./src/compile');
  const csvtojson = require('./src/csvtojson'),
        jsontocsv = require('./src/jsontocsv');
  const writeFiles = require('./src/writefiles');

  let exports = {};
  const csvExt = /\.csv$/i;

  exports.fileNames = fileNames;
  function fileNames ( filename ) {
    return {
         filename: filename,
         cleanCSV: filename.replace( csvExt, '-clean.csv' ),
        cleanJSON: filename.replace( csvExt, '-clean.json' ),
      cleanReport: filename.replace( csvExt, '-report.txt'),
         cleanLog: filename.replace( csvExt, '-log.csv'),
       cleanStats: filename.replace( csvExt, '-stats.json')
    };
  }

  exports.execute = execute;
  function execute ( csv, cols, options, filenames ) {
    let waterfallChain = [ scan, clean, validate, compile, jsontocsv ];
    waterfallChain.unshift( csvtojson( csv, cols, options ) );
    async.waterfall(
      waterfallChain,
      writeFiles( filenames )
    );
  }


  module.exports = exports;

}).call(this);