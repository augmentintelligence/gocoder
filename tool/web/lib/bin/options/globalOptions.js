// Load Core Function
const targetFieldsFilter = require('../../../../core/lib/bin/options/lib/targetFieldsFilter');

const GlobalOptions = function ( json, options ) {

    this.caps = typeof options.caps === 'boolean' ? options.caps :
      !( json.some (
        row => Object.keys( row ).some(
          col => /[a-z]+/.test( row[ col ] )
      )
    ));

    this.fields = options.fields;
    this.targetFields = targetFieldsFilter( this.fields );
    this.validate = options.validate;
    this.clean = options.clean;
    this.casing = options.casing;
    this.postalCodeLimit = options.hasOwnProperty('postalCodeLimit') ? options.postalCodeLimit : undefined;

};

module.exports = GlobalOptions;