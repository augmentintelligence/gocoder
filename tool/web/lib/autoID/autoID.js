const uniqueID = require( '../../../core/lib/autoID/uniqueID' );

/**
 * Given a json-like array representing rows of data from a CSV, Global Options (`gOptions`) generated
 * from scanning the input CSV-as-JSON file, and optionally -
 * the preferred name for a primary key field, and a starting integer for the autoID:
 * Determines whether gOptions.PrimaryKey is unique, if it isn't, preserves the values in it by different
 * means depending on the supplied arguments and forces the PrimaryKey value to be unique.
 *
 * @param {Object[]} json -
 *  A JSON-like array of repeating objects representing rows in a table.
 * @param {gOptions} gOptions -
 *  A Global-Options object generated from the preceding `json` argument.
 * @param {string} [keyPref] -
 *  Optionally define the preferred ID field name, regardless of the current ID mapped in gOptions.
 * @param {number} [start] -
 *  The number to start from for the incrementing autoID.
 */
const autoID = function( json, gOptions, cols, keyPref, start, verbose ) {

  // Show AutoID Console Section
  document.getElementById( 'autoid' ).classList.add( 'show' );

  // Get target ID field from Global Options
  let idField = gOptions.fields.PrimaryKey;

  // Coerce valid starting integer for autoID
  start = start === undefined || typeof start !== 'number' ? 1 : Math.floor( start );

  if ( idField !== '' ) {
    // 1. If target ID is unique...
    if ( uniqueID( json, idField ) ) {
      if ( verbose ) {
        addListItem(`${fieldName(idField)} ${strAccent('is',true)} unique`, true);
      }
      // ... Target ID matches preferred ID name or no ID is preferred..
      //    ~~> No action necessary
      if ( keyPref === undefined || typeof keyPref !== 'string' || idField === keyPref ) {
        if (verbose) {
          if ( keyPref === undefined || typeof keyPref !== 'string' )
            addListItem(`Preferred key ${strAccent('is not')} defined`);
          else
            addListItem(`${fieldName(idField)} ${strAccent('is',true)} preferred key`);
          addListItem(`IDs ${strAccent('not')} generated`);
        } return cols;
      } else {
        // ... Target ID does not match preferred ID name ...
        if ( verbose ) {
          addListItem(`${fieldName(idField)} ${strAccent('is not')} preferred key (${fieldName(keyPref)})`);
        }

        // ~~> Change Global Options PK Map
        gOptions.fields.PrimaryKey = keyPref;

        //  ... preferred ID name does not exist in object ...
        if ( !json[0].hasOwnProperty( keyPref ) ) {
          if ( verbose ) {
            addListItem(`${fieldName(keyPref)} ${strAccent('does not')} exist`);
            addListItem(`Renaming ${fieldName(idField)} to ${fieldName(keyPref)}`);
          }

          json.forEach( ( row, i ) => {
            let newRow = {};
            newRow[ keyPref ] = row[ idField ];
            for ( let col of cols ) {
              if ( col !== idField ) newRow[ col ] = row[ col ];
            }
            json[i] = newRow;
          });
          //-----------------------------------------------------------------------------------------------------------------------------
          // THE BELOW MUST BE WHERE IT IS SO THAT THE ROW DOESN'T GET OVERWRITTEN
          // Rename idField in Columns Array
          cols = cols.slice( 0, cols.indexOf( idField ) ).concat( keyPref, cols.slice( cols.indexOf( idField ) + 1 ) );
          //-----------------------------------------------------------------------------------------------------------------------------

          if ( verbose ) {
            addListItem(`Remapped Primary Key to ${fieldName(keyPref)}`);
            addListItem(`IDs ${strAccent('not')} generated`);
          }

        // ...Preferred ID exists in object ...
        //   ... and it is unique ...
        } else if ( uniqueID( json, keyPref ) ) {
          if ( verbose ) {
            addListItem(`${fieldName(keyPref)} exists`);
            addListItem(`${fieldName(keyPref)} ${strAccent('is', true)} unique`);
            addListItem(`Remapped Primary Key to ${fieldName(keyPref)}`);
            addListItem(`IDs ${strAccent('not')} generated`);
          } return cols;
        //   ... and it is not unique ...
        } else {
          if ( verbose ) {
            addListItem(`${fieldName(keyPref)} exists` );
            addListItem(`${fieldName(keyPref)} ${strAccent('is not')} unique`);
            addListItem(`Preserving ${fieldName(keyPref)} values under ${fieldName('Original_'+keyPref)}`);
            addListItem(`Remapping Primary Key to ${fieldName(keyPref)}`);
            addListItem(`Generating AutoIDs for ${fieldName(keyPref)}`);
          }

          json.forEach( ( row, i ) => {
            let newRow = {};
            newRow[ keyPref ] = start++;
            newRow[ 'Original_' + keyPref ] = row[ keyPref ];
            for ( let col of cols ) {
              if ( col !== keyPref ) newRow[ col ] = row[ col ];
            }
            json[i] = newRow;
          });
          //-----------------------------------------------------------------------------------------------------------------------------
          // THE BELOW *MUST* COME AFTER THE ABOVE OR ELSE Original_xxx WILL BE undefined && CAUSE ERRORS.
          // Add Original_keyPref after keyPref in Columns Array
          cols = cols.slice( 0, cols.indexOf( keyPref ) + 1 ).concat( 'Original_' + keyPref, cols.slice( cols.indexOf( keyPref ) + 1 ) );
          //-----------------------------------------------------------------------------------------------------------------------------
        }
      }
    // 2. If target ID is NOT unique...
    } else {
      if ( verbose ) {
        addListItem(`${fieldName(idField)} ${strAccent('is not')} unique`, true );
      }

      // ... target ID === preferred ID name
      if ( idField === keyPref ) {
        if ( verbose ) {
          addListItem(`${fieldName(idField)} ${strAccent('is', true)} preferred key`);
          addListItem(`Preserving ${fieldName(keyPref)} values under ${fieldName('Original_'+keyPref)}`);
          addListItem(`Generating AutoIDs for ${fieldName(keyPref)}`);
        }

        json.forEach( ( row, i ) => {
          let newRow = {};
          newRow[ keyPref ] = String( start++ );
          newRow[ 'Original_' + keyPref ] = row[ keyPref ];
          for ( let col of cols ) {
            if ( col !== keyPref ) newRow[ col ] = row[ col ];
          }
          json[ i ] = newRow;
        });
        //-----------------------------------------------------------------------------------------------------------------------------
        // THE BELOW *MUST* COME AFTER THE ABOVE OR ELSE Original_xxx WILL BE undefined && CAUSE ERRORS.
        // Add Original_keyPref after keyPref in Columns Array
        cols = cols.slice( 0, cols.indexOf( keyPref ) + 1 ).concat( 'Original_' + keyPref, cols.slice( cols.indexOf( keyPref ) + 1 ) );
        //-----------------------------------------------------------------------------------------------------------------------------
        //!
        /* if ( verbose )
            con.innerText = currentCon + '\n' + lns.map( ln => ln + `[${iTotal}/${iTotal}][OK]`).join('\n'); */

      // ... target ID !== preferred ID ...
      } else {
        // ~~> Change Global Options PK Map
        gOptions.fields.PrimaryKey = keyPref;

        if ( verbose ) {
          addListItem(`${fieldName(idField)} ${strAccent('is not')} preferred key`);
          addListItem(`Remapping Primary Key to ${fieldName(keyPref)}`);
        }
        // ... preferred ID does not exist ...
        if ( !json[0].hasOwnProperty( keyPref ) ) {

          if ( verbose ) {
            addListItem(`${fieldName(keyPref)} ${strAccent('does not')} exist`);
            addListItem(`Generating AutoIDs for ${fieldName(keyPref)}`);
          }

          json.forEach( ( row, i ) => {
            let newRow = {};
            newRow[ keyPref ] = start++;
            for ( let col of cols ) {
              newRow[ col ] = row[ col ];
            }
            json[i] = newRow;
          });

          //-----------------------------------------------------------------------------------------------------------------------------
          // THE BELOW *MUST* COME AFTER THE ABOVE OR ELSE ID will be undefined && CAUSE ERRORS.
          // Prepend Columns array with keyPref
          cols = [ keyPref ].concat( cols );
          //-----------------------------------------------------------------------------------------------------------------------------

        // ... preferred ID exists ...
        } else {
          if ( verbose ) {
            addListItem(`${fieldName(keyPref)} exists`);
          }
          // ... it is unique ...
          if ( uniqueID( json, keyPref) ) {
            // ~~> Do nothing, options changed
            if ( verbose ) {
              addListItem(`${fieldName(keyPref)} ${strAccent('is',true)} unique`);
              addListItem(`IDs ${strAccent('not')} generated`);
            }
          }
          // ... it is not unique ...
          else {
            if (verbose) {
              addListItem(`${fieldName(keyPref)} ${strAccent('is not')} unique`);
              addListItem(`Preserving ${fieldName(keyPref)} values under ${fieldName('Original_'+keyPref)}`);
              addListItem(`Generating AutoIDs for ${fieldName(keyPref)}`);
            }

            json.forEach( (row, i) => {
              let newRow = {};
              newRow[ keyPref ] = start++;
              newRow[ 'Original_' + keyPref ] = row[ keyPref ];
              for ( let col of cols ) {
                if ( col !== keyPref ) newRow[ col ] = row[ col ];
              }
              json[i] = newRow;
            });
            //-----------------------------------------------------------------------------------------------------------------------------
            // THE BELOW *MUST* COME AFTER THE ABOVE OR ELSE ID will be undefined && CAUSE ERRORS.
            // Add Original_keyPref after keyPref in Columns Array
            cols = cols.slice( 0, cols.indexOf( keyPref ) + 1 ).concat( 'Original_' + keyPref, cols.slice( cols.indexOf( keyPref ) + 1 ) );

          }
        }
      }
    }
  } else {
    // [WEB] Primary Key is Not Defined

    if (verbose) {
      addListItem(`Primary Key ${strAccent('is not')} mapped`,true);
      addListItem(`Mapping Primary Key to ${fieldName(keyPref)}`);
    }

    // ~~> Change Global Options PK Map
    gOptions.fields.PrimaryKey = keyPref;


    if (!(json[0].hasOwnProperty(keyPref))) {
      if (verbose) {
        addListItem(`${fieldName(keyPref)} ${strAccent('does not')} exist`);
        addListItem(`Generating AutoIDs for ${fieldName(keyPref)}`);
      }
      json.forEach( (row,i) => {
        let newRow = {};
        newRow[keyPref] = start++;
        for ( let col of cols ) {
          newRow[col] = row[col];
        }
        json[i] = newRow;
      });
      //-----------------------------------------------------------------------------------------------------------------------------
      // THE BELOW *MUST* COME AFTER THE ABOVE OR ELSE ID will be undefined && CAUSE ERRORS.
      // Prepend Columns array with keyPref
      cols = [ keyPref ].concat( cols );
    }
    // It exists...
    else {
      if (verbose) {
        addListItem(`${fieldName(keyPref)} ${strAccent('does', true)} exist`);
      }
      // ... it is unique ...
      if ( uniqueID( json, keyPref) ) {
        if (verbose) {
          addListItem(`${fieldName(keyPref)} ${strAccent('is',true)} unique`);
          addListItem(`IDs ${strAccent('not')} generated`);
        }
      }
      // ... it is not unique ...
      else {
        if (verbose) {
          addListItem(`${fieldName(keyPref)} ${strAccent('is not')} unique`);
          addListItem(`Preserving ${fieldName(keyPref)} values under ${fieldName('Original_'+keyPref)}`);
          addListItem(`Generating AutoIDs for ${fieldName(keyPref)}`);
        }
        json.forEach( (row, i) => {
          let newRow = {};
          newRow[ keyPref ] = start++;
          newRow[ 'Original_' + keyPref ] = row[ keyPref ];
          for ( let col of cols ) {
            if ( col !== keyPref ) newRow[ col ] = row[ col ];
          }
          json[i] = newRow;
        });
        //-----------------------------------------------------------------------------------------------------------------------------
        // THE BELOW *MUST* COME AFTER THE ABOVE OR ELSE ID will be undefined && CAUSE ERRORS.
        // Add Original_keyPref after keyPref in Columns Array
        cols = cols.slice( 0, cols.indexOf( keyPref ) + 1 ).concat( 'Original_' + keyPref, cols.slice( cols.indexOf( keyPref ) + 1 ) );
      }
    }
  }

  return cols;
};

function addListItem( itemHTML, noHeight ) {
  let container = document.getElementById( 'autoid' );
  if ( noHeight === false || noHeight === undefined ) {
    let height = container.getAttribute( 'style' )
                          .replace( 'height: ', '' )
                          .replace( 'px;', '' );
    height = Number.parseInt(height) + 26;
    container.setAttribute( 'style', 'height: ' + height + 'px;' );
  } else { container.setAttribute( 'style', 'height: 38px;' ); }
  let li = document.createElement('li');
  document.getElementById( 'autoidList' ).appendChild( li );
  li.innerHTML = itemHTML;
}

function fieldName( name ) { return `<span class="console-field-name">${name}</span>`; }
function strAccent( str, isPositive ) { return `<span class="console-text-${(isPositive?'positive':'negative')}">${str}</span>`; }

module.exports = autoID;