const XRegExp = require( 'xregexp' );
const illegals = XRegExp.build(
  `(?i)(?:{{A}}|{{B}}|{{C}}|{{D}}|{{E}}|{{F}}|{{G}}|{{I}}|{{L}}|{{N}}|{{O}}|{{S}}|{{T}}|{{U}}|{{V}}|{{W}})`,
  {
    A: /\bA(?:[ND]D|LTER)\b/,
    B: /\bBY\b/,
    C: /\bC(?:OLUMN|REATE)\b/,
    D: /\bD(?:ELETE|ROP)\b/,
    E: /\bEXISTS\b/,
    F: /\bF(?:OR|ROM)\b/,
    G: /\bGROUP\b/,
    I: /\bI(?:N(?:SERT|TO)?|S)\b/,
    L: /\bLIKE\b/,
    N: /\bN(?:OT|ULL)\b/,
    O: /\bOR(?:DER)?\b/,
    S: /\bSE(?:(?:LEC)?T)\b/,
    T: /\bTABLE\b/,
    U: /\bUPDATE\b/,
    V: /\bVALUES\b/,
    W: /\bWHERE\b/
  }
);

/**
 * Modifies the object passed so that
 * all of its property keys do not violate
 * database standards.
 **/
function sanitizeFields( obj ) {
  Object.keys( obj )
    .forEach( key => {
      if ( sanitizeField( key ) !== key ) {
        obj[ sanitizeField( key ) ] = obj[ key ];
        delete obj[ key ];
      }
    });
}

/**
 * Given an object, ensures that the field names do not
 * use reserved words. Returns `true` if they all pass.
 * @param {*[]} obj -
 *  An object representing tabular data as key/value pairs.
 * @returns {boolean} -
 *  `False` if any field uses reserved words.
 */
function legalFields( obj ) {
  return Object.keys( obj ).every( key => !illegals.test( key ) );
}

/**
 * Given a string representing a field name,
 * sanitizes it for use in database schemas.
 **/
function sanitizeField( str ) {
  return typeof str === 'string' ? str
    .replace(/^[\d_]+/,'')      // Cannot begin with number/_
    .replace(/[-\s\.]+/g,'_')   // Convert spaces/-/. ~> _
    .replace(/[^A-Z\d_]/ig,'') : // Remove non-alphanumerics, _
    undefined;
}

module.exports = {
   field: sanitizeField,
  fields: sanitizeFields,
  fieldsLegal: legalFields
};