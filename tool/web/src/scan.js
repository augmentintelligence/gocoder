const GlobalOptions = require('../lib/bin/options/globalOptions');
const autoID = require( '../lib/autoID/autoID' );
const sanitize = require( '../lib/sanitizeFields/sanitizeFields' );

// UI
const Progress = require( '../../../src/js/lib/progress/BSProgress' ),
            ft = require( '../../../src/js/lib/formTools/formTools' );

const scan = function ( json, options, cols, callback ) {
  let p, gOptions;

  // Sanitization
  try {
    // Create GlobalOptions
    gOptions = new GlobalOptions( json, options );

    /* UI */
      // Init Progress
      p = new Progress( document.getElementById( 'sanitizeProgress' ), json.length );
      p.init();
      // Show Sanitization Section
      document.getElementById( 'sanitize' ).classList.add( 'show' );
      // Update 'cols'
      document.getElementById( 'cols' ).dataset.content = cols.map( sanitize.field );

    // Sanitize Fields in Data
    json.forEach( ( row, i ) => {
      p.update( i + 1 );
      sanitize.fields( row );
    });

    // Sanitize Fields in GlobalOptions
    Object.keys( gOptions.fields ).forEach( field => {
      gOptions.fields[ field ] = sanitize.field( gOptions.fields[field] );
    });

    // Sanitize TargetFields in GlobalOptions
    gOptions.targetFields = gOptions.targetFields.map( field => sanitize.field( field ) );

    // Ensure field names are legal
    if ( !sanitize.fieldsLegal( json[ 0 ] ) )
      throw new Error('Error: Fields contain reserved words.');

    // Update Status Icon
    ft.statusImg( '.op-status-img.sanitize', 'sanitizeValid' );

  }
  catch ( e ) {

    // Update Status Icon
    ft.statusImg( '.op-status-img.sanitize', 'sanitizeInvalid' );

    // Display Error Message
    document.getElementById( 'console-error' ).classList.remove( 'd-none' );
    document.getElementById( 'console-error-message' ).innerText = 'Field names contain database reserved words.';

    // Handle Rejection
    callback( e );

  }

  // AutoID
  try {

    // Run AutoID
    cols = autoID( json, gOptions, cols, 'MOH_SERVICE_PROVIDER_IDENT', 9999999990, true );

    // Update Status Icon
    ft.statusImg( '.op-status-img.autoid', 'autoidValid' );

    // Move on to Cleaning
    callback( null, json, gOptions, cols );

  }
  catch ( e ) {

    // Update Status Icon
    ft.statusImg( '.op-status-img.autoid', 'autoidValid' );

    // Display Error Message
    document.getElementById( 'console-error' ).classList.remove( 'd-none' );
    document.getElementById( 'console-error-message' ).innerText = e.message;

    // Handle Rejection
    callback( e );

  }

};

module.exports = scan;