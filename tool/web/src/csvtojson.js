const csvtojson = require('../node_modules_override/csvtojson/browser/browser');
const Progress = require( '../../../src/js/lib/progress/BSProgress' );
const ft = require( '../../../src/js/lib/formTools/formTools' );

module.exports = function( csv, cols, options, callback ) {
  return function( callback ) {
    let rows = [];
    document.getElementById( 'csv2json' ).classList.add( 'show' );

    // Run Transformer
    csvtojson( { noheader: true, output: "csv" } )
      .fromString( csv )
      .then( csvRows => {
        let rTotal = csvRows.length;
        let p = new Progress( document.getElementById('csv2jsonProgress'), rTotal );
        p.init();
        for ( let r = 0; r < rTotal; r++ ) {
          p.update( r + 1 );
          let row = {};
          for ( let c = 0; c < cols.length; c++ ) {
            row[cols[c]] = csvRows[r][c];
          }
          rows.push( row );
        }
        ft.statusImg( '.op-status-img.csv2json', 'csv2jsonValid' );
        callback( null, rows, options, cols );
      })
      .catch( err => {
        ft.statusImg( '.op-status-img.csv2json', 'csv2jsonInvalid' );
        document.getElementById( 'console-error' ).classList.remove( 'd-none' );
        document.getElementById( 'console-error-message' ).innerText = err.message;
        throw err;
      });
  };
};