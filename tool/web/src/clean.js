const  Row = require('../../core/lib/row/Row'),
  groupExe = require('../../core/lib/regex/patternGroup'),
  exeChain = require('../../core/lib/bin/exechain/exechain'),
coerceName = require('../../core/lib/coerceName/coerceName' ),
   OTHER_1 = require('../../core/lib/regex/patterns/other/_other1'),
   OTHER_2 = require('../../core/lib/regex/patterns/other/_other2'),
     POSTC = require('../../core/lib/regex/patterns/postalcode/_postalcode'),
       SST = require('../../core/lib/regex/patterns/siteServiceType/_sst' );

// Interface
const Progress = require( '../../../src/js/lib/progress/BSProgress' ),
            ft = require( '../../../src/js/lib/formTools/formTools' );

const Mutator = require('../../core/lib/regex/classes/Mutators');

const dataClean = function ( json, opts, cols, callback ) {

  // Show Cleaning in Console
  document.getElementById( 'cleaning' ).classList.add( 'show' );

  // Create Progress
  const p = new Progress( document.getElementById( 'cleaningProgress' ), json.length );

  // No Cleaning Option is set
  if ( !opts.clean ) {
    try {

      // Create Rows
      json = json.map( row => new Row( opts, row ) );

      // Do Skip Progress
      p.skip();
      ft.statusImg( '.op-status-img.cleaning', 'cleaningSkip' );

      // Move to Validation
      callback( null, json, cols );

    }
    catch( e ) {

      // Update Progress
      p.error();
      ft.statusImg( '.op-status-img.cleaning', 'cleaningInvalid' );

      // Display Error
      consoleError( 'Unable to create Row objects out of data. ' + e.message );

      // Handle Error
      callback( e );
    }

  }
  else { // Otherwise...
    try {

      // Init Progress
      p.init();

      let ROWS = [];

      // Initialize Caser
      Mutator.Caser = new Mutator.Casers( opts.caps );

      // Loop through rows
      for ( let idx = 0, len = json.length; idx < len; idx++ ) {

        let row = json[ idx ];
        row = new Row( opts, row );

        groupExe( POSTC, row, opts.fields.PostalCode );

        // Option to restrict Postal Codes
        if ( opts.postalCodeLimit && opts.postalCodeLimit.source === '[KLMNP]' ) {
          let localPostalFocal = new Mutator.Cleaner(
            'LocalPostal',
            'Removed out-of-bounds Postal Code',
            /^[^KLMNP].+/ig
          );
          localPostalFocal.exec( row, [ opts.fields.PostalCode ] );
        }

        for ( let g of exeChain ) {
          groupExe( g, row, [ opts.fields.StreetAddress, opts.fields.AddressLine2 ] );
        }

        groupExe( OTHER_1, row, opts.fields.StreetAddress );
        groupExe( OTHER_2, row, opts.fields.AddressLine2 );

        row.appendShadows( opts.fields.StreetAddress, [ 'Street' ] );

        if ( row.shadows.includes( 'RR' ) ) {
          if ( row.valueOf( opts.fields.StreetAddress ) == '')
            row.appendShadows( opts.fields.StreetAddress, [ 'RR' ]);
          else
            row.appendShadows( opts.fields.AddressLine2, [ 'RR' ]);
        }

        row.appendShadows( opts.fields.AddressLine2,
          [
            'Lot',
            'Concession',
            'Unit',
            'Suite',
            'Building',
            'Section',
            'Wing',
            'Level',
            'Floor',
            'Room',
            'Postal_Box',
            'Postal_Bag',
            'Code_Postal',
            'Reservation',
            'Island',
            'Other_1',
            'Other_2'
          ]);

        Mutator.Caser.exec( row, [ opts.fields.Name, opts.fields.City ]);
        coerceName( row, opts );

        new Mutator.Expander('Health',   /\bHLTH\b/i)
          .exec( row, opts.fields.Name );
        new Mutator.Expander('Pharmacy', /\bPhm\b|\bPharmcy\b/i)
          .exec( row, [ opts.fields.Name, opts.fields.AddressLine2 ] );
        new Mutator.Expander('Hospital', /\bHosp\b\.?|\bHosptl\b/i)
          .exec( row, [ opts.fields.Name ]);
        new Mutator.Expander('Saint',    /\bSt\b\.?/i)
          .exec( row, opts.fields.City );

        if ( row.keys.includes( 'SITE_SERVICE_TYPE' ) )
          SST.exec( row, [ 'SITE_SERVICE_TYPE' ]);

        // Enforce Casing Option
        if ( !opts.caps ) Mutator.Caser.exec( row, opts.casing );

        // Update Progress
        p.update( idx + 1 );

        // Push cleaned row to rows array.
        ROWS.push( row );

      }
      // Update Icon
      ft.statusImg( '.op-status-img.cleaning', 'cleaningValid' );

      // Move to Validation
      callback( null, ROWS, cols );
    }
    catch( e ) {

      // Update Progress
      p.error();
      consoleError( e.message );
      ft.statusImg( '.op-status-img.cleaning', 'cleaningInvalid' );

      // Handle Error
      callback( e );

    }
  }
};

function consoleError( msg ) {
  document.getElementById( 'console-error' ).classList.remove( 'd-none' );
  document.getElementById( 'console-error-message' ).innerText = msg;
}

module.exports = dataClean;