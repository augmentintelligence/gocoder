const Json2csvParser = require('json2csv').Parser;


module.exports = function( [ rows, report, logs, stats, cols ], callback ) {
  const fieldsCSV = cols;
  const optsCSV = { fieldsCSV };
  const fieldsLogs = Object.keys(logs[0]);
  const optsLogs = { fieldsLogs };
  const csvParser = new Json2csvParser(optsCSV);
  const logsParser = new Json2csvParser(optsLogs);

  const csv = csvParser.parse( rows );
       logs = logsParser.parse( logs );

  callback(null, [ rows, csv, report, logs, stats, cols ]);
};