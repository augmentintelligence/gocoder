module.exports = function ( answers, err, res ) {
 /* Note:
  * The way that the execution chain is called, we pass the wrtieFiles
  * module the answers before the first function runs so that answers does
  * not need to be passed all the way through the chain only to be used here.
  */
  return function ( err, [ rows, csv, report, logs, stats, cols ] ) {
    if ( err ) { console.error( err ); }
    else {
      // Display File Output Container
      document.getElementById( 'output-container' ).classList.remove( 'd-none' );
      try {
        // Call the function to add them to the sub-container
        wrtieFiles([
          {
            name: answers.cleanCSV,
            data: csv,
            type: 'cleaned-csv',
            mime: 'csv',
          }, {
            name: answers.cleanJSON,
            data: JSON.stringify( rows ),
            type: 'cleaned-json',
            mime: 'json'
          }, {
            name: answers.cleanReport,
            data: report,
            type: 'report',
            mime: 'plain',
          }, {
            name: answers.cleanStats,
            data: JSON.stringify( stats ),
            mime: 'json',
            type: 'stats'
          }, {
            name: answers.cleanLog,
            data: logs,
            type: 'logs',
            mime: 'csv'
          }
        ]);
      }
      catch ( e ) {
        // Hide File Output Container
        document.getElementById( 'file-output-container' ).classList.add( 'd-none' );
        // Display Error Message
        document.getElementById( 'output-error' ).classList.remove( 'd-none' );
        document.getElementById( 'output-error-message' ).innerText = e.message;
      }
    }
  };
};

// For each file, append it as the component for it.
function wrtieFiles ( fileInfo ) {
  let timer = 50,
      delay = 75;
  for ( let i of fileInfo ) {
    // Generate Blob
    let blob = new Blob( [ i.data ], { type: `text/${i.mime};charset=utf-8` } );
    // Get Elements
    let a = document.getElementById( i.type ),
      img = document.getElementById( i.type + '-icon' );

    // Anchor Mod
    a.setAttribute( 'download', i.name );
    a.setAttribute( 'href', window.URL.createObjectURL( blob ) );
    a.setAttribute( 'alt', `Download ${ i.name }` );

    // Image Mod
    img.setAttribute( 'alt', `Download ${ i.name }` );

    window.setTimeout(() => {
      document.getElementById( i.type ).classList.add( 'show' );
    }, timer);
    timer += delay;
    delay *= 1.5 ;
  }
}