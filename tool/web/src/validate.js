const logCorrections = require( '../../core/lib/validator/logCorrections' );
const History = require('../../core/lib/history/History');
const ZERO_RESULTS = require('../../core/lib/validator/classes/ZERO_RESULTS');
const E500 = require('../../core/lib/validator/classes/E500');
const fill = require( '../../core/lib/validator/fill' );
const addressValidator = require('../lib/validator/address-validator/index'),
               Address = addressValidator.Address,
                     _ = require('underscore');
                //  async = require('async');

// Interface
const Progress = require( '../../../src/js/lib/progress/BSProgress' ),
            ft = require( '../../../src/js/lib/formTools/formTools' );

if (/gocoder\.(?:(?:mohltc\.net)|(?:healthdatascience\.ca))/.test(window.location.href)) {
  addressValidator.setOptions({
    key: "AIzaSyDytRL7mHwsYHiVJiGylOrWTxUJlW9_wYw"
  });
} else {
  addressValidator.setOptions({
    key: "AIzaSyAGcLS1l7m-1s8cVe10aAaGpmyT6rHCVJ0"
    // proxy: "http://204.40.130.129:3128"
  });
}

// streetAddress, route, city, state, country, unknown
//!TODO: Make a version that tries with match.streetAddress, and if it doesn't produce a lat/lon, try again with match.unknown

module.exports = function ( rows, cols, callback ) {

  // Show Validation in Console
  document.getElementById( 'validate' ).classList.add( 'show' );

  // Create Progress
  const p = new Progress( document.getElementById( 'validateProgress' ), rows.length );



  // No Validate Option is set
  if ( !rows[0].options.validate ) {

   // Add Changes to Cols
    cols.push( 'CHANGES' );

   // Do Skip Progress
    p.skip();
    ft.statusImg( '.op-status-img.validate', 'validateSkip' );

   // Move to compilation
    callback( null, rows, cols );

  }

  // Validate Enabled
  else {

    // Add Lat/Lon Fields to Cols
    cols.push( rows[0].options.fields.Lat, rows[0].options.fields.Lon );
    // Add Changes to Cols
    cols.push( 'CHANGES' );

    let promise = validateAll( rows, p );
    promise.then(
      function( result ) {

       // Update Icon
        ft.statusImg( '.op-status-img.validate', 'validateValid' );

       // Move to Compilation
        callback( null, rows, cols );

      }
    ).catch(
      function( e ) {

        // Update Progress
        p.error();

        // Display Error
        document.getElementById( 'console-error' ).classList.remove( 'd-none' );
        document.getElementById( 'console-error-message' ).innerText = e.message;

        // Update Icon
        ft.statusImg( '.op-status-img.validate', 'validateInvalid' );

        // Handle Rejection
        callback( e, [rows, cols] );
      }
    );
  }

};

function validateAll( rows, p ) {
  let deferred = $.Deferred();
  let i = 0;
  p.init();
  let nextStep = function() {
    if ( i < rows.length ) {
      let item = rows[i];
      p.update(i+1);
      addGeocodeFields( item );
      let address = makeAddress( item );
      if ( address === false ) {
        fill.missing( item, 'StreetAddress' );
        fill.missing( item, 'PostalCode' );
        i++;
        nextStep();
      } else {
        try {
          validate( rows, address, item, i++, nextStep );
        }
        catch ( e ) {
          fill.invalid( item, 'StreetAddress' );
          fill.invalid( item, 'PostalCode' );
          writeGeocode( item, item.options.fields.Lat, '' );
          writeGeocode( item, item.options.fields.Lon, '' );
          i++;
          nextStep();
        }
      }
    } else {
      deferred.resolve();
    }
  };
  nextStep();
  return deferred.promise();
}

function validate( rows, address, item, index, callback ) {
  addressValidator.validate (
    address,
    addressValidator.match.unknown,
    function( err, exact, inexact ) {
      // If Error is Timeout, retry
      if ( err && err instanceof E500 ) {
        validate( rows, address, item, index, callback );
      // Otherwise...
      } else {
        // If Error...
        if ( err ) {
          // Display Logs
          itemError( err, item );
          // If Zero Results...
          if ( err instanceof ZERO_RESULTS ) {
            fill.invalid( item, 'StreetAddress' );
            fill.invalid( item, 'PostalCode' );
            writeGeocode( item, item.options.fields.Lat, '' );
            writeGeocode( item, item.options.fields.Lon, '' );
            callback();
          }
          /* else throw ( err ); */
        // No Error
        } else {
          let match = _.map( exact, a => a.toString() ),
            noMatch = _.map( inexact, a => a.toString() ),
                  f = item.options.fields;

          // Exact Match
          if ( match != '' ) {

            // Logging
            console.log( `\n [${index+1}/${rows.length}] > ${item._name}: OK` );

            // Missing Postal Code + Available Postal Code
            if ( item.valueOf( f.PostalCode ) === '' && exact[0].postalCode ) {
              // Postal Code Limitation is Defined
              if ( item.options.postalCodeLimit ) {
                // Postal Code is Valid (in Ontario)
                if ( item.options.postalCodeLimit.test( exact[0].postalCode.charAt(0) ) ) {
                  fill.valid.PostalCode( item, exact[0].postalCode.replace(' ', '') );
                  writeGeocode( item, f.Lat, exact[0].location.lat );
                  writeGeocode( item, f.Lon, exact[0].location.lon );
                  callback();
                } else {
                  // Invalid Ontario Postal Code
                  fill.invalid( item, 'StreetAddress' );
                  fill.invalid( item, 'PostalCode' );
                  writeGeocode( item, f.Lat, '' );
                  writeGeocode( item, f.Lon, '' );
                  callback();
                }
              } else {
                // Postal Code Limitation not Defined
                fill.valid.PostalCode( item, exact[0].postalCode.replace( ' ', '' ) );
                writeGeocode( item, f.Lat, exact[0].location.lat );
                writeGeocode( item, f.Lon, exact[0].location.lon );
                callback();
              }
            } else {
              // Legacy Geocode
              writeGeocode( item, f.Lat, exact[0].location.lat );
              writeGeocode( item, f.Lon, exact[0].location.lon );
              callback();
            }

          // Inexact Match
          } else if ( noMatch != '' ) {

            let v_street_address = inexact[0].streetNumber ? inexact[0].streetNumber + ' ' : '';
            if ( item.options.caps ) {
              v_street_address += inexact[0].street ? inexact[0].street.toUpperCase() : '';
            } else {
              v_street_address += inexact[0].street ? inexact[0].street : '';
            }

            let v_community = inexact[0].city ?
              ( item.options.caps ? inexact[0].city.toUpperCase() : inexact[0].city ) : '';

            let corrections = [];

            // If Validation Street Address is not Empty & does not match input Street_Address...
            if ( v_street_address !== item.valueOf(f.StreetAddress) && v_street_address !== '' ) {

              // Missing Postal Code + Available Postal Code
              if ( item.valueOf( f.PostalCode ) === '' && inexact[0].postalCode ) {

                // Postal Code Limitation is Defined
                if ( item.options.postalCodeLimit ) {
                  // Valid Ontario Postal Code
                  if ( item.options.postalCodeLimit.test( inexact[0].postalCode.charAt(0) ) ) {

                    fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                    fill.valid.StreetAddress( item, v_street_address, corrections );

                    if ( v_community !== item.valueOf( f.City ) && v_community !== '' )
                      fill.valid.City( item, v_community, corrections );

                    if ( corrections.length )
                      logCorrections( item, rows.length, index, corrections );

                    writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                    writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );

                    callback();
                  } else {
                  // Invalid Ontario Postal Code
                    fill.invalid( item, 'PostalCode', corrections );
                    fill.invalid( item, 'StreetAddress', corrections );
                    if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                    writeGeocode( item, f.Lat, '' );
                    writeGeocode( item, f.Lon, '' );
                    callback();
                  }
                } else {
                // No Postal Code Limitation Defined
                  fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                  fill.valid.StreetAddress( item, v_street_address, corrections );
                  if (v_community !== item.valueOf(f.City) && v_community !== '' )
                    fill.valid.City( item, v_community, corrections );
                  if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                  writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                  writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                  callback();
                }
              } else {
                fill.valid.StreetAddress( item, v_street_address, corrections );
                if (v_community !== item.valueOf(f.City) && v_community !== '' )
                    fill.valid.City( item, v_community, corrections );
                if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                callback();
              }
            // JUST DO COMMUNITY
            } else {

              // Missing Postal Code + Available Postal Code
              if ( item.valueOf( f.PostalCode ) === '' && inexact[0].postalCode ) {
                // Postal Code Limitation is Defined
                if ( item.options.postalCodeLimit ) {
                  // Valid Ontario Postal Code
                  if ( item.options.postalCodeLimit.test( inexact[0].postalCode.charAt(0) ) ) {
                    fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                    if ( v_community !== item.valueOf( f.City ) && v_community !== '' )
                      fill.valid.City( item, v_community, corrections );
                    if ( corrections.length )
                      logCorrections( item, rows.length, index, corrections );
                    writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                    writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                    callback();
                  } else {
                  // Invalid Ontario Postal Code
                    fill.invalid( item, 'PostalCode', corrections );
                    fill.invalid( item, 'StreetAddress', corrections );
                    if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                    writeGeocode( item, f.Lat, '' );
                    writeGeocode( item, f.Lon, '' );
                    callback();
                  }
                } else {
                // Postal Code Limitation is not Defined
                  fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                  if (v_community !== item.valueOf(f.City) && v_community !== '' )
                    fill.valid.City( item, v_community, corrections );
                  if ( corrections.length )
                  logCorrections( item, rows.length, index, corrections );

                  writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                  writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                  callback();
                }
              } else {
              // Has Postal code or No Postal Code Available
                if ( item.valueOf( f.PostalCode ) === '' )
                  fill.missing( item, 'PostalCode', corrections );

                if (v_community !== item.valueOf(f.City) && v_community !== '' )
                  fill.valid.City( item, v_community, corrections );

                if ( corrections.length )
                  logCorrections( item, rows.length, index, corrections );

                writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                callback();
              }
            }
          }
        }
      }
    });
}

function makeAddress ( item ) {
  if (
    typeof item.valueOf( item.options.fields.StreetAddress ) !== 'string' ||
    item.valueOf( item.options.fields.StreetAddress ) === ''
  ) return false;
  return new Address({
    street: item.valueOf( item.options.fields.StreetAddress ),
      city: item.valueOf( item.options.fields.City ),
     state: "ON",
   country: "CA"
    });
}

function addGeocodeFields( item ) {
  item[ item.options.fields.Lat ] = new History( item.options.fields.Lat, '', true );
  item[ item.options.fields.Lon ] = new History( item.options.fields.Lon, '', true );
}

function writeGeocode( item, target, value ) {
  let change = {
    action: 'Geocoder',
      type: '1',
     value: value,
      note: 'Geocoded ' + ( target == item.options.fields.Lat ? 'Latitude' : 'Longitude' ),
      from: target,
        to: target
  };
  item.addChange( target, change );
}

function itemError (e, item) {
  console.log(e);
  console.log('Error: Primary Key - ' + item.valueOf(item.options.fields.PrimaryKey));
  console.log('Error: Name - ' + item.valueOf(item.options.fields.Name));
  console.log('Error: Address_Line_1 - ' + item.valueOf(item.options.fields.StreetAddress));
  console.log('Error: Community - ' + item.valueOf(item.options.fields.City));
  console.log('Error: Postal Code - ' + item.valueOf(item.options.fields.PostalCode));
}