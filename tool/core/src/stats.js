const listAll         = require( '../lib/stat/lib/listAll' ),
      RowStats        = require( '../lib/stat/process/RowStats' ),
      fieldCategories = require( '../lib/stat/process/fieldCategories' ),
      fieldNames      = require( '../lib/stat/process/fieldNames' ),
      operationStats  = require( '../lib/stat/process/operationStats' );

module.exports = function( stats ) {
  let fieldList = listAll.fieldsList( stats );
  return {
    row: new RowStats( stats, fieldList ),
    field: {
      category: fieldCategories( stats, fieldList, [ 'all', 'main', 'shadow' ] ),
          name: fieldNames( stats, fieldList )
    },
    operation: operationStats( stats, fieldList )
  };
};