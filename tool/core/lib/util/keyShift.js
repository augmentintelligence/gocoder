exports.toFirst = keyFirst;
/**
 * Given an `obj`ect and a `key` name in the form of a string;
 * will modify the object in place so that the given `key`
 * appears first.
 * @param {Object} obj -
 *    The object to modify the ordering of keys.
 * @param {string} key -
 *    The name of the key to move into the first position.
 */
function keyFirst( obj, key ) {
  let keys = Object.keys( obj );
  if ( !obj.hasOwnProperty( key ) || keys.indexOf( key ) < 1 ) return;
  keys = [ key ].concat( keys.slice( keys.slice( 0, keys.indexOf( key ) ), keys.indexOf( key + 1 ) ) );
  keys.forEach( key => {
    let v = obj[ key ];
    delete obj[ key ];
    obj[ key ] = v;
  });
}

exports.toLast = keyLast;
/**
 * Given an `obj`ect and a `key` name in the form of a string;
 * will modify the object in place so that the given `key`
 * appears last.
 * @param {Object} obj -
 *    The object to modify the ordering of keys.
 * @param {string} key -
 *    The name of the key to move into the last position.
 */
function keyLast( obj, key ) {
  let keys = Object.keys( obj );
  if ( !obj.hasOwnProperty( key ) || keys.indexOf( key ) === keys.length - 1 ) return;
  keys = keys.filter( v => v !== key ).concat( key );
  keys.forEach( key => {
    let v = obj[ key ];
    delete obj[ key ];
    obj[ key ] = v;
  });
}

module.exports = exports;