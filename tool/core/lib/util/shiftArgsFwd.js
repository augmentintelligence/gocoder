
/**
 * Shifts values of `args` array forward by
 * the value of `by` number of times. Each value
 * moved with nothing to replace it becomes
 * `undefined`.
 *
 * @param {*[]} args -
 *  The array of arguments to shift
 *
 * @param {number} [by=1]
 *  The number of shifts forward to perform
 *
 * @returns {*[]}
 */
function shiftArgsFwd ( args, by = 1 ) {
  while ( by > 0 ) {
    args = [undefined].concat( args.slice( 0, args.length - 1 ) );
    --by;
  } return args;
}

module.exports = shiftArgsFwd;