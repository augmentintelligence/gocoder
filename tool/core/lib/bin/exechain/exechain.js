const BUILD = require("../../regex/patterns/building/_building");
const ROOM = require("../../regex/patterns/room/_room");
const FLOOR = require("../../regex/patterns/floor/_floor");
const LEVEL = require("../../regex/patterns/level/_level");
const RR = require("../../regex/patterns/rr/_rr");
const SECTION = require("../../regex/patterns/section/_section");
const SUITE = require("../../regex/patterns/suite/_suite");
const UNIT = require("../../regex/patterns/unit/_unit");
const WING = require("../../regex/patterns/wing/_wing");
const LOT = require("../../regex/patterns/lot/_lot");
const CON = require("../../regex/patterns/concession/_concession");
const IR = require("../../regex/patterns/reservation/_ir");
const POSTBAG = require("../../regex/patterns/pobag/_pobag");
const POSTBOX = require("../../regex/patterns/pobox/_pobox");
const STATION = require("../../regex/patterns/station/_station");
const CP = require("../../regex/patterns/codepostal/_cp");
const STREET = require("../../regex/patterns/street/_street");
const ISLAND = require("../../regex/patterns/island/_island");

module.exports = [
  BUILD,
  ROOM,
  FLOOR,
  LEVEL,
  RR,
  SUITE,
  POSTBAG,
  SECTION,
  UNIT,
  WING,
  LOT,
  CON,
  IR,
  POSTBOX,
  STATION,
  CP,
  STREET,
  ISLAND
];
