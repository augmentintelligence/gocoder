
const targetFieldsFilter = function ( fields ) {
  return Object.keys( fields )
    .filter( field => fields[field] !== '' &&
      (
        field == 'Name' ||
        field == 'StreetAddress' ||
        field == 'AddressLine2' ||
        field == 'City' ||
        field == 'PostalCode'
      )
    ).map( field => fields[field] );
};

module.exports = targetFieldsFilter;