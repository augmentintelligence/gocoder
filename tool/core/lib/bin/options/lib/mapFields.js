const fieldSets = require('../data/fieldSets');

const mapFields = function ( fieldset, obj ) {

  let mapKeys, fieldKeys;

  fieldKeys = Object.keys( obj );
    mapKeys = Object.keys( fieldset );

  mapKeys.forEach( key => {
    let mapNames, mapValueExceptions, fieldValue, fieldNameMap;

    mapNames = fieldSets[ key ].names;
    mapValueExceptions = fieldSets[ key ].valueExceptions;

    fieldNameMap = mapNames.filter( name => fieldKeys.includes( name ) )[0];
    fieldValue   = obj[ fieldNameMap ];

    if ( fieldNameMap && ( mapValueExceptions.length === 0 || !mapValueExceptions.includes( fieldValue ) ) )
      fieldset[ key ] = fieldNameMap;
  });

};

module.exports = mapFields;