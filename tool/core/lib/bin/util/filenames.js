const types = {
   csv: [/\.csv$/, /[\/\\][^\/\\]+\.csv/i],
  json: [/\.json$/, /[\/\\][^\/\\]+\.json/i]
};
// Type is result of `fromCSV/fromJSON`
const filenames = function ( type, answers ) {
  let a = Object.assign( answers );
  if ( types[type][0].test( a.srcPath ) ) {
    a.filename    = types[type][1].exec( a.srcPath )[0];
    a.fromDir     = a.srcPath.replace(a.filename, '');
    a.cleanCSV    = a.filename.substr(1).replace('.'+type, '-cleaned.csv');
    a.cleanJSON   = a.filename.substr(1).replace('.'+type, '-cleaned.json');
    a.cleanReport = a.filename.substr(1).replace('.'+type, '-report.txt');
    a.cleanLog    = a.filename.substr(1).replace('.'+type, '-log.csv');
    a.cleanStats  = a.filename.substr(1).replace('.'+type, '-stats.json');
  }
  return a;
};

module.exports = filenames;