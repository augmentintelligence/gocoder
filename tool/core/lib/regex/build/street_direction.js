const XRegExp = require('xregexp');

module.exports = XRegExp.build('(?xi) (?: {{north}} | {{east}} | {{south}} | {{west}} )', {
  north: /\bN(?:orth\b|[\.]?)/,
   east: /\bE(?:ast\b|[\.]?)/,
  south: /\bS(?:outh\b|[\.]?)/,
   west: /\bW(?:est\b|[\.]?)/
});