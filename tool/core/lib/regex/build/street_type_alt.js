const XRegExp = require('xregexp');

// You need to make a version
module.exports = XRegExp.build(`(?xi)
  (?: {{highway}} | {{queensway}} | {{countyRoad}} | {{rr}} )`, {
    highway: /\bH(?:i(?:ghway)?|wy)\b[\.]?/,
  queensway: /\bQueensway\b/,
 countyRoad: /\bCounty\b\s*\bR(?:oa)?d\b/,
         rr: /R(?:ural|egional|[\.])\s*R(?:(?:(?:oa)?d)|[\.])/,
    // fairway: /\bFairways?\b/
  });