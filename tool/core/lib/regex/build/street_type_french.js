const XRegExp = require('xregexp');

module.exports = XRegExp.build(`(?xi)
  (?: {{rue}} )`, {
    rue: /\bRue\b/
  });