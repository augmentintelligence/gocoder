const XRegExp = require('xregexp');

module.exports = XRegExp.build(`(?xi) (?:
      {{alley}}   |  {{avenue}}   |
      {{boulevard}} |
      {{cape}}    |  {{circle}}   | {{circuit}} | {{close}} | {{common}} | {{court}} | {{cove}} | {{crescent}} |
      {{drive}}   |
      {{gardens}} |   {{gate}}    |  {{green}}  | {{grove}} |
      {{heath}}   |  {{heights}}  | {{highway}} | {{hill}}  |
      {{landing}} |   {{lane}}    |  {{link}}   | {{line}} |
      {{manor}}   |   {{mews}}    |  {{mount}}  |
      {{parade}}  |   {{park}}    | {{parkway}} | {{passage}} | {{path}} | {{point}} |
      {{rise}}    |   {{road}}    |   {{row}}   |
      {{square}}  |  {{street}}   |
      {{terrace}} |   {{trail}}   |
      {{view}}    |   {{villas}}  |
      {{walk}}    |  {{walkway}}  | {{way}}
    ) {{period}}? {{comma}}?`, {

    // Extras
        comma: /,/,
       period: /[,\.]/,

    // Street Types
        alley: /\bAl(?:ley)?\b/,
       avenue: /\bAv(?:e(?:nue)?)?\b/,

          // bay: /\bBay?\b/,
    boulevard: /\bB(?:v|oulevard|lvd?)\b/,

         cape: /\bCa(?:pe)?\b/,
      //  centre: /\bC(?:e(?:ntre)?|tr)\b/,
      circuit: /\bCircuit\b/,
       circle: /\bCi(?:r(?:c(?:le)?)?)?\b/,
        close: /\bCl(?:ose)?\b/,
       common: /\bC(?:m|ommon)\b/,
        court: /\bC(?:[ot]|rt)\b/,
         cove: /\bC(?:ove|v)\b/,
     crescent: /\bCr(?:st?|es(?:cent)?)?\b/,

        drive: /\bDr(?:v|ive)?\b/,

      gardens: /\bG(?:dn?|rd|ardens)\b/,
         gate: /\bG(?:ate|[at])\b/,
        green: /\bGr(?:n|een)?\b/,
        grove: /\bG(?:r?v|rove)\b/,

        heath: /\bHe(?:ath)?\b/,
      heights: /\bH(?:t|eights)\b/,
      highway: /\bH(?:i(?:ghway)?|wy)\b/,
         hill: /\bH(?:il)?l\b/,

      //  island: /\bIs(?:land)?\b/,

      landing: /\bL(?:d|anding)\b/,
         lane: /\bL(?:n|ane)\b/,
         link: /\bLi(?:nk)?\b/,
         line: /\bLine\b/,

        manor: /\bM(?:ano)?r\b/,
         mews: /\bMe(?:ws)?\b/,
        mount: /\bM(?:n?t)\b/,

       parade: /\bP(?:r|arade)\b/,
         park: /\bP(?:a(?:rk)?|rk)\b/,
      parkway: /\bP(?:arkway|w?y|r?kwy)\b/,
      passage: /\bP(?:s|assage)\b/,
         path: /\bP(?:at)?h\b/,
        // place: /\bPl(?:ace)?\b/,
        // plaza: /\bP(?:z|laza)\b/,
        point: /\bP(?:oin)?t\b/,

         rise: /\bRi(?:se)?\b/,
         road: /\bR(?:oa)?d\b/,
          row: /\bRow?\b/,

       square: /\bSq(?:uare)?\b/,
       street: /\bSt(?:r(?:eet)?)?\b/,

      terrace: /\bT(?:c|errace)\b/,
        trail: /\bTr(?:ail)?\b/,

         view: /\bV(?:ie)?w\b/,
       villas: /\bVi(?:llas)?\b/,

         walk: /\bW(?:al)?k\b/,
      walkway: /\bW(?:alkway|k(?:wy)?)\b/,
          way: /\bWa?y\b/

  });