const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// [Feature Request] Coerce English_Name Not Null
const NameExtractor = new Mutator.Extractor(
  'Name', /.+/i,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, NameExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( item, to );
  }, ['trim'] );

module.exports = NameExtractor;