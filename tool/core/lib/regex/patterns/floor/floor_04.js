const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const floorPattern = XRegExp.build(`(?xi)
  ({{num}})({{floor}})
  `, {
      num:  /\b[\d]{1,2}/,
    floor:  /FL?\b/
  });

// GROUND FLOOR
const FloorExtractor = new Mutator.Extractor(
  'Floor', floorPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, FloorExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    item.addChange( to, {
      action: 'spacer',
       value: match.floor + ' ' + match.num,
        type: 2,
        note: 'Added space between elements',
        from: to,
          to: to
    });
  },
  [
    'trim',
    new Mutator.Expander( 'Floor ', /\bFl?r?\b[\.]?/i ),
    'trim'
  ]
);

module.exports = FloorExtractor;