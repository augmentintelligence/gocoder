const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim'),
  rangeFinder = require('../../../rangeFinder/rangeFinder');

const ordinalPattern = XRegExp.build( `(?xi) {{ordinal}}`, { ordinal: /\b[\d]{1,3}(?:st|nd|rd|th)\b/ } );
const floorPattern = XRegExp.build(`(?xi) ({{num_1}}) ({{separator}}) ({{num_2}}) {{space}} ({{floor}})`, {
    num_1: ordinalPattern,
    separator: /\s*[\/\-\+&]\s*/,
    num_2: ordinalPattern,
    space: /\s+/,
    floor: /\bFl(?:oo)?r?\b[\.]?/
  });

// 1st & 2nd Floor
const FloorExtractor = new Mutator.Extractor(
  'Floor', floorPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, FloorExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    trim(item, [to,from]);

    let range = [match.num_1.substr(0, match.num_1.length - 2), match.num_2.substr(0, match.num_2.length - 2 ) ],
     isSeries = rangeFinder.isSeries(...range);

    item.addChange( to, {
      action: 'rangeFinder',
       value: [match.num_1,match.num_2].join(isSeries ? ' & ' : ' - ') + ' ' + match.floor,
        type: 2,
        note: 'Make ' + (isSeries ? 'Series' : 'Range')
    });

    Mutator.Caser.exec( [ item, match ], to, [ 'floor' ] );
  },
  [
    'trim',
    new Mutator.Expander( 'Floor', /\bFlr?\b[\.]?/i ),
    'trim'
  ]
 );

module.exports = FloorExtractor;