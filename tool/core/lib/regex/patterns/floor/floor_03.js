const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const floorPattern = XRegExp.build(`(?xi)
  ({{name}}) {{space}} ({{floor}})
  `, {
     name:  /Gr?(?:ou)?nd|M(?:ai)?n|Upper|Lower|Top/,
    space:  /\s+/,
    floor:  /\bFl(?:oo)?r?\b[\.]?/
  });

// GROUND FLOOR
const FloorExtractor = new Mutator.Extractor(
  'Floor', floorPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, FloorExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( [ item, match ], to, [ 'name', 'floor' ] );
  },
  [
    'trim',
    new Mutator.Expander( 'Ground', /\bGr?nd[\.]?\b/i ),
    new Mutator.Expander( 'Main', /\bMn\b/i ),
    new Mutator.Expander( 'Floor', /\bFlr?\b[\.]?/i ),
    'trim'
  ]
);

module.exports = FloorExtractor;