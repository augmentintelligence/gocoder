const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const floorPattern = XRegExp.build(`(?xi)
  ({{tag}})({{space}}?)({{num}})
  `, {
      tag: /\bFL?/,
      num: /[123][\d]?\b/,
    space: /\s+/
  });

// GROUND FLOOR
const FloorExtractor = new Mutator.Extractor(
  'Floor', floorPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, FloorExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    if ( !match.space )
      item.addChange( to, {
        action: 'spacer',
        value: match.tag + ' ' + match.num,
          type: 2,
          note: 'Added space between elements',
          from: to,
            to: to
      });
  },
  [
    'trim',
    new Mutator.Expander( 'Floor ', /\bFl?r?\b[\.]?/i ),
    'trim'
  ]
);

module.exports = FloorExtractor;