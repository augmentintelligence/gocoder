const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const floorPattern = XRegExp.build(`(?xi)
  ({{num}}) {{space}} ({{floor}})
  `, {
      num: /\b[\d]{1,3}(?:st|nd|rd|th)\b/,
    space: /\s+/,
    floor: /\bFl(?:oo)?r?\b[\.]?/
  });

// 13th Floor
const FloorExtractor = new Mutator.Extractor(
  'Floor', floorPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, FloorExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( [ item, match ], to, [ 'num', 'floor' ] );
  },
  [
    'trim',
    new Mutator.Expander( 'Floor', /\bFlr?\b[\.]?/i ),
    'trim'
  ]
 );

module.exports = FloorExtractor;