const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// Lot 50, Con 1, Municipality of Huron East
const lotPattern = XRegExp.build(`(?xi)
  ({{tag}}) ({{space}}?) ({{num}})
  `, {
      tag:  /\bLot\b(?:[\#\.]?[\#\.]?)/,
    space:  /\s+/,
      num:  /[\#]?\b[\d]+\b/,
  });

const LotExtractor = new Mutator.Extractor(
  'Lot', lotPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, LotExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    // Spacer
    if ( !match.space ) {
      match.space = ' ';
      item.addChange({
        action: 'spacer',
         value: match.tag + match.space + match.num,
          type: 2,
          note: 'Added Space',
          from: to,
            to: to
      });
    }
    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'tag' ] );
  },
  [
    'trim',
    new Mutator.Cleaner( 'pound', 'Removed Extraneous "#"', /[\#]/),
    'trim'
  ]
);

module.exports = LotExtractor;