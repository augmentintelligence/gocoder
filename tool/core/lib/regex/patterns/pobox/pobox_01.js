const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
       spacer = require( '../../pseudo/spacer2' ),
safeOptionals = require( '../../pseudo/safeOpt' );

// P.O. BOX ####...

const spacePattern = /\s+/;
const PostalBoxPattern = XRegExp.build(
  `(?xi) (?:({{P}})({{PSpace}}?)({{O}})({{OSpace}}?))?({{B}})({{BSpace}}?)({{Hash}})({{HashSpace}}?)({{box}})`,
  {
    P: /\bP(?:ost(?:al)?)?\.?/,
    PSpace: spacePattern,
    O: /O(?:ffice)?\.?/,
    OSpace: spacePattern,
    B: /Bo?x/,
    BSpace: spacePattern,
    Hash: /[\#]?/,
    HashSpace: spacePattern,
    box: XRegExp.build(`(?xi) {{num_letter}}|{{num}}|{{letter_num}}`,{
      num_letter: /[\d]+[A-Z]\b/,
      num: /[\d]+\b/,
      letter_num: /[A-Z][\d]+\b/
    })
  }
);
///(?:(?:[Pp](?:ost)?\s*\.?\s*)(?:[Oo](?:ffice)?\s*\.?\s*))?(?:[Bb]o?x\s*\#?\s*)([\d]+)/i,
const PostalBoxExtractor = new Mutator.Extractor(
  'Postal_Box', PostalBoxPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, PostalBoxExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    // Declare Match Group Order
    let outGroups = [
      'P', 'PSpace',
      'O', 'OSpace',
      'B', 'BSpace',
      'Hash', 'HashSpace',
      'box'
    ];

    // Make optionals safe
    safeOptionals( match, [ 'P', 'PSpace', 'O', 'OSpace', 'BSpace', 'Hash', 'HashSpace' ] );

    //? SPACING
    //~> 1 Space
    spacer( item, to, match, outGroups, [ 'BSpace' ], true );
    //~> 0 Spaces
    spacer( item, to, match, outGroups, [ 'PSpace', 'OSpace', 'HashSpace' ], false );

  },
  [
    'trim',
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /[\#]/),
    new Mutator.Expander('Postal Office Box', /(?:(?:[Pp](?:ost)?\s*\.?\s*)(?:[Oo](?:ffice)?\s*\.?\s*))?(?:[Bb]o?x)/i),
    'trim'
  ]
);


module.exports = PostalBoxExtractor;