const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutator');

class Trimmer extends Mutator {

  constructor(name, note, pattern) {
    if (typeof note !== 'string' && pattern === undefined) {
      pattern = note;
      note = undefined;
    } super(name, note || `Cleaned ${name}`, '2', pattern);
  }

  exec(item, targets) {
    super.exec(item, targets, undefined, false, (item, change, from, to, match) => {
      change.value = XRegExp.replace(item.valueOf(from), this.pattern, ' ');
      item.addChange(from, change);
    });
  }
}

module.exports = new Trimmer('doubleSpace', 'Double Spaces to Single Space', /\s\s+/g);