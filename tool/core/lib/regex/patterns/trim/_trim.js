const trimmers = [
  require('./comma'),
  require('./period'),
  require('./dash'),
  require('./space'),
  require('./semicolon'),
  require('./doublespace')
];
module.exports = (item, targets) => trimmers.forEach(trimmer => trimmer.exec(item, targets));