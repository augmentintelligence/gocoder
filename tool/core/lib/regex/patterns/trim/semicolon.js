const Cleaner = require('../../classes/Cleaner');
module.exports = new Cleaner('trailingSemicolon', 'Removed Trailing Semicolons', /(?:^\;+)|(?:\;+$)/g);