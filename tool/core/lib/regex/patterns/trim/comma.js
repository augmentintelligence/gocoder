const Cleaner = require('../../classes/Cleaner');
module.exports = new Cleaner('trailingComma', 'Removed Trailing Commas', /(?:^\,+)|(?:\,+$)/g);