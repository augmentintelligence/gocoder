const Cleaner = require('../../classes/Cleaner');
module.exports = new Cleaner('trailingPeriod', 'Removed Trailing Periods', /(?:^\.+)|(?:\.+$)/g);