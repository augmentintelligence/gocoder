const Cleaner = require('../../classes/Cleaner');
module.exports = new Cleaner('trailingSpace', 'Removed Trailing Spaces', /(?:^\s+)|(?:\s+$)/g);