const Cleaner = require('../../classes/Cleaner');
module.exports = new Cleaner('trailingDash', 'Removed Trailing Dashes', /(?:^-)|(?:-$)/g);