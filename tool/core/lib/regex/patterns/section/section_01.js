const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// SECTION B3
const sectionPattern = XRegExp.build(
  `(?xi) ({{tag}})({{space}})({{section}})`,
  {
    tag: /\bsection\b/,
    space: /\s+/,
    section: XRegExp.build(
      `(?xi) {{letter_numbers}}|{{numbers_letter}}|{{numbers_only}}|{{letter_only}}`,
      {
        letter_numbers: /\b[A-Z][\d]+\b/,
        numbers_letter: /\b[\d]+[A-Z]\b/,
        numbers_only: /\b[\d]+\b/,
        letter_only: /\b[A-Z]\b/
      }
    )
  }
);

const SectionExtractor = new Mutator.Extractor(
  'Section', sectionPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, SectionExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'tag' ] );
  },
  [ 'trim' ]
);

module.exports = SectionExtractor;