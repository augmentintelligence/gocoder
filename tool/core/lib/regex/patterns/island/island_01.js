const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const pattern = XRegExp.build(
  `(?xi) ({{name}}) ({{tag}})`,
  {
    name: /(\b[\w]+\b\s)+/,
    tag: /\bIsland\b/
  }
);

// CORNWALL ISLAND
// Original: /(\b[\w]+\b\s)+\bIsland\b/i
const IslandExtractor = new Mutator.Extractor(
  'Island', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, IslandExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( item, to );
  }, [ 'trim' ] );

module.exports = IslandExtractor;