const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Cleaner('slash', 'Removed Extraneous "/"', /\//),
  new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
  'trim'
];

///(?:(\bunit\s*#?\s*)([A-Z0-9]+)\s\&\s([\d]+))/i,

const pattern = XRegExp.build (

  `(?xi)
    ({{tag}})
    {{space}}? {{pound}}? {{space}}?
    ({{unit_01}})
    ({{andspace}})
    ({{unit_02}})
  `,
  {
    tag: /\bunit/,
    space: /\s*/,
    pound: /\#/,
    unit_01: /[A-Z0-9]+/,
    andspace: /\s\&\s/,
    unit_02: /(?:[A-Z]?[\d]+\b)|(?:[\d+]+[A-Z]?\b)/
  }

);

// UNIT 1004 & 1005
const UnitExtractor = new Mutator.Extractor(
  'Unit', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( [ item, match ], to, [ 'tag' ] );
  }, extras );

module.exports = UnitExtractor;