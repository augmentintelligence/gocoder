const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/)
];

// 3-33A BROADWAY AVENUE
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([\d]+)-([\d]+[A-Z]{1,2})\b\s*/i,
  (item, change, from, to, match) => {
    let streetNo = match[1];
    change.value = streetNo + ' ' + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match[2]);
  }, extras );

module.exports = UnitExtractor;