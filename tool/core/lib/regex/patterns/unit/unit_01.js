const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Cleaner('slash', 'Removed Extraneous "/"', /\//),
  new Mutator.Cleaner('dash',  'Removed Extraneous "-"', /-/),
  new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
  'trim'
];

// UNIT # A-109
// /(?:(\bunit\s*#?\s*)([A-Z][-\/][\d]+))/i
const unitPattern = XRegExp.build(
  `(?xi)
   ({{unit_tag}})
   {{space}}? {{pound}}? {{space}}?
   ({{unit_num}})`,
  {
    unit_tag: /\bUnit\b/,
    unit_num: XRegExp.build(
      `(?xi)
       {{letter}}{{separator}}{{numbers}}`,
       {
         letter:    /[A-Z]/,
         separator: /[\-\/]/,
         numbers:   /[\d]+/
       }
    ),
    // Extras
    space: /\s+/,
    pound: /[#]/
  }
);

const UnitExtractor = new Mutator.Extractor(
  'Unit', unitPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( [ item, match ], to, [ 'unit_tag' ] );
  }, extras );

module.exports = UnitExtractor;