const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');


// #PHYSICIANS
// T2-041 2075 Bayview Avenue
const unit1Pattern = XRegExp.build(
  `(?xi) (?:{{noU}}|{{noN}}){{rest}}`,
  {
    noU: /[\dABCDEFGHIJKLMNOPQRSTVWXYZ][\dA-Z]*/,
    noN: /[\dA-Z][\dABCDEFGHIJKLMOPQRSTUVWXYZ]*/,
    rest: /[\dA-Z]*/
  }
);
const UnitPattern = XRegExp.build(`(?xi)
  ^({{unit1}}) ({{dash}}) ({{unit2}}) ({{comma}}?)({{space}}) ({{street_no}}) {{space}} ({{street_start}})`,
  {
    unit1:     unit1Pattern,
    dash:      /[\-]/,
    unit2: XRegExp.build(`(?xi){{letters}}|{{numbers}}|{{letters}}{{numbers}}|{{numbers}}{{letters}}|{{numbers}}{{letters}}{{numbers}}|{{letters}}{{numbers}}{{letters}}`,{
      letters: /[A-Z]+/,
      numbers: /[0-9]+/
    }),
    comma: /[\,]+/,
    street_no: /[0-9]+/,
    street_start: /[A-Z]/,
    space:     /\s+/
});

const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = (match.comma?match.comma:'') + (match.space?match.space:'') + match.street_no + ' ' + match.street_start + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match.unit1 + match.dash + match.unit2 );
  },
  [
    'trim',
    new Mutator.Tagger('Unit', /^/)
  ]
);

module.exports = UnitExtractor;