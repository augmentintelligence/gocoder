const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');


// #PHYSICIANS
// B-100, 751 Victoria St S
const UnitPattern = XRegExp.build(`(?xi)
  ^({{unit}})({{comma}})`,
  {
    unit: XRegExp.build(`(?xi){{letter}}{{dash}}{{numbers}}{{letter}}?`,{
       letter: /[A-Z]/,
         dash: /[\-]/,
      numbers: /[0-9]+/
    }),
    comma: /[\,]/
});

const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  },
  [
    'trim',
    new Mutator.Cleaner('Dash', /[\-]/),
    new Mutator.Tagger('Unit', /^/)
  ]
 );

module.exports = UnitExtractor;