const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

// A134 - 2075 Bayview Avenue
const UnitPattern = XRegExp.build(`(?xi)
  ^({{unit}}) {{space}}{{dash}}{{space}} ({{street_no}}) {{space}} ({{street_start}})`,
  {
    unit: XRegExp.build(`(?xi) (?: {{letter}}?{{number}} ) | (?: {{number}}{{letter}}? ) `,{
      letter: /[A-Z]/,
      number: /[0-9]+/
  }),
    dash:      /[\-]/,
    street_no: /[0-9]+/,
    space:     /\s+/,
    street_start: /[A-Z]/
});

const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = match.street_no + ' ' + match.street_start + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match.unit);
  },
  [
    'trim',
    new Mutator.Tagger('Unit', /^/)
  ]
);

module.exports = UnitExtractor;