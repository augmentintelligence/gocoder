const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');
const rangeFinder = require('../../../rangeFinder/rangeFinder'),
             trim = require('../trim/_trim');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/),
  'trim'
];

// 18-19-8321 KENNEDY ROAD
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([\d]+)(-)([\d]+)(-)([\d]+)\b/,
  (item, change, from, to, match) => {

    let streetNo, units;
    if (rangeFinder.charRange(match[1],match[3]) < rangeFinder.charRange(match[3],match[5])) {
       streetNo = match[5];
          units = [match[1], match[4], match[3]];

    } else {
       streetNo = match[1];
          units = [match[3], match[2], match[5]];
    }

    let isSeries = rangeFinder.isSeries(units[0], units[2]);

    change.value = streetNo + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, units.join(''));

    trim(item, [from, to]);

    item.addChange(to, {
      action: 'rangeFinder',
       value: [units[0], units[2]].join(isSeries ? ' & ' : ' - '),
        type: 2,
        note: 'Make ' + (isSeries ? 'Series' : 'Range')
    });

  }, extras );

module.exports = UnitExtractor;