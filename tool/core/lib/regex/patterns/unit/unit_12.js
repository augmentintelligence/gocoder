const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/),
  'trim'
];

// Old 07 is now part of [03, 04]
// This is old 08

// 34 A AVONDALE BLVD
/* List of non-directonal letters
 * ABCDFGHIJKLMOPQRTXYZ
 */
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([\d]+)\s([ABCDFGHIJKLMOPQRTXYZ])\b\s/i,
  (item, change, from, to, match) => {
    change.value = match[1] + ' ' + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange( from, change, match[2] );
  }, extras );

module.exports = UnitExtractor;