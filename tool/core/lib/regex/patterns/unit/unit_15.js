const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/)
];

const spaceRX = /\s+/;
const UnitPattern = XRegExp.build(`(?xi)
  ^({{unit}}) {{dash}} {{space}}? ({{street_no}}) {{space}} `,
  {
    unit: XRegExp.build(`(?xi) (?: {{letter}}?{{number}} ) | (?: {{number}}{{letter}}? ) `,{
      letter: /[A-Z]/,
      number: /[0-9]+/
  }),
    dash:      /[\-]/,
    street_no: /[0-9]+/,
    space:     /\s+/
});

// C2-95 TIMES AVE
// 4-350 BURNHAMTHORPE RD E
const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = match.street_no + ' ' + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match.unit);
  }, extras );

module.exports = UnitExtractor;