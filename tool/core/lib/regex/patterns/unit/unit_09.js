const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');
const rangeFinder = require('../../../rangeFinder/rangeFinder'),
             trim = require('../trim/_trim');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/)
];

// 112-400/420 MAIN STREET EAST
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([\d]+)-([\d]+)([\/,+])([\d]+)\b/,
  (item, change, from, to, match) => {

    change.value = match[1] + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match.slice(2,5).join(''));

    trim(item, [from, to]);

    let range = [match[2], match[4]],
     isSeries = rangeFinder.isSeries(...range);

    item.addChange(to, {
      action: 'rangeFinder',
       value: range.join(isSeries ? ' & ' : ' - '),
        type: 2,
        note: 'Make ' + (isSeries ? 'Series' : 'Range')
    });

  }, extras );

module.exports = UnitExtractor;