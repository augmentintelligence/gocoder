const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');


// #PHYSICIANS
// 204 A-2016 West St
const UnitPattern = XRegExp.build(`(?xi)
  ^({{unit}})({{dash}})({{street_no}})`,
  {
    unit: XRegExp.build(`(?xi){{numbers}}{{space}}?{{letter}}`,{
       letter: /[A-Z]/,
        space: /\s+/,
      numbers: /[0-9]+/
    }),
    dash: /[\-]/,
    street_no: /[\d]+/
});

const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = match.street_no + XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match.unit + match.dash );
    if ( (match.unit + match.dash).indexOf( ' ' ) > 0 )
      item.addChange( to, {
        action: 'whitespaceCleaner',
         value: (match.unit + match.dash).replace( /\s+/g, '' ),
          type: 2,
          note: 'Removed all whitespace',
          from: to,
            to: to
      });
  },
  [
    'trim',
    new Mutator.Cleaner('Dash', /[\-]/),
    new Mutator.Tagger('Unit', /^/)
  ]
 );

module.exports = UnitExtractor;