const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const unitPattern = XRegExp.build(
  `(?xi) {{letter_number}} | {{number_letter}}`, {
    letter_number: /\b[A-Z][\d]+\b/,
    number_letter: /\b[\d]+[A-Z]\b/
  }
);

const pattern = XRegExp.build(
  `(?xi) ({{unit_1}}) [&] ({{unit_2}}) $`, {
    unit_1: unitPattern,
    unit_2: unitPattern
  }
);

// 4A&4C
///\b([A-Z0-9]+)\&([A-Z0-9]+)\b$/i
const UnitExtractor = new Mutator.Extractor(
  'Unit', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '').trim();
    item.addChange( from, change, match[0] );

    item.addChange(to, {
      action: 'spacer',
      value: match.unit_1 + ' & ' + match.unit_2,
      type: 2,
      note: 'Added Space'
    });
  },
  [
    'trim',
    new Mutator.Tagger('Unit', /^/),
    'trim'
  ]
);

module.exports = UnitExtractor;