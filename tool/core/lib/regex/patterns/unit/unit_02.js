const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

// 1726 HWY 7A
// /^(\b[\d]+\b\s+\bH(?:i(?:ghway)?|wy)\b\s+[\d]+)([A-Z])(\sN(?:orth)?|E(?:ast)?|S(?:outh)?|W(?:est)?)?/i
const pattern = XRegExp.build (
  `(?xi)
  ^ ( {{highway}} ) ( {{unit}} ) ( {{direction}} )? `,
  {
    highway: XRegExp.build (
      `(?xi) ( {{address_no}} ) {{space}} ( {{highway_tag}} ) {{space}} ( {{highway_no}} )`,
      {
        space:       /\s+/,
        address_no:  /\b[\d]+\b/,
        highway_tag: /\bH(?:i(?:ghway)?|wy)\b/,
        highway_no:  /[\d]+/
      }
    ),
    unit: /[A-Z]\b/,
    direction: XRegExp.build (
      `(?xi) {{space}} (?: {{north}} | {{east}} | {{south}} | {{west}} )`,
      {
        space: /\s+/,
        north: /N(?:orth|[\.])?/,
        east:  /E(?:ast|[\.])?/,
        south: /S(?:outh|[\.])?/,
        west:  /W(?:est|[\.])?/
      }
    )
  }
);

const UnitExtractor = new Mutator.Extractor(
  'Unit', pattern,
  (item, change, from, to, match) => {
    change.value = match.highway + ( match.direction ? match.direction : '' );
    item.addChange( from, change, match.unit );
  }, [ 'trim', new Mutator.Tagger('Unit', /^/) ]);

module.exports = UnitExtractor;