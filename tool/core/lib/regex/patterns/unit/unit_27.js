const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');


// #PHYSICIANS
// Un 2-200 Whitmore Road
const UnitPattern = XRegExp.build(`(?xi)
  ^({{tag}})({{space}})({{unit}})({{dash}})`,
  {
    unit: XRegExp.build(`(?xi){{letters}}|{{numbers}}|{{letters}}{{numbers}}|{{numbers}}{{letters}}|{{numbers}}{{letters}}{{numbers}}|{{letters}}{{numbers}}{{letters}}`,{
      letters: /[A-Z]+/,
      numbers: /[0-9]+/
  }),
    tag:       /\bUn\b/,
    dash:      /[\-]/,
    street_no: /[\d]+/,
    space:    /\s+/
});

const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  },
  [
    'trim',
    new Mutator.Cleaner('Dash', /[\-]/),
    new Mutator.Expander('Unit', /^\bUn\b/i)
  ]
 );

module.exports = UnitExtractor;