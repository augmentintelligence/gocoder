const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Cleaner('slash', 'Removed Extraneous "/"', /\//),
  new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
  'trim'
];
///(?:(\bunit\s*#?\s*)([A-Z0-9]+)\&([\d]+))/i
const pattern = XRegExp.build (

  `(?xi)
    ({{tag}})
    {{space}}? {{pound}}? {{space}}?
    ({{unit_01}})
    ({{and}})
    ({{unit_02}})
  `,
  {
    tag: /\bunit/,
    space: /\s*/,
    pound: /\#/,
    unit_01: /[A-Z0-9]+/,
    and: /\&/,
    unit_02: /[\d]+/
  }

);

// UNIT 1004&1005
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( [ item, match ], to, ['tag'] );
    item.addChange('Unit', {
      action: 'SeriesExpander',
       value: match.tag + ' ' + match.unit_01 + ' & ' + match.unit_02,
        type: '2',
        note: 'Added space between series',
        from: 'Unit',
          to: 'Unit'
    });
  }, extras );

module.exports = UnitExtractor;