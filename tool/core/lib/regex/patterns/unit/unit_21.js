const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/),
  new Mutator.Cleaner('Pound', /[\#]/),
  'trim'
];

// 648 DUNDAS STREET A102
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /[\#]?\b([A-Z]+[\d]+)\b$/i,
  (item, change, from, to, match) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '').trim();
    item.addChange(from, change, match[1]);
  }, extras);

module.exports = UnitExtractor;