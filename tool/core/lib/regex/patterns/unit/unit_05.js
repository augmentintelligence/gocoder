const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');
const rangeFinder = require('../../../rangeFinder/rangeFinder'),
             trim = require('../trim/_trim');

// const street_type = require('../../build/street_type_all');
// const street_dir = require('../../build/street_direction');

// UNIT 3-4
// /(?:(\bunit\s*#?\s*)([\d]+)-([\d]+))$/i
const pattern_tag = XRegExp.build(
  `(?xi) {{unit}} {{space}}? {{pound}}? {{space}}?`,
  {
    unit: /\bunit/,
    space: /\s+/,
    pound: /\#/,
  }
);
// /(?:(\bunit\s*#?\s*)([\d]+)-([\d]+))$/i
const pattern = XRegExp.build(
  `(?xi)
    (?:
      ({{unit_tag1}}) ({{d01}})-({{d02}}) $
    ) | (?:
      ({{unit_tag2}}) ({{d11}})-({{d12}}) {{comma}}
    )`,
  {
    unit_tag1: pattern_tag,
    unit_tag2: pattern_tag,
    d01: /[\d]+/,
    d11: /[\d]+/,
    d02: /[\d]+/,
    d12: /[\d]+/,
    comma: /[\,]/
  }
);

const UnitExtractor = new Mutator.Extractor(
  'Unit', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( [ item, match ], to, [ 'unit_tag1', 'unit_tag2' ] );
    trim( item, [ from, to ] );

    let range = [match.d01, match.d11, match.d02, match.d12].filter(v => v !== undefined),
      isSeries = rangeFinder.isSeries(...range);

    item.addChange(to, {
      action: 'rangeFinder',
       value: [ match.unit_tag1, match.unit_tag2].filter(v => v !== undefined)[0] + range.join(isSeries ? ' & ' : ' - '),
        type: 2,
        note: 'Make ' + (isSeries ? 'Series' : 'Range')
    });

  },
  [
    'trim',
    new Mutator.Cleaner('slash', 'Removed Extraneous "/"', /\//),
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
    'trim'
  ]
);

module.exports = UnitExtractor;