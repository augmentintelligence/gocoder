const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');
const rangeFinder = require('../../../rangeFinder/rangeFinder'),
             trim = require('../trim/_trim');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/),
  'trim'
];

// W-1A-1249 LONDON ROAD
//! NOTE: This record is just wrong - supposed to be 1A-1249 LONDON ROAD WEST, though the same can't be assumed otherwise

const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([A-Z]+)(-)([\d]+[A-Z]{1,2})-([\d]+)\b/i,
  (item, change, from, to, match) => {
    change.value = match[4] + XRegExp.replace(match.input, UnitExtractor.pattern, '');

    item.addChange(from, change, match.slice(1,4).join(''));

    trim(item, [from, to]);

    let range = [match[1], match[3]],
     isSeries = rangeFinder.isSeries(...range);

    item.addChange(to, {
      action: 'rangeFinder',
       value: range.join(isSeries ? ' & ' : ' - '),
        type: 2,
        note: 'Make ' + (isSeries ? 'Series' : 'Range')
    });

  }, extras );


module.exports = UnitExtractor;