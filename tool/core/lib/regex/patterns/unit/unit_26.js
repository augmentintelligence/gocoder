const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');


// 152 - 1880 Eglington Avenue East
const space = /\s+/;
const UnitPattern = XRegExp.build(`(?xi)
  ^({{unit}})({{space1}})({{dash}})({{space2}})({{street_no}}){{space3}} `,
  {
    unit: XRegExp.build(`(?xi){{letters}}|{{number}}`,{
      letters: /[A-Z]+/,
       number: /[0-9]+/
  }),
    dash:      /[\-]/,
    street_no: /[\d]+/,
    space1:    space,
    space2:    space,
    space3:    space
});

const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = match.street_no + ' ' + XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match.unit + match.space1 + match.dash + match.space2 );
  },
  [
    'trim',
    new Mutator.Cleaner('Dash', /[\-]/),
    'trim',
    new Mutator.Tagger('Unit', /^/)
  ]
 );

module.exports = UnitExtractor;