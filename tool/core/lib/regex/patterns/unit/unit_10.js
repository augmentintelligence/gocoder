const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/)
];

// 579A LAKESHORE RD E
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([\d]+)([A-Z]+)\b\s/i,
  (item, change, from, to, match) => {
    change.value = match[1] + ' ' + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match[2]);
  }, extras );

module.exports = UnitExtractor;