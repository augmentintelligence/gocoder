const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const UnitPattern = XRegExp.build(`(?xi)
  ^({{pound}})?({{unit}}) ({{dash}}) ({{street_no}}) {{space}} `,
  {
    pound: /[\#]/,
    unit: XRegExp.build(`(?xi){{letters}}|{{number}}`,{
      letters: /[A-Z]+/,
       number: /[0-9]+/
  }),
    dash:      /[\-]/,
    street_no: /[0-9]+/,
    space:     /\s+/
});

// C2-95 TIMES AVE
// 4-350 BURNHAMTHORPE RD E
const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = match.street_no + ' ' + XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, (match.pound?match.pound:'') + match.unit + match.dash );
  },
  [
    new Mutator.Cleaner('Pound', /[\#]/),
    new Mutator.Cleaner('Dash', /[\-]/),
    'trim',
    new Mutator.Tagger('Unit', /^/)
  ]
 );

module.exports = UnitExtractor;