const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/),
  'trim'
];

// 648/650 DUNDAS STREET
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([\d]+)[\/]([\d]+)\b\s/i,
  (item, change, from, to, match) => {
    let street_no = match[2];
    change.value = street_no + ' ' + XRegExp.replace(match.input, UnitExtractor.pattern, '').trim();
    item.addChange(from, change, match[1]);
  }, extras);

module.exports = UnitExtractor;