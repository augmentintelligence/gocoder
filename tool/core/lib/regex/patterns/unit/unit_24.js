const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// Copy from 15 (Try putting before... though currently this is not being detected at all, so it can technically come at the end)

const UnitPattern = XRegExp.build(`(?xi)
  ^({{unit}}) ({{dash}}) ({{street_no}}) {{space}} `,
  {
    unit: XRegExp.build(`(?xi){{letters}}{{number}}`,{
      letters: /[A-Z]{2}/,
       number: /[0-9]+/
  }),
    dash:      /[\-]/,
    street_no: /[0-9]+/,
    space:     /\s+/
});

// C2-95 TIMES AVE
// 4-350 BURNHAMTHORPE RD E
const UnitExtractor = new Mutator.Extractor(
  'Unit', UnitPattern,
  (item, change, from, to, match) => {
    change.value = match.street_no + ' ' + XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match.unit + match.dash );
  },
  [
    'trim',
    new Mutator.Tagger('Unit', /^/)
  ]
 );

module.exports = UnitExtractor;