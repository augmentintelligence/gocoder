const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/),
  new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /[\#]/),
  'trim'
];

//! TODO: Make this sensitive to HWY #27 etc
// const street_type = require('../../build/street_type_alt');
// const unitPattern = XRegExp.build

// 7125 GOREWAY DR #104
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /#\b([\d]+[A-Z]*)$/i,
  (item, change, from, to, match) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '').trim();
    item.addChange(from, change, match[0]);
  }, extras);

module.exports = UnitExtractor;