const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/),
  'trim'
];

// B30455 DURHAM REGION ROAD 23
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([A-Z])([\d]+)\b\s/i,
  (item, change, from, to, match) => {
    let street_no = match[2];
    change.value = street_no + ' ' + XRegExp.replace(match.input, UnitExtractor.pattern, '').trim();
    item.addChange(from, change, match[1]);
  }, extras);

module.exports = UnitExtractor;