const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');
const trim = require('../trim/_trim');

const extras = [
  'trim',
  new Mutator.Tagger('Unit', /^/)
];

// A&F-2 TREMONT DR
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /^\b([A-Z])([&\/])([A-Z])\-([\d]+)\b/i,
  (item, change, from, to, match) => {
    let street_no = match[4];
    change.value = street_no + XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match[1] + match[2] + match[3]);

    trim(item, [from, to]);

    let range = [match[1], match[3]];
    item.addChange(to, {
      action: 'rangeFinder',
       value: range.join(' & '),
        type: 2,
        note: 'Make Series'
    });

  }, extras);

module.exports = UnitExtractor;