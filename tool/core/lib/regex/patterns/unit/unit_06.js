const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Cleaner('slash', 'Removed Extraneous "/"', /\//),
  new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
  'trim'
];

// UNIT 1004[/A]?
// UNIT # 1004

// /(?:\bunit\s*#?\s*([A-Z0-9]+(?:([\/\-\+&])[A-Z])?))/i
const unitPattern = XRegExp.build(
  `(?xi)
   ({{unit_tag}})
   ({{space}})?
   ({{pound}})?
   ({{unit_num}})
  `,
  {
    unit_tag: /\bUnit/,
    space: /\s+/,
    pound: /[#]\s*/,
    unit_num: XRegExp.build(
      `(?xi)
       {{part_1}}
       (?: {{separator}}{{part_2}} )?
      `,
      {
        // Guard against 'United'?
        part_1: /(?:[A-Z\d]\b)|(?:[ABCDFGHIJKLMNOPQRSTUVWXYZ\d][A-Z\d]+\b)|(?:[A-Z\d][ABCEFGHIJKLMNOPQRSTUVWXYZ\d]+\b)/,
        separator: /[\/\-\+&]/,
        part_2: /[A-Z]/
      }
    )
  }
);

const UnitExtractor = new Mutator.Extractor(
  'Unit', unitPattern,
  (item, change, from, to, match) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '');
    item.addChange(from, change, match[0]);
    Mutator.Caser.exec( [ item, match ], to, 'unit_tag' );

    if ( match.pound && match[0] == match.unit_tag.trim() + ' ' + match.pound.trim() + match.unit_num.trim() ) {
      item.addChange( to, {
        action: 'Spacer',
         value: match.unit_tag.trim() + ' ' + ( match.pound ? match.pound.trim() : '' ) + ' ' + match.unit_num.trim(),
          type: 2,
          note: 'Added space',
          from: to,
            to: to
      });
    }
    if ( !match.space ) {
      item.addChange( to, {
        action: 'Spacer',
         value: match.unit_tag.trim() + ' ' + ( match.pound ? match.pound.trim() : '' ) + ' ' + match.unit_num.trim(),
          type: 2,
          note: 'Added space',
          from: to,
            to: to
      });
    }

  }, extras );

module.exports = UnitExtractor;