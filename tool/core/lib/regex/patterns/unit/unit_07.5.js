const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');
/*
 * This was copied from unit_07 (Pretty sure it can replace 07 and then then rest shift down)
 * Clarification: It's not unit_07 and not related... it's just where this needs to be above.
 */
const separatorPattern = XRegExp.build(
  `(?xi) {{space}} {{dash}} {{space}}`,
  {
    space: /\s+/,
     dash: /[\-]/
  }
);
const pattern = XRegExp.build(
  `(?xi) ({{unit}}) ({{separator_1}}) ({{streetNo}}) ({{separator_2}})`,
  {
    unit: XRegExp.build(
      `(?xi) {{letter_num}} | {{num_letter}}`,
      {
        letter_num: /\b[A-Z]{1,2}[\d]+\b/,
        num_letter: /\b[\d]+[A-Z]{1,2}\b/
      }
    ),
    separator_1: separatorPattern,
    separator_2: separatorPattern,
    streetNo: /\b[\d]+\b/
  }
);

// A2 - 5995 - 14TH AVE
const UnitExtractor = new Mutator.Extractor(
  'Unit', pattern,
  ( item, change, from, to, match ) => {
    change.value = match.streetNo + match.separator_2 + XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match.unit + match.separator_1 );
    trim( item, to );
    new Mutator.Cleaner( 'dash', 'Removed Extraneous Dash (-)', /\-/ ).exec( item, from );

  },
  [ 'trim', new Mutator.Tagger('Unit', /^/) ]
);

module.exports = UnitExtractor;