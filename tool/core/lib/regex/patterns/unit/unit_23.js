const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const digitPattern = XRegExp.build(`(?xi) {{num}}`, { num: /\b[\d]+\b/ } );
const pattern = XRegExp.build(
  `(?xi) ^ ({{unit}}) ({{and}}) ({{street_no}})`,
  {
     unit: digitPattern,
      and: /\s*[&]\s*/,
street_no: digitPattern
  }
);
// 15 & 21 Bradford Street
// Original Pattern: N/A
const UnitExtractor = new Mutator.Extractor(
  'Unit', pattern,
  ( item, change, from, to, match ) => {
    change.value = match.street_no + XRegExp.replace( match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match.unit + match.and );
  },
  [
    'trim',
    new Mutator.Cleaner('Ampersand', 'Removed Extraneous "&"', /[&]/),
    'trim',
    new Mutator.Tagger('Unit', /^/),
    'trim'
  ]
);

module.exports = UnitExtractor;