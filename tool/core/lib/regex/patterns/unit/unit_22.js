const XRegExp = require('xregexp');
const Mutator = require('../../classes/Mutators');

const extras = [
  'trim',
  new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
  'trim'
];

// UNIT B
const UnitExtractor = new Mutator.Extractor(
  'Unit',
  /(?:(\bunit\s*#?\s*)([A-Z])\b)/i,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace(match.input, UnitExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  }, extras );

module.exports = UnitExtractor;