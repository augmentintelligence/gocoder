const Mutator = require('../../classes/Mutators');
// const Trim = require('../../helpers/_groups/trim');
// A1A 1A1
const PostalCodeCleaner = new Mutator.Cleaner('Whitespace', 'Removed All Whitespace', /\s+/g);
module.exports = PostalCodeCleaner;