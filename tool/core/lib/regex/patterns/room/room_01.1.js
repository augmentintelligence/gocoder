const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim'),
  rangeFinder = require('../../../rangeFinder/rangeFinder');

const spaceRX = /\s+/;
const roomPattern = XRegExp.build(`(?xi) ({{room}})({{room_space}})?({{no_1}})({{sep_space_1}})?({{sep}})({{sep_space_2}})?({{no_2}})`, {
    room: /\bR(?:oo)?m\b[\.]?[\#]?/,
    room_space: spaceRX,
    no_1: /\b[\d]+\b/,
    sep_space_1: spaceRX,
    sep: /[\-\&]/,
    sep_space_2: spaceRX,
    no_2: /\b[\d]+\b/
  });

// Room 2-400
const RoomExtractor = new Mutator.Extractor(
  'Room', roomPattern,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, RoomExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    trim( item, [ to, from ] );

    let c = Object.assign(change);
    c.from = 'Room';
    c.to = 'Room';
    c.action = 'Spacer';
    c.note = 'Added Space';
    c.type = 2;

    if ( !match.room_space ) {
      match.room_space = ' ';
      c.value = match.room + valOrEmpty( match.room_space ) + match.no_1 + valOrEmpty( match.sep_space_1 ) + match.sep + valOrEmpty( match.sep_space_2 ) + match.no_1;
      item.addChange( 'Room', change );
    }

    let range = [ match.no_1, match.no_2 ],
     isSeries = rangeFinder.isSeries(...range);

    item.addChange( to, {
      action: 'rangeFinder',
       value: match.room + match.room_space + [match.no_1,match.no_2].join(isSeries ? ' & ' : ' - '),
        type: 2,
        note: 'Make ' + (isSeries ? 'Series' : 'Range')
    });

    Mutator.Caser.exec( [ item, match ], to, [ 'room' ] );
  },
  [
    'trim',
    new Mutator.Cleaner( 'Pound', 'Removed Extraneous #', /[\#]/ ),
    new Mutator.Expander( 'Room', /\bRm\b[\.]?/i ),
    'trim'
  ]
 );

module.exports = RoomExtractor;

function valOrEmpty ( value ) {
  return value ? value : '';
}