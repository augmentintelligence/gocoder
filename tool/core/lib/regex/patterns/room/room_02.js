const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const pattern = XRegExp.build(
  `(?xi) ({{tag}}) ({{space}}?) ({{num}})`,
  {
    tag:   /\bR(?:oo)?m[\.]?[\#]?/,
    space: /\s+/,
    num:   /\b(?:[a-z]?[\d]+)\b|\b(?:[\d]+[a-z]?)\b|\b(?:[a-z][\d]?)\b|\b(?:[\d]?[a-z])\b/
  }
);
// ROOM 1305
const RoomExtractor = new Mutator.Extractor(
  'Room', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, RoomExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    if (match.space && match[0] !== match.tag.trim() + ' ' + match.num.trim()) {
      item.addChange(to, {
        action: 'Spacer',
        value: match.tag.trim() + ' ' + match.num.trim(),
        type: 2,
        note: 'Added space',
        from: to,
        to: to
      });
    }

    Mutator.Caser.exec( [ item, match ], to, [ 'tag' ] );

  },
  [
    'trim',
    new Mutator.Cleaner( 'Pound', 'Removed Extraneous #', /[\#]/ ),
    new Mutator.Cleaner( 'Period', 'Removed Extraneous .', /[\.]/ ),
    new Mutator.Expander( 'Room', /rm/i )
  ]
);

module.exports = RoomExtractor;