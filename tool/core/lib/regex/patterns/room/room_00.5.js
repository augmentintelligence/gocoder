const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// RM230-A 166 BROCK ST
const roomPattern = XRegExp.build(`(?xi)
  ^({{tag}})({{space1}}?)({{room}})({{space2}})({{street_no}})({{space3}})({{street_nameStart}})`,
  {
    tag: /\br(?:o{2})?m[\#]?/,
    space1: /\s+/,
    room: XRegExp.build(`(?xi) {{numbers}}{{dash}}{{letter}}`,{
      numbers: /[\d]+/,
      dash: /[\-]/,
      letter: /\b[A-Z]\b/
    }),
    space2: /\s+/,
    street_no: /[\d]+/,
    space3: /\s+/,
    street_nameStart: /[A-Z]/
  });

const RoomExtractor = new Mutator.Extractor(
  'Room', roomPattern,
  ( item, change, from, to, match ) => {
    change.value = match.street_no + ' ' + match.street_nameStart + XRegExp.replace( match.input, RoomExtractor.pattern, '' );
    item.addChange( from, change, match.tag + (match.space1?' ':'') + match.room );

    if ( !match.space1 ) {
      item.addChange( to, {
        action: 'spacer',
         value: match.tag + ' ' + match.room,
          type: 2,
          note: 'Added spacing between elements',
          from: to,
            to: to
      });
    }

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'tag' ] );

  },
  [
    'trim',
    new Mutator.Cleaner( 'Pound', 'Removed Extraneous #', /[\#]/ ),
    new Mutator.Expander( 'Room', /rm/i )
  ]
);

module.exports = RoomExtractor;