const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// ROOM 1018-94 STUART ST
const roomPattern = XRegExp.build(`(?xi)
  ^({{tag}})({{space1}}?)({{room}})({{dash}})({{street_no}})({{space2}})({{street_nameStart}})`,
  {
    tag: /\br(?:o{2})?m[\#]?/,
    space1: /\s+/,
    room: XRegExp.build(`(?xi) {{numbers_letter}}|{{letter_numbers}}`,{
      numbers_letter: /[\d]+[A-Z]?/,
      letter_numbers: /[A-Z]?[\d]+/
    }),
    dash: /[\-]/,
    street_no: /[\d]+/,
    space2: /\s+/,
    street_nameStart: /[A-Z]/
  });

const RoomExtractor = new Mutator.Extractor(
  'Room', roomPattern,
  ( item, change, from, to, match ) => {
    change.value = match.street_no + ' ' + match.street_nameStart + XRegExp.replace( match.input, RoomExtractor.pattern, '' );
    item.addChange( from, change, match.tag + (match.space1?match.space1:'') + match.room + match.dash );

    if ( !match.space1 ) {
      item.addChange( to, {
        action: 'spacer',
         value: match.tag + ' ' + match.room + match.dash,
          type: 2,
          note: 'Added spacing between elements',
          from: to,
            to: to
      });
    }

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'tag' ] );

  },
  [
    'trim',
    new Mutator.Cleaner( 'Pound', 'Removed Extraneous #', /[\#]/ ),
    new Mutator.Cleaner( 'dash', 'Trimmed Dashes', /[\-]$/),
    new Mutator.Expander( 'Room', /rm/i )
  ]
);

module.exports = RoomExtractor;