const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// RM1305
const RoomExtractor = new Mutator.Extractor(
  'Room', /\brm([0-9]+)\b/i,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, RoomExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  },
  [
    'trim',
    new Mutator.Expander( 'Room ', /rm/i )
  ]
);

module.exports = RoomExtractor;