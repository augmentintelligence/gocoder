const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

/**
 * Building Extractor
 * Detects:
 *  * 'BLD B'
 * Original Pattern:
 *  /(?:\b(?:(?:building)|(?:bld)|(?:bldg))\b\s*(#?\b[A-Z0-9]+\b))/i
 */

const pattern = XRegExp.build(
  `(?xi) ({{tag}}) ({{separator}}) ({{symbol}}) ({{identifier}}) `,
  {
    tag:        /\b(?:building|bldg?)\b/,
    separator:  /\s*/,
    symbol:     /[#:]?/,
    identifier: /\b[A-Z0-9]+\b/
  }
);

const BuildingExtractor = new Mutator.Extractor(
  'Building', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, BuildingExtractor.pattern, '' );
    item.addChange( from, change, match[ 0 ] );
    Mutator.Caser.exec( [ item, match ], to, 'tag' );
  },
  [
    'trim',
    new Mutator.Expander( 'Building', /bldg?/i ),
    new Mutator.Cleaner(  'pound', 'Removed Extraneous "#"', /#/ ),
    'trim'
  ]
);

module.exports = BuildingExtractor;