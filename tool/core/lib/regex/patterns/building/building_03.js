const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

/**
 * Building Extractor
 * Detects:
 *  * 'Wiarton Hospital - PROFESSIONAL SERVICES BUILDING'
 * Original Pattern:
 *  /(?:[,]?\s*(\b[\w]+\b\s)+\b(?:(?:building)|(?:bld)|(?:bldg))\b)/i
 */

const pattern = XRegExp.build(
  `(?xi) {{end}}? {{space}}? ({{identifier}}) ({{tag}})`,
  {
    end: /[,;]/,
    space: /\s+/,
    identifier: /(?:(?:(?:\b[\w]+\b)|(?:\b[\w\.\-']+[\w]+\b\.?)|(?:\b[\w]+\.))\s+)+/,
    tag: /\b(?:building|bldg?)\b/
  }
);

const BuildingExtractor = new Mutator.Extractor(
  'Building', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, BuildingExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( [ item, match ], to, [ 'identifier', 'tag' ] );
  },
  [
    'trim',
    new Mutator.Expander( 'Building', /bldg?/i ),
    'trim'
  ]
);


module.exports = BuildingExtractor;