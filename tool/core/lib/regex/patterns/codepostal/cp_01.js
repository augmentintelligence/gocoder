const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// C[ode] P[ostal] ####
// Original: /(?:\bCP|\bC\b\.?\s?\bP\b\.?\s?|\bCode\b\s\bPostale?\b)\s?[\d]+/i
const CodePostalExtractor = new Mutator.Extractor(
  'Code_Postal', /(?:\bCP|\bC\b\.?\s?\bP\b\.?\s?|\bCode\b\s\bPostale?\b)\s?[\d]+/i,
  (item, change, from, to, match) => {
    change.value = XRegExp.replace( match.input, CodePostalExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  },
  [
    'trim',
    new Mutator.Expander('Code Postal', /\bCP|\bC\b\.?\s?\bP\b\.?/i),
    'trim'
  ]
);


module.exports = CodePostalExtractor;