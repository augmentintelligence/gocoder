const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// Anything left in Address_Line_1/2
const OtherExtractor = new Mutator.Extractor(
  'Other_1', /.+/i,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, OtherExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    Mutator.Caser.exec( item, to );
  }, ['trim'] );

module.exports = OtherExtractor;