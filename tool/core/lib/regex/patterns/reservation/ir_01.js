const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// I.R.
const ReservationExtractor = new Mutator.Extractor(
  'Reservation',
  /\bI\.?R\.?\b/i,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, ReservationExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  },
  [
    'trim',
    new Mutator.Expander( 'Indian Reserve', /\bI\.?R\.?\b/i ),
    'trim'
  ]
);

module.exports = ReservationExtractor;