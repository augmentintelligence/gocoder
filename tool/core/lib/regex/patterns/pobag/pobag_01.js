const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const pattern = XRegExp.build(
  `(?xi) ({{tag}}) ({{space}}) ({{num}})`,
  {
    tag: /(?:[Pp]ost(?:al)?\s*[Bb]ag)|(?:[Pp]ost(?:al)?\s*(?:[Oo]ffice)?\s*[Bb]ag)/,
    space: /\s*/,
    num: /[\d]+/
  }
);

// Post[al] Bag ####
// Old Pattern: /[Pp]ostal\s*[Bb]ag\s*[\d]+/i
const PostalBagExtractor = new Mutator.Extractor(
  'Postal_Bag', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, PostalBagExtractor.pattern, '' );
    item.addChange(from, change, match[0]);
    if ( typeof match.space !== 'string' || match.space.length > 1 || match.space.length < 1 ) {
      item.addChange(to, {
        action: 'Spacer',
        value: match.tag.trim() + ' ' + match.num.trim(),
        type: 2,
        note: 'Added space',
        from: to,
        to: to
      });
    }
  },
  [
    'trim',
    new Mutator.Expander('Postal Office Bag', /(?:[Pp]ost(?:al)?\s*[Bb]ag)|(?:[Pp]ost\s*(?:[Oo]ffice)?\s*[Bb]ag)/i),
    'trim'
  ]
);

module.exports = PostalBagExtractor;