const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const pattern = XRegExp.build(`(?xi)({{tag}})({{pound}})({{num}})`,
  {
      tag: /[Bb]ag/,
    pound: /\s*[\#]?\s*/,
      num: /[\d]+|\b[A-Z]\b/
  }
);

// Post[al] Bag ####
// Old Pattern: /[Pp]ostal\s*[Bb]ag\s*[\d]+/i
const PostalBagExtractor = new Mutator.Extractor(
  'Postal_Bag', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, PostalBagExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  },
  [
    'trim',
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
    new Mutator.Expander('Postal Office Bag ', /[Bb]ag\s*/i),
    'trim'
  ]
);

module.exports = PostalBagExtractor;