module.exports = [

  require('./street_01'),
  require('./street_02'),
  require('./street_03'),   //! WatchOut: Added flexibility for one-word named highway ('Merrittville Highway')
  require('./street_03.5'),
  require('./street_04.1'), //! WatchOut: Added support for dashes between street no & ordinal street name
  require('./street_04'),
  require('./street_05'),
  require('./street_06'),   //! WatchOut: Added support for dashes between street no & ordinal street name
  require('./street_07'),
  require('./street_08')    //! WatchOut: Added support for dashes between street no & ordinal street name

];