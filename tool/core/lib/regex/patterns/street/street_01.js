const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_type_all = require('../../build/street_type_all'),
         type_exp_all = require('./lib/street_type_expanders_all');

const street_type_alt = require('../../build/street_type_alt'),
         type_exp_alt = require('./lib/street_type_expanders_alt');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

const street_alt = XRegExp.build(`(?xi)
    (?: ({{no_1}})?  {{space}}?
      (?:
        (?: ({{alt_type_1}}) {{period}}? {{space}}? ({{number_1}}) ) |
        (?: ({{name_1}}) {{space}}? ({{type_1}}) )
      ) (?: {{space}} ({{dir_1}}) )?
    )

      {{space}}? {{and}} {{space}}?

    (?: ({{no_2}})?  {{space}}?
      (?:
        (?: ({{alt_type_2}}) {{period}}? {{space}}? ({{number_2}}) ) |
        (?: ({{name_2}}) {{space}}? ({{type_2}}) )
      ) (?: {{space}} ({{dir_2}}) )?
    )`, {

    // Extras
    space: /\s+/,
   period: /\./,
      and: /&|and/,

     no_1: /^[\d]+/,
     no_2: /^[\d]+/,

   name_1: /(?:\b[0-9A-Z\u00C0-\u00FF\.\-]+\s)+/,
   name_2: /(?:\b[0-9A-Z\u00C0-\u00FF\.\-]+\s)+/,

   type_1: street_type_all,
   type_2: street_type_all,

   alt_type_1: street_type_alt,
   alt_type_2: street_type_alt,

   number_1: /\#?\s*[\d]{1,4}/,
   number_2: /\#?\s*[\d]{1,4}/,

   dir_1: street_dir,
   dir_2: street_dir

});


/*//!TODO
 * Shoutout to:
 * 133 HWY #11 & GRANT RD
 */

// no name/type dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', street_alt,
  ( item, change, from, to, match ) => {

    // Extract
    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();

    // Get Non-Empty Properties
    let s1p = getFound( match, 1 ),
        s2p = getFound( match, 2 );

    // Get Non-Empty Property Values
    let s1v = s1p.map(v => match[ v ].trim()),
        s2v = s2p.map(v => match[ v ].trim());

    // Join them with '&'
    item.addChange( from, change, makeIntersection( s1v, s2v ) );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'name_1', 'name_2', 'type_1', 'type_2', 'alt_type_1', 'alt_type_2', 'dir_1', 'dir_2' ] );

    // Refresh maps with Case-Corrected Values
    s1v = s1p.map(v => match[ v ].trim());
    s2v = s2p.map(v => match[ v ].trim());

    // Trim Original
    trim( item, from );

    // If first street has a regulear type... expand it.
    if ( s1p.includes( 'type_1' ) ) {
      type_exp_alt.exec( match, 'type_1', ( i, change, f, t, m ) => {
        Object.keys( type_exp_all.dictionary ).some( v => {
          if ( XRegExp.test( m.input, type_exp_all.dictionary[ v ] ) ) {
            change.from = 'Street';
              change.to = 'Street';
            s1v[ s1p.indexOf( 'type_1' ) ] = v;
            change.value = makeIntersection( s1v, s2v );
            item.addChange( 'Street', change );
          }
        });
      });
    }

    if ( s2p.includes( 'type_2') ) {
      type_exp_alt.exec( match, 'type_2', ( i, change, f, t, m ) => {
        Object.keys( type_exp_alt.dictionary ).some( v => {
          if ( XRegExp.test( m.input, type_exp_alt.dictionary[ v ] ) ) {
            change.from = 'Street';
              change.to = 'Street';
            s2v[ s2p.indexOf( 'type_2' ) ] = v;
            change.value = makeIntersection( s1v, s2v );
            item.addChange( 'Street', change );
          }
        });
      });
    }

    if ( s1p.includes( 'alt_type_1' ) ) {
      type_exp_alt.exec( match, 'alt_type_1', ( i, change, f, t, m ) => {
        Object.keys( type_exp_alt.dictionary ).some( v => {
          if ( XRegExp.test( m.input, type_exp_alt.dictionary[ v ] ) ) {
            change.from = 'Street';
              change.to = 'Street';
            s1v[ s1p.indexOf( 'alt_type_1' ) ] = v;
            change.value = makeIntersection( s1v, s2v );
            item.addChange( 'Street', change );
          }
        });
      });
    }

    if ( s2p.includes( 'alt_type_2' ) ) {
      type_exp_alt.exec( match, 'alt_type_2', ( i, change, f, t, m ) => {
        Object.keys( type_exp_alt.dictionary ).some( v => {
          if ( XRegExp.test( m.input, type_exp_alt.dictionary[ v ] ) ) {
            change.from = 'Street';
              change.to = 'Street';
            s2v[ s2p.indexOf( 'alt_type_2' ) ] = v;
            change.value = makeIntersection( s1v, s2v );
            item.addChange( 'Street', change );
          }
        });
      });
    }
    // Expand Direction
    dirExp.exec(item, 'Street');
  },
  [
    'trim',
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
    new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/),
    'trim'
  ]
);

  function makeIntersection( street1Components, street2Components ) {
    return [ street1Components.join(' '), street2Components.join(' ') ].join(' & ');
  }

  function getFound (match, i) {
    return Object.keys(match).filter(v => v[v.length-2] == '_' && v[v.length-1] == i && match[v]);
  }

module.exports = StreetExtractor;