const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

// This is to prevent '...SHOPPING CENTRE/PLAZA/MOUNT' from being caught as an address
const nameRX = XRegExp.build(
  `(?xi) ({{named}}) | ({{numbered}})`, {
    named: /(?:\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*)+/,
    numbered: /\b[\d]{1,3}(?:st|nd|rd|th)\b/
  }
);
const streetRx = XRegExp.build('(?xi) ({{no}}) (?:{{space}}|[\-]|{{space}}[\-]|{{space}}[\-]{{space}}|[\-]{{space}})? ({{name}}) {{space}}? {{comma}}? ({{type}}) {{space}}? ({{dir}})?', {
     no: /^[\d]+/,
  space: /\s+/,
   name: nameRX,
  comma: /\s*,?\s*/,
   type: /\b(?:Bay|Circle|C(?:e(?:ntre)?|tr|ourt)|Pl(?:a(?:ce|za))?|Mount)/,
    dir: street_dir
});
const centreExpander = new Mutator.Expander('Centre', /\bC(?:e?tr)\b[\.]?/i);
const placeExpander = new Mutator.Expander('Place', /\bPl\b/);

/*
 x SHOPPING CENTRE
 x IGA PLAZA
 x BIRCH MOUNT CAMPUS
 x MEADOWVALE COURT
 * 200 GARDEN CENTRE
 * 200 IGA PLAZA
 */

// no name type dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', streetRx,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
    item.addChange( from, change, match[0] );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'name', 'type', 'dir' ] );

    trim( item, from );

    centreExpander.exec( item, 'Street' );
    placeExpander.exec( item, 'Street' );
    dirExp.exec( item, 'Street' );

  },
  ['trim', new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/), 'trim' ] );

module.exports = StreetExtractor;