const Expander = require('../../../classes/Expander');
const XRegExp = require('xregexp');

let typeRx = {
  Highway: /\bHi?(?:wy)?\b[\.]?/i,
  Parkway: /\bP(?:r?kw)?y\b[\.]?/i,
     Road: /\bRd\b[\.]?/i,
   'R.R.': /R(?:\s|R\.?)/i
};

module.exports = new Expander('Street Type', XRegExp.build(`(?xi) (?: {{Highway}} | {{Parkway}} | {{Road}} | {{R.R.}} )`, typeRx), typeRx);