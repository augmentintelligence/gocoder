const Expander = require('../../../classes/Expander');
const XRegExp = require('xregexp');

let typeRx = {
        Alley: /\bAl\b[\.]?/i,
       Avenue: /\bAve?\b[\.]?/i,
          Bay: /\bBa\b[\.?]/i,
    Boulevard: /\bB(?:v|lvd?)\b[\.]?/i,
         Cape: /\bCa\b[\.]?/i,
       Centre: /\bC(?:e|tr)\b[\.]?/i,
       Circle: /\bCi(?:rc?)?\b[\.]?/i,
        Close: /\bCl\b[\.]?/i,
       Common: /\bCm\b[\.]?/i,
        Court: /\bC(?:o|rt)\b[\.]?/i,
         Cove: /\bCv\b[\.]?/i,
     Crescent: /\bCr(?:e?(?:st?))\b[\.]?/i,
        Drive: /\bDrv?\b[\.]?/i,
      Gardens: /\bGr?dn?\b[\.]?/i,
         Gate: /\bG[at]\b[\.]?/i,
        Green: /\bGrn?\b[\.]?/i,
        Grove: /\bGr?v\b[\.]?/i,
        Heath: /\bHe\b[\.]?/i,
      Heights: /\bHt\b[\.]?/i,
      Highway: /\bH(?:i?(?:wy)?)\b[\.]?/i,
         Hill: /\bHl\b[\.]?/i,
       Island: /\bIs\b[\.]?/i,
      Landing: /\bLd\b[\.]?/i,
         Lane: /\bLn\b[\.]?/i,
         Link: /\bLi\b[\.]?/i,
        Manor: /\bMr\b[\.]?/i,
         Mews: /\bMe\b[\.]?/i,
        Mount: /\bMn?t\b[\.]?/i,
       Parade: /\bPr\b[\.]?/i,
         Park: /\bP(?:a|rk)\b[\.]?/i,
      Parkway: /\bP(?:r?kw)?y\b[\.]?/i,
      Passage: /\bPs\b[\.]?/i,
         Path: /\bPh\b[\.]?/i,
        Place: /\bPl\b[\.]?/i,
        Plaza: /\bPz\b[\.]?/i,
        Point: /\bPt\b[\.]?/i,
         Rise: /\bRi\b[\.]?/i,
         Road: /\bRd\b[\.]?/i,
          Row: /\bRo\b[\.]?/i,
       Square: /\bSq\b[\.]?/i,
       Street: /\bStr?\b[\.]?/i,
      Terrace: /\bTc\b[\.]?/i,
        Trail: /\bTr\b[\.]?/i,
         View: /\bVw\b[\.]?/i,
       Villas: /\bVi\b[\.]?/i,
         Walk: /\bWl?k\b[\.]?/i,
      Walkway: /\bWk(?:wy)?\b[\.]?/i,
          Way: /\bWy\b[\.]?/i
};

module.exports = new Expander('Street Type', XRegExp.build(`(?xi)
(?:
  {{alley}}   |  {{avenue}}   |
  {{bay}}     | {{boulevard}} |
  {{cape}}    |  {{centre}}   | {{circle}}  | {{close}} | {{common}} | {{court}} | {{cove}} | {{crescent}} |
  {{drive}}   |
  {{gardens}} |   {{gate}}    |  {{green}}  | {{grove}} |
  {{heath}}   |  {{heights}}  | {{highway}} | {{hill}}  |
  {{island}}  |
  {{landing}} |   {{lane}}    |  {{link}}   |
  {{manor}}   |   {{mews}}    |  {{mount}}  |
  {{parade}}  |   {{park}}    | {{parkway}} | {{passage}} | {{path}} | {{place}} | {{plaza}} | {{point}} |
  {{rise}}    |   {{road}}    |   {{row}}   |
  {{square}}  |  {{street}}   |
  {{terrace}} |   {{trail}}   |
  {{view}}    |   {{villas}}  |
  {{walk}}    |  {{walkway}}  | {{way}}
)`, {
        alley: typeRx.Alley,
       avenue: typeRx.Avenue,

          bay: typeRx.Bay,
    boulevard: typeRx.Boulevard,

         cape: typeRx.Cape,
       centre: typeRx.Centre,
       circle: typeRx.Circle,
        close: typeRx.Close,
       common: typeRx.Common,
        court: typeRx.Court,
         cove: typeRx.Cove,
     crescent: typeRx.Crescent,

        drive: typeRx.Drive,

      gardens: typeRx.Gardens,
         gate: typeRx.Gate,
        green: typeRx.Green,
        grove: typeRx.Grove,

        heath: typeRx.Heath,
      heights: typeRx.Heights,
      highway: typeRx.Highway,
         hill: typeRx.Hill,

       island: typeRx.Island,

      landing: typeRx.Landing,
         lane: typeRx.Lane,
         link: typeRx.Link,

        manor: typeRx.Manor,
         mews: typeRx.Mews,
        mount: typeRx.Mount,

       parade: typeRx.Parade,
         park: typeRx.Park,
      parkway: typeRx.Parkway,
      passage: typeRx.Passage,
         path: typeRx.Path,
        place: typeRx.Place,
        plaza: typeRx.Plaza,
        point: typeRx.Point,

         rise: typeRx.Rise,
         road: typeRx.Road,
          row: typeRx.Row,

       square: typeRx.Square,
       street: typeRx.Street,

      terrace: typeRx.Terrace,
        trail: typeRx.Trail,

         view: typeRx.View,
       villas: typeRx.Villas,

         walk: typeRx.Walk,
      walkway: typeRx.Walkway,
          way: typeRx.Way

  }), {
        Alley: typeRx.Alley,
       Avenue: typeRx.Avenue,
          Bay: typeRx.Bay,
    Boulevard: typeRx.Boulevard,
         Cape: typeRx.Cape,
       Centre: typeRx.Centre,
       Circle: typeRx.Circle,
        Close: typeRx.Close,
       Common: typeRx.Common,
        Court: typeRx.Court,
         Cove: typeRx.Cove,
     Crescent: typeRx.Crescent,
        Drive: typeRx.Drive,
      Gardens: typeRx.Gardens,
         Gate: typeRx.Gate,
        Green: typeRx.Green,
        Grove: typeRx.Grove,
        Heath: typeRx.Heath,
      Heights: typeRx.Heights,
      Highway: typeRx.Highway,
         Hill: typeRx.Hill,
       Island: typeRx.Island,
      Landing: typeRx.Landing,
         Lane: typeRx.Lane,
         Link: typeRx.Link,
        Manor: typeRx.Manor,
         Mews: typeRx.Mews,
        Mount: typeRx.Mount,
       Parade: typeRx.Parade,
         Park: typeRx.Park,
      Parkway: typeRx.Parkway,
      Passage: typeRx.Passage,
         Path: typeRx.Path,
        Place: typeRx.Place,
        Plaza: typeRx.Plaza,
        Point: typeRx.Point,
         Rise: typeRx.Rise,
         Road: typeRx.Road,
          Row: typeRx.Row,
       Square: typeRx.Square,
       Street: typeRx.Street,
      Terrace: typeRx.Terrace,
        Trail: typeRx.Trail,
         View: typeRx.View,
       Villas: typeRx.Villas,
         Walk: typeRx.Walk,
      Walkway: typeRx.Walkway,
          Way: typeRx.Way
  });