const Expander = require('../../../classes/Expander');

module.exports = new Expander('Street Direction', /\b[NESW]\b[\.]?/i, {
  ' North': /\s+\bN\b[\.]?/i,
   ' East': /\s+\bE\b[\.]?/i,
  ' South': /\s+\bS\b[\.]?/i,
   ' West': /\s+\bW\b[\.]?/i
});