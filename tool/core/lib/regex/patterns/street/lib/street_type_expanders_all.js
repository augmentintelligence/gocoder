const Expander = require('../../../classes/Expander');
const XRegExp = require('xregexp');

let typeRx = {
        Alley: /\bAl\b[\.]?/i,
       Avenue: /\bAve?\b[\.]?/i,
          Bay: /\bBa\b[\.?]/i,
    Boulevard: /\bB(?:v|lvd?)\b[\.]?/i,
         Cape: /\bCa\b[\.]?/i,
       Centre: /\bC(?:e|tr)\b[\.]?/i,
       Circle: /\bCi(?:rc?)?\b[\.]?/i,
        Close: /\bCl\b[\.]?/i,
       Common: /\bCm\b[\.]?/i,
        Court: /\bC(?:o|r?t)\b[\.]?/i,
         Cove: /\bCv\b[\.]?/i,
     Crescent: /\bCr(?:e?(?:st?))?\b[\.]?/i,
        Drive: /\bDrv?\b[\.]?/i,
      Gardens: /\bGr?dn?\b[\.]?/i,
         Gate: /\bG[at]\b[\.]?/i,
        Green: /\bGrn?\b[\.]?/i,
        Grove: /\bGr?v\b[\.]?/i,
        Heath: /\bHe\b[\.]?/i,
      Heights: /\bHt\b[\.]?/i,
      Highway: /\bH(?:i?(?:wy)?)\b[\.]?/i,
         Hill: /\bHl\b[\.]?/i,
       Island: /\bIs\b[\.]?/i,
      Landing: /\bLd\b[\.]?/i,
         Lane: /\bLn\b[\.]?/i,
         Link: /\bLi\b[\.]?/i,
        Manor: /\bMr\b[\.]?/i,
         Mews: /\bMe\b[\.]?/i,
        Mount: /\bMn?t\b[\.]?/i,
       Parade: /\bPr\b[\.]?/i,
         Park: /\bP(?:a|rk)\b[\.]?/i,
      Parkway: /\bP(?:r?kw)?y\b[\.]?/i,
      Passage: /\bPs\b[\.]?/i,
         Path: /\bPh\b[\.]?/i,
        Place: /\bPl\b[\.]?/i,
        Plaza: /\bPz\b[\.]?/i,
        Point: /\bPt\b[\.]?/i,
         Rise: /\bRi\b[\.]?/i,
         Road: /\bRd\b[\.]?/i,
          Row: /\bRo\b[\.]?/i,
       Square: /\bSq\b[\.]?/i,
       Street: /\bStr?\b[\.]?/i,
      Terrace: /\bTc\b[\.]?/i,
        Trail: /\bTr\b[\.]?/i,
         View: /\bVw\b[\.]?/i,
       Villas: /\bVi\b[\.]?/i,
         Walk: /\bWl?k\b[\.]?/i,
      Walkway: /\bWk(?:wy)?\b[\.]?/i,
          Way: /\bWy\b[\.]?/i
};

module.exports = new Expander('Street Type', XRegExp.build(`(?xi)
(?:
  {{Alley}}   |  {{Avenue}}   |
  {{Bay}}     | {{Boulevard}} |
  {{Cape}}    |  {{Centre}}   | {{Circle}}  | {{Close}} | {{Common}} | {{Court}} | {{Cove}} | {{Crescent}} |
  {{Drive}}   |
  {{Gardens}} |   {{Gate}}    |  {{Green}}  | {{Grove}} |
  {{Heath}}   |  {{Heights}}  | {{Highway}} | {{Hill}}  |
  {{Island}}  |
  {{Landing}} |   {{Lane}}    |  {{Link}}   |
  {{Manor}}   |   {{Mews}}    |  {{Mount}}  |
  {{Parade}}  |   {{Park}}    | {{Parkway}} | {{Passage}} | {{Path}} | {{Place}} | {{Plaza}} | {{Point}} |
  {{Rise}}    |   {{Road}}    |   {{Row}}   |
  {{Square}}  |  {{Street}}   |
  {{Terrace}} |   {{Trail}}   |
  {{View}}    |   {{Villas}}  |
  {{Walk}}    |  {{Walkway}}  | {{Way}}

)`, typeRx ), typeRx );