const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_alt = XRegExp.build(
  '(?xi) ^ ({{no}}) {{space}}? ({{name}}) $', {
       no: /^[\d]+/,
     name: /the(?:\s+(?:\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*))+/,
    space: /\s+/
  });

/*
 * 100 THE WEST MALL
 */

// no? name/type dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', street_alt,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
    item.addChange( from, change, match[0] );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'name' ] );

    trim( item, from );

  }, ['trim', new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/), 'trim' ] );

module.exports = StreetExtractor;