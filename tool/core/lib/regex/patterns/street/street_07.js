const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_type = require('../../build/street_type_french');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

/*
 x 200 RUE PRINCIPALE
 */
const streetRx = XRegExp.build('(?xi) ({{no}}) {{space}}? ({{type}}) {{space}}? {{comma}}? ({{name}}+) ({{dir}})?', {
     no: /^[\d]+/,
  space: /\s+/,
  comma: /\s*,?\s*/,
   type: street_type,
  name: /\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*/,
    dir: street_dir
});

// no type name dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', streetRx,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
    item.addChange( from, change, match[0] );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'type', 'name', 'dir' ] );

    trim( item, from );

    dirExp.exec( item, 'Street' );

  }, ['trim', new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/), 'trim' ] );

module.exports = StreetExtractor;