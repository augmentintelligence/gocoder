const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_type = require('../../build/street_type_all'),
          typeExp = require('./lib/street_type_expanders_all');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

///(?:(?:\b[A-Z'-.]+\b\s*)+|(?:\b[\d]{1,3}(?:st|nd|rd|th)\b))\s*/,
const nameRX = XRegExp.build(
  `(?xi) ({{name_num}}) | ({{named}}+) | ({{numbered}})`, {
    name_num: /(?:\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*)(?:\b[\d]{1,3}(?:st|nd|rd|th)\b\s*)/,
    named:    /\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*/,
    numbered: /\b[\d]{1,3}(?:st|nd|rd|th)\b\s*/
  }
);
const streetRx = XRegExp.build(`(?xi)
  ({{no_1}}){{dash}}({{no_2}}) {{space}}?
  ({{name}}) {{space}}? {{comma}}?
  ({{type}})
  (?: {{space}} ({{dir}}) )?
  (?: {{space}} ({{num}}) )?`, {
   no_1: /^[\d]+[a-z]/,
   no_2: /[\d]+[a-z]/,
   dash: /\-/,
  space: /\s+/,
   name: nameRX,
  comma: /\s*,?\s*/,
   type: street_type,
    dir: street_dir,
    num: /\#?\s?[\d]+/
});

/*
 * 2A-2F Tremont Dr
 */

 const digit = /[\d]+/,
      letter = /[a-z]/i;

// no? name type dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', streetRx,
  ( item, change, from, to, match ) => {
    console.log( letter.exec( match.no_1 ) );
    if ( digit.exec( match.no_1 )[0] === digit.exec( match.no_2 )[0] ) {
      let no = digit.exec( match.no_1 )[0],
       units = [ letter.exec( match.no_1 )[0], letter.exec( match.no_2 )[0] ];

      change.value = units.join('-') + XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();

      // Get Non-Empty Properties
      let sp = getFound( match ),
      // Get Non-Empty Property Values
          sv = [no].concat(sp.map(v => v !== 'no_1' && v !== 'no_2' && match[ v ].trim()));

      item.addChange( from, change, sv.join( ' ' ) );

      change.action = 'UnitPseudoExtractor';
      change.note = 'Pseudo-Unit Mutation';

      change.value = 'Unit ' + units.join(' - ') + XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
      item.addChange( from, change );

      // Enforce Casing
      Mutator.Caser.exec( [ item, match ], to, [ 'name', 'type', 'dir' ] );

      trim( item, from );

      typeExp.exec( match, 'type', ( i, change, f, t, m ) => {
        Object.keys( typeExp.dictionary ).some( v => {
          if ( XRegExp.test( m.input, typeExp.dictionary[ v ] ) ) {
            change.from = 'Street';
              change.to = 'Street';

            change.value = ( no + ' ' ) + match.name + v + ( match.dir ? ' ' + match.dir : '' ) + (match.num ? ' ' + match.num : '');
            item.addChange( 'Street', change );
          }
        });
      });

      dirExp.exec(item, 'Street');

    }

  }, [ 'trim', new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/), 'trim' ] );

module.exports = StreetExtractor;

function getFound (match) {
  return Object.keys(match).filter(v => match[v] );
}