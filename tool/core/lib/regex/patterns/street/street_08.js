const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_type = require('../../build/street_type_all'),
          typeExp = require('./lib/street_type_expanders_all');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

const nameRX = XRegExp.build(
  `(?xi) ({{named}}+) | ({{numbered}})`, {
    named: /\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*/,
    numbered: /\b[\d]{1,3}(?:st|nd|rd|th)\b/
  }
);
const streetRx = XRegExp.build(`(?xi)
  ({{no}}) (?:{{space}}|[\-]|{{space}}[\-]|{{space}}[\-]{{space}}|[\-]{{space}})?
  ({{name}}) {{space}}? ({{num}})? {{comma}}?
  ({{type}})?
  (?: {{space}} ({{dir}}) )?`, {
     no: /^[\d]+/,
  space: /\s+/,
   name: nameRX,
  comma: /\s*,?\s*/,
    num: /[\d]{1,4}/,
   type: street_type,
    dir: street_dir
});

/*
 x HWY 27
 x 2000 HIGHWAY #27
 x 23 COUNTY ROAD 45
 x COUNTY ROAD #45
 x BROOMHEAD RD
 x 6951 DERRY RD W
 x 34 AVONDALE BLVD
 x 585 UNIVERSITY AVE
 x 100 MAIN STREET WEST
 x 2 DEWSIDE DRIVE
 x 112 MAIN STREET EAST
 * 489 BROADWAY
 * 39 LAROCQUE
 */

// no name type? dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', streetRx,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
    item.addChange( from, change, match[0] );

    trim( item, from );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'type', 'name', 'dir' ] );

    typeExp.exec( match, 'type', ( i, change, f, t, m ) => {
      Object.keys( typeExp.dictionary ).some( v => {
        if ( XRegExp.test( m.input, typeExp.dictionary[ v ] ) ) {
          change.from = 'Street';
            change.to = 'Street';
          change.value = ( match.no ? match.no + ' ' : '' ) + match.name + v + ( match.dir ? ' ' + match.dir : '' );
          item.addChange( 'Street', change );
        }
      });
    });

    dirExp.exec( item, 'Street' );

  }, [ 'trim', new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/), 'trim' ] );

module.exports = StreetExtractor;