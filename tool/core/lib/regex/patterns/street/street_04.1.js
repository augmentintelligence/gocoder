const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_type = require('../../build/street_type_all'),
          typeExp = require('./lib/street_type_expanders_all');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

///(?:(?:\b[A-Z'-.]+\b\s*)+|(?:\b[\d]{1,3}(?:st|nd|rd|th)\b))\s*/,
const spaceRX = /\s+/;
const nameRX = XRegExp.build(
  `(?xi) ({{name_num}}) | ({{num_name}}) | ({{named}}) | ({{numbered}})`, {
    name_num: /(?:\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*)(?:\b[\d]{1,3}(?:st|nd|rd|th)\b\s*)/,
    num_name: /(?:\b[\d]{1,3}(?:st|nd|rd|th)\b\s*)(?:\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*)/,
    named:    /(?:\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*)+/,
    numbered: /\b[\d]{1,3}(?:st|nd|rd|th)\b\s*/
  }
);
const streetRx = XRegExp.build(`(?xi)
  (?: ({{no}}) ({{no_space}})? )?
  ({{name}}) (?: ({{comma}}) ({{comma_space}}) )?
  ({{type}})
  (?: ({{num_space}})? ({{num}}) )?
  (?: ({{dir_space}})? ({{dir}}) )?`, {
    no: /^[\d]+/,
    no_space: /(?:\s+|[\-]|\s+[\-]|\s+[\-]\s+|[\-]\s+)?/,
    name: nameRX,
    comma: /[,]/,
    comma_space: spaceRX,
    type: street_type,
    dir: street_dir,
    dir_space: spaceRX,
    num: /\#?\s?[\d]+/,
    num_space: spaceRX
});

/*
 x HWY 27
 x 2000 HIGHWAY #27
 x 23 COUNTY ROAD 45
 x COUNTY ROAD #45
 * BROOMHEAD RD
 * 6951 DERRY RD W
 * 34 AVONDALE BLVD
 * 585 UNIVERSITY AVE
 * 100 MAIN STREET WEST
 * 2 DEWSIDE DRIVE
 * 112 MAIN STREET EAST
 */

// no? name type dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', streetRx,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
    item.addChange( from, change, match[0] );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'name', 'type', 'dir' ] );

    let c = Object.assign(change);
    c.from = 'Street';
    c.to = 'Street';
    c.action = 'Spacer';
    c.note = 'Added Space';
    c.type = 2;

    if ( match.no && !match.no_space ) {
      match.no_space = ' ';
      c.value = valOrEmpty( match.no ) + valOrEmpty( match.no_space ) + match.name  + valOrEmpty( match.comma ) + valOrEmpty( match.comma_space ) + match.type + valOrEmpty( match.num_space ) + valOrEmpty( match.num ) + valOrEmpty( match.dir_space ) + valOrEmpty( match.dir );
      item.addChange( 'Street', change );
    }
    if ( match.dir && !match.dir_space ) {
      match.dir_space = ' ';
      c.value = valOrEmpty( match.no ) + valOrEmpty( match.no_space ) + match.name  + valOrEmpty( match.comma ) + valOrEmpty( match.comma_space ) + match.type + valOrEmpty( match.num_space ) + valOrEmpty( match.num ) + valOrEmpty( match.dir_space ) + valOrEmpty( match.dir );
      item.addChange( 'Street', change );
    }
    if ( match.num && !match.num_space ) {
      match.num_space = ' ';
      c.value = valOrEmpty( match.no ) + valOrEmpty( match.no_space ) + match.name  + valOrEmpty( match.comma ) + valOrEmpty( match.comma_space ) + match.type + valOrEmpty( match.num_space ) + valOrEmpty( match.num ) + valOrEmpty( match.dir_space ) + valOrEmpty( match.dir );
      item.addChange( 'Street', change );
    }

    trim( item, from );

    typeExp.exec( match, 'type', ( i, change, f, t, m ) => {
      Object.keys( typeExp.dictionary ).some( v => {
        if ( XRegExp.test( m.input, typeExp.dictionary[ v ] ) ) {
          change.from = 'Street';
            change.to = 'Street';
           match.type = v;
           change.value = valOrEmpty( match.no ) + valOrEmpty( match.no_space ) + match.name  + valOrEmpty( match.comma ) + valOrEmpty( match.comma_space ) + match.type + valOrEmpty( match.num_space ) + valOrEmpty( match.num ) + valOrEmpty( match.dir_space ) + valOrEmpty( match.dir );
          item.addChange( 'Street', change );
        }
      });
    });

    dirExp.exec(item, 'Street');

  }, [ 'trim', new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/), 'trim' ] );

module.exports = StreetExtractor;

function valOrEmpty ( value ) {
  return value ? value : '';
}