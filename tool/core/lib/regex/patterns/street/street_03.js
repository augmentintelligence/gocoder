const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_type = require('../../build/street_type_alt'),
          typeExp = require('./lib/street_type_expanders_alt');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

const spaceRX = /\s+/;
const street_alt = XRegExp.build(
  `(?xi) ({{no}})? {{space}}?
         (?: ({{old}}) {{space}}? )?
         ({{name}}) {{space}}?
         (?: ({{pound}}?) {{space}}? ({{num}}) )? {{space}}? {{period}}? {{space}}? {{comma}}? {{space}}?
         ({{dir}})?{{space}}?{{notDash}}`,
  {
    old: /\bOld\b|\bThe\b|\bKing(?:\'s)?\b|\bFalconbridge\b|\bDurham\b\s\bRegional\b|\b[A-Z]{3,}\b/,
     no: /^[\d]+/,
  space: spaceRX,
  comma: /\s*,?\s*/,
 period: /[,\.]/,
  pound: /[\#]/,
   name: street_type,
    num: /[\d]{1,4}/,
    dir: street_dir,
notDash: /[^\-A-Z\d]+?|\b$/
  }
);

/*
 * HWY 27
 * 2300 OLD HWY 27
 * 2000 HIGHWAY #27
 * 23 COUNTY ROAD 45
 * COUNTY ROAD #45
 * RR #20
 * R.R. 20
 * 300 RR #3
 * 100 QUEENSWAY W
 x BROOMHEAD RD
 x 6951 DERRY RD W
 x 34 AVONDALE BLVD
 x 585 UNIVERSITY AVE
 x 100 MAIN STREET WEST
 x 2 DEWSIDE DRIVE
 x 112 MAIN STREET EAST
 */

// no? name/type dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', street_alt,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
    item.addChange( from, change, match[0] );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'old', 'name', 'dir' ] );

    trim( item, from );

    typeExp.exec( match, 'name', ( i, change, f, t, m ) => {
      Object.keys( typeExp.dictionary ).some( v => {
        if ( XRegExp.test( m.input, typeExp.dictionary[ v ] ) ) {
          change.from = 'Street';
            change.to = 'Street';
            if ( /County/i.test(match.name) )
              match.name = 'County ' + v;
            else match.name = v;
          change.value = ( match.no ? match.no + ' ' : '' ) + ( match.old ? match.old + ' ' : '' ) + match.name + ' ' + ( match.num ? match.num : '' ) + (match.dir ? ' ' + match.dir : '');
          item.addChange('Street', change);
        }
      });
    });

    dirExp.exec(item, 'Street');

  },
  [
    'trim',
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /[\#]/),
    new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/),
    'trim'
  ]
);

module.exports = StreetExtractor;