const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const street_dir = require('../../build/street_direction'),
          dirExp = require('./lib/street_dir_expanders');

const street_alt = XRegExp.build(`(?xi)
    ({{no}})    {{space}}?
   ({{name}})?  {{space}}?
   ({{type}})   {{space}}? {{period}}?
  ({{number}})? {{space}}?
   ({{dir}})?`, {
  // Extras
  space: /\s+/,
 period: /\./,

     no: /^[\d]+/,
   name: /(?:\b[\'\`\-\.A-Z\u00C0-\u00FF]+\s*)+/,
   type: /\bRegion(?:al)?\b\s\bR(?:oa)?d\b/,
 number: /[\#]?\s*[\d]{1,4}/,
    dir: street_dir
});

/*
 * 30455 DURHAM REGION(AL)? ROAD 23
 */

// no name/type dir?
const StreetExtractor = new Mutator.Extractor(
  'Street', street_alt,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, StreetExtractor.pattern, '' ).trim();
    item.addChange( from, change, match[0] );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'name', 'type', 'dir' ] );

    trim( item, from );
    dirExp.exec( item, 'Street' );

  },
  [
    'trim',
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /[\#]/),
    new Mutator.Cleaner('comma', 'Removed Extraneous ","', /[,]/),
    new Mutator.Expander('Regional', /\bRegion\b/i),
    new Mutator.Expander('Street Type', /\bRd\b\.?/i, { Road: /\bRd\b\.?/i }),
    'trim'
  ]
);

module.exports = StreetExtractor;