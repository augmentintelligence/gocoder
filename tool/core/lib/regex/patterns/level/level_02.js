const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// LEVEL D2-268 GROSVENOR ST
const levelPattern = XRegExp.build(`(?xi)
  ^({{tag}})({{space1}}?)({{level}})`,
  {
    tag: /\ble?ve?l/,
    space1: /\s+/,
    level: XRegExp.build(`(?xi) {{numbers_letter}}|{{letter_numbers}}`,{
      numbers_letter: /[\d]+[A-Z]?\b/,
      letter_numbers: /[A-Z]?[\d]+\b/
    }),
  });

const LevelExtractor = new Mutator.Extractor(
  'Level', levelPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, LevelExtractor.pattern, '' );
    item.addChange( from, change, match.tag + (match.space1?match.space1:'') + match.level );

    if ( !match.space1 ) {
      item.addChange( to, {
        action: 'spacer',
         value: match.tag + ' ' + match.level,
          type: 2,
          note: 'Added spacing between elements',
          from: to,
            to: to
      });
    }

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'tag' ] );

  },
  [
    'trim',
    new Mutator.Expander( 'Level', /\blvl\b/i ),
    'trim'
  ]
);

module.exports = LevelExtractor;