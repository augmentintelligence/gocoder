const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// LOWER LEVEL-435 NOTRE DAME AVE
const levelPattern = XRegExp.build(`(?xi)
  ({{level}}) {{space}} ({{tag}})
  `, {
     level:  /\bG(?:rou)?nd\b|\bM(?:ai)?n\b|\bUpper\b|\bLower\b|\bConcourse\b/,
    space:  /\s+/,
      tag:  /\bLe?ve?l\b/
  });

const LevelExtractor = new Mutator.Extractor(
  'Level', levelPattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, LevelExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, [ 'level', 'tag' ] );

  },
  [
    'trim',
    new Mutator.Cleaner( 'dash', 'Trimmed Dashes', /[\-]$/),
    new Mutator.Expander( 'Level', /\blvl\b/i ),
    'trim'
  ]
);

module.exports = LevelExtractor;