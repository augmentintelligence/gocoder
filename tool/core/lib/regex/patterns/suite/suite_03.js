const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
       spacer = require( '../../pseudo/spacer2' ),
safeOptionals = require( '../../pseudo/safeOpt' );

const pattern = XRegExp.build (
  `(?xi) ({{tag}})({{dot}}?)
         ({{sym_space1}}?)({{symbol}}?)({{sym_space2}}?)
         ({{num}})({{sep_space1}}?)({{separator}})({{sep_space2}}?)({{letter}})`,
  {
           tag: /\bs(?:ui)?tes?/,
           dot: /\./,
    sym_space1: /\s+/,
        symbol: /[\#\:]/,
    sym_space2: /\s+/,
           num: /[\d]{1,4}/,
    sep_space1: /\s+/,
     separator: /[\/\-\+&]/,
    sep_space2: /\s+/,
        letter: /\b[A-Z]\b/
  }
);
// SUITE 1004/A
// Original Pattern: /\b(suite)\s*([#:]?\s*([A-Z0-9]+)(?:([\/\-\+&])([A-Z]))?)/i
const SuiteExtractor = new Mutator.Extractor(
  'Suite', pattern,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, SuiteExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    // Declare Match Group Order
    let outGroups = [
      'tag', 'dot',
      'sym_space1', 'symbol', 'sym_space2',
      'num',
      'sep_space1', 'separator', 'sep_space2',
      'letter'
    ];

    // Make optionals safe
    safeOptionals( match, [ 'dot', 'sym_space1', 'symbol', 'sym_space2', 'sep_space1', 'sep_space2' ] );

    //? SPACING
    // ~> 1 Space
    spacer( item, to, match, outGroups, [ 'sym_space1' ], true );
    // ~> 0 Spaces
    spacer( item, to, match, outGroups, [ 'sym_space2', 'sep_space1', 'sep_space2' ], false );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, 'tag' );

  },
  [
    'trim',
    new Mutator.Expander( 'Suite', /\bSte/ ),
    new Mutator.Compressor( 'Suite', /\bSuites/ ),
    new Mutator.Cleaner( 'period', 'Removed Exraneous "."', /\./ ),
    new Mutator.Cleaner('slash', 'Removed Extraneous "/"', /\//),
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /[\#]/),
    new Mutator.Cleaner('colon', 'Removed Extraneous ":"', /\:/),
    'trim'
  ]
);

module.exports = SuiteExtractor;