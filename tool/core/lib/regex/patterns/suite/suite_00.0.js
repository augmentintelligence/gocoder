const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

const spacePattern = /\s+/;
const numPattern = /[\dA-Z]+/;
const pattern = XRegExp.build (
  `(?xi)^({{tag}})({{space10}}?)({{symbol}}?)({{space11}}?)({{num1}})({{space2}}?)({{dash}})({{space3}}?)({{num2}})$`,
  {
        tag: /\bs(?:ui)?tes?\.?/,
    space10: spacePattern,
     symbol: /[\#\:]/,
    space11: spacePattern,
       num1: numPattern,
     space2: spacePattern,
       dash: /[\-]/,
     space3: spacePattern,
       num2: numPattern
  }
);
// SUITE 1004-<...street>
// Original Pattern: /\b(suite)\s*([#:]?\s*([A-Z0-9]+)(?:([\/\-\+&])([A-Z]))?)/i
const SuiteExtractor = new Mutator.Extractor(
  'Suite', pattern,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, SuiteExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, 'tag' );
    if ( !match.space10 && !match.space11 ) {
      match.space10 = ' ';
      match.space11 = '';
      if ( !match.symbol ) match.symbol = '';
      if ( !match.space2 ) match.space2 = '';
      if ( !match.space3 ) match.space3 = '';
      item.addChange({
        action: 'spacer',
         value: match.tag + match.space10 + match.symbol + match.space11 + match.num1 + match.space2 + match.dash + match.space3 + match.num2,
          type: 2,
          note: 'Added Space',
          from: to,
            to: to
      });
    }
    if ( !match.space2 || !match.space3 ) {
      if ( !match.space2 ) match.space2 = ' ';
      if ( !match.space3 ) match.space3 = ' ';
      if ( !match.space10 ) match.space10 = '';
      if ( !match.space11 ) match.space11 = '';
      if ( !match.symbol ) match.symbol = '';
      item.addChange({
        action: 'spacer',
         value: match.tag + match.space10 + match.symbol + match.space11 + match.num1 + match.space2 + match.dash + match.space3 + match.num2,
          type: 2,
          note: 'Added Space',
          from: to,
            to: to
      });
    }

  },
  [
    'trim',
    new Mutator.Compressor( 'Suite', /\bSuites/ ),
    new Mutator.Expander('Suite', /^[Ss][Tt][Ee]\.?/ ),
    new Mutator.Cleaner('slash', 'Removed Extraneous "/"', /\//),
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /#/),
    new Mutator.Cleaner('colon', 'Removed Extraneous ":"', /\:/),
    'trim'
  ]
);

module.exports = SuiteExtractor;