const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim'),
       spacer = require( '../../pseudo/spacer2' ),
safeOptionals = require( '../../pseudo/safeOpt' );

const rangeFinder = require('../../../rangeFinder/rangeFinder');

const pattern = XRegExp.build(
  `(?xi) ({{tag}})({{dot}}?)({{sep_space1}}?)({{separator}}?)({{sep_space2}}?)({{no_1}})({{rs_space1}}?)({{rs}})({{rs_space2}}?)({{no_2}})`,
  {
    tag:        /\bs(?:ui)?tes?/,
    dot:        /\./,
    sep_space1: /\s+/,
    separator:  /[\#\:]/,
    sep_space2: /\s+/,
    no_1:       /[A-Z0-9]+/,
    rs_space1:  /\s+/,
    rs:         /[\-\+&]/,
    rs_space2:  /\s+/,
    no_2:       /[A-Z0-9]+/
  }
);

// SUITE 100-150
// SUITES 100-150
// STE. 100-150
// SUITE: 100-150
// SUITE #100-150
// Original Pattern: /\b(suite\s*[#:]?\s*)([A-Z0-9]+)[\-\+&]([A-Z0-9]+)/i
const SuiteExtractor = new Mutator.Extractor(
  'Suite', pattern,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, SuiteExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    // Make optionals safe
    safeOptionals( match, [ 'dot', 'sep_space1', 'separator' ,'sep_space2', 'rs_space1', 'rs_space2' ] );
    // Declare Output Group Order
    let outGroups = [
      'tag', 'dot',
      'sep_space1', 'separator', 'sep_space2',
      'no_1',
      'rs_space1', 'rs', 'rs_space2',
      'no_2'
    ];

    trim( item, [ from, to ] );

    //? RANGEFINDER
    let range = [ match.no_1, match.no_2 ],
     isSeries = rangeFinder.isSeries( ...range );
     match.rs = isSeries ? '&' : '-';
    item.addChange( to, {
      action: 'rangeFinder',
       value: outGroups.reduce( ( acc, group ) => acc += match[group], '' ),
        type: 2,
        note: 'Make ' + ( isSeries ? 'Series' : 'Range' )
    });

    //? SPACING
    // ~> 1 Space
    spacer( item, to, match, outGroups, [ 'sep_space1', 'rs_space1', 'rs_space2' ], true );
    // ~> 0 Spaces
    spacer( item, to, match, outGroups, 'sep_space2', false );


    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, 'tag' );

  },
  [
    'trim',
    new Mutator.Expander( 'Suite', /\bSte/ ),
    new Mutator.Compressor( 'Suite', /\bSuites/ ),
    new Mutator.Cleaner('period', 'Removed Exraneous "."', /\./ ),
    new Mutator.Cleaner('pound', 'Removed Extraneous "#"', /[\#]/),
    new Mutator.Cleaner('colon', 'Removed Extraneous ":"', /\:/),
    'trim'
  ]
);

module.exports = SuiteExtractor;