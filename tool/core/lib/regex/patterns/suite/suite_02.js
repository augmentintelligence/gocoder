const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim'),
       spacer = require( '../../pseudo/spacer2' ),
safeOptionals = require( '../../pseudo/safeOpt' ),
  rangeFinder = require('../../../rangeFinder/rangeFinder');

const pattern = XRegExp.build(
  `(?xi)
    ({{tag}})({{dot}}?)
    ({{sym_space1}}?)({{symbol}}?)({{sym_space2}}?)
    ({{digits}})({{letter_1}})({{sep_space1}}?)({{separator}})({{sep_space2}}?)({{letter_2}})`,
  {
           tag: /\bs(?:ui)?tes?/,
           dot: /\./,
    sym_space1: /\s+/,
        symbol: /[\#\:]/,
    sym_space2: /\s+/,
        digits: /[\d]{1,4}/,
      letter_1: /[A-Z]/,
    sep_space1: /\s+/,
     separator: /[\/\-\+&]/,
    sep_space2: /\s+/,
      letter_2: /[A-Z]/,
  }
);

// [ match.tag, match.dot, match.sym_space1, match.symbol, match.sym_space2, match.digits, match.letter_1, match.sep_space1, match.separator, match.sep_space2, match.digits, match.letter_2 ]

// SUITE 11A/B
// Original Pattern: /\b(suite)\s*([#:]?\s*([A-Z0-9]+)(?:([\/\-\+&])([A-Z]))?)/i
const SuiteExtractor = new Mutator.Extractor(
  'Suite', pattern,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, SuiteExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    // Declare Match Group Order
    let outGroups = [
      'tag', 'dot',
      'sym_space1', 'symbol', 'sym_space2',
      'digits', 'letter_1',
      'sep_space1', 'separator', 'sep_space2',
      'digits', 'letter_2'
    ];

    trim( item, [ from, to ] );

    let range = [ match.digits + match.letter_1, match.digits + match.letter_2 ],
     isSeries = rangeFinder.isSeries( ...range );

    // Make optionals safe
    safeOptionals( match, [ 'dot', 'sym_space1', 'symbol', 'sym_space2', 'sep_space1', 'sep_space2' ] );

    //? RANGEFINDER
    match.separator = isSeries ? '&' : '-';
    item.addChange(to, {
      action: 'rangeFinder',
       value: outGroups.reduce( ( acc, group ) => acc += match[group], '' ),
        type: 2,
        note: 'Make ' + ( isSeries ? 'Series' : 'Range' )
    });

    //? SPACING
    //~> 1 Space
    spacer( item, to, match, outGroups, [ 'sym_space1', 'sep_space1', 'sep_space2' ], true );
    //~> 0 Spaces
    spacer( item, to, match, outGroups, [ 'sym_space2' ], false );

    // Enforce Casing
    Mutator.Caser.exec( [ item, match ], to, 'tag' );

  },
  [
    'trim',
    new Mutator.Expander( 'Suite', /\bSte/ ),
    new Mutator.Compressor( 'Suite', /\bSuites/ ),
    new Mutator.Cleaner( 'period', 'Removed Exraneous "."', /\./ ),
    new Mutator.Cleaner( 'pound', 'Removed Extraneous "#"', /[\#]/ ),
    new Mutator.Cleaner( 'colon', 'Removed Extraneous ":"', /\:/ ),
    'trim'
  ]
);

module.exports = SuiteExtractor;