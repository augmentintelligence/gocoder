const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators');

// RR #13
const RRExtractor = new Mutator.Extractor(
  'RR', /(?:[Rr]\s*\.?\s*)(?:[Rr]\s*\.?\s+)#?\s*([\d]+)/i,
  ( item, change, from, to, match ) => {
    change.value = XRegExp.replace( match.input, RRExtractor.pattern, '' );
    item.addChange( from, change, match[0] );
  },
  [
    'trim',
    new Mutator.Expander( 'R.R.', /RR|R\sR|RR./i ),
    new Mutator.Expander( 'R.R.', /R.R[^.]/i ),
    new Mutator.Cleaner( 'pound', 'Removed Extraneous "#"', /#/ ),
    'trim'
  ]
);

module.exports = RRExtractor;