const XRegExp = require('xregexp'),
      Mutator = require('../../classes/Mutators'),
         trim = require('../trim/_trim');

const wingPattern = XRegExp.build(`(?xi)
  ({{name}}) {{space}} ({{wing}})
  `, {
     name:  /(?:\bNew\b\s+)?(?:\b(?:N(?:orth)?|E(?:ast)?|S(?:outh)?|W(?:est)?)\b)/,
    space:  /\s+/,
     wing:  /Wing/
  });

// EAST WING
const WingExtractor = new Mutator.Extractor(
  'Wing', wingPattern,
  ( item, change, from, to, match ) => {

    change.value = XRegExp.replace( match.input, WingExtractor.pattern, '' );
    item.addChange( from, change, match[0] );

    Mutator.Caser.exec( item, to );
    trim( item, [ from, to ] );
    new Mutator.Cleaner( 'slash', 'Removed Extraneous "/"', /[\/]$|^[\/]/ ).exec( item, [ from, to ] );

  },
  [
    'trim',
    new Mutator.Cleaner( 'pound', 'Removed Extraneous "#"', /#/ ),
    new Mutator.Expander( 'North', /\bN\b/ ),
    new Mutator.Expander( 'East', /\bE\b/ ),
    new Mutator.Expander( 'South', /\bS\b/ ),
    new Mutator.Expander( 'West', /\bW\b/ ),
    'trim'
  ]
);

module.exports = WingExtractor;