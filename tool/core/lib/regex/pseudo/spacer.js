

/**
 * Given a `match` object, and a capture `group`, if the match value
 * is empty or has more than one space, *modifies the match value* to
 * a single space. If a `Row` column name is provided, creates a `HistoryItem`
 * change object for the modification without the `value` property.
 *
 *
 *
 * @param {*} match
 * @param {*} group
 * @param {*} iTarget
 */
const spacer = function ( match, group, iTarget ) {

  let operationFlag = detectAddRemove( match, group );

  if ( operationFlag === false && !( /^\s+$/.test( match[ group ] ) ) )
    return new Error( 'Spacer called on value with non-whitespace characters!' );

  if ( typeof operationFlag === 'boolean' ) {

    match[ group ] = ' ';

    if ( iTarget && typeof iTarget === 'string' )
      return [ operationFlag, {
        action: operationFlag ? 'Spacer' : 'MultiSpaceToSingleSpace',
          type: 2,
          note: operationFlag ? 'Added space' : 'Trimmed Multiple Spaces to Single Space',
          from: iTarget,
            to: iTarget
      }];

    return operationFlag;

  }

};

module.exports = spacer;

/**
 * Given a `match` object, and a capture `group` name - returns:
 *  * `true`: the capture group value is empty or `undefined`.
 *  * `false`: the capture group value length is > 1.
 *  * `null`: the capture group value length is 1.
 *
 * @param {(XRegExp.match|RegExp.match)} match -
 *  A `X/RegExp` match object
 * @param {(string|number)} tag -
 *  A `XRegExp` capture group name or `RegExp` capture group index
 * @returns {(boolean|null)}
 */
function detectAddRemove( match, group ) {
  return typeof match[group] !== 'string' || match[group].length === 0 ? true :
    match[group].length > 1 ? false : null;
}