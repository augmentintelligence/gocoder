module.exports = function( match, optionalGroups ) {
  // Coerce targetGroup to array.
  if ( typeof optionalGroups === 'string' ) optionalGroups = [ optionalGroups ];
  // ~> optionalGroups: TypeChecking
  if ( !Array.isArray( optionalGroups ) )
    throw new TypeError( "[safeOptionals] 'optionalGroups' is of invalid type. Expected array." );
  // // ~> optionalGroups: match contains all targetGroup(s)
  // if ( !optionalGroups.every( group => match.hasOwnProperty( group ) ) )
  //   throw new Error( "[safeOptionals] 'optionalGroups' argument contains one or more items not found in 'match'." );
  optionalGroups.forEach( s => { if ( !match.hasOwnProperty(s) || !match[s] ) match[s] = ''; } );
};