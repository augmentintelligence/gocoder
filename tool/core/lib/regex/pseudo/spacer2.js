const Row = require( '../../row/Row' );
module.exports = spacer;

/**
 * Given a `Row`, the target field (`to`) within the `Row`,
 * a `XRegExp` `match` object, an array of strings representing
 * the `match` `groups` that make up the desired value of `to`,
 * the `targetGroup`(s) of those `groups` to adjust the amount
 * of whitespace for, and finally, whether a whitepsace character
 * is expected (`withSpace`) in `targetGroup`;
 *
 * If necessary;
 * - Adjusts it within the `match` object for future operations
 * - Adds a `HistoryItem` reflecting the change to the `Row`
 *
 * **Note**: If more than one group needs the same operation done and
 * have the same amount of target whitespace, `targetGroup` can be an
 * array.
 *
 * @param {Row} item
 *    - A `Row` item
 * @param {string} to
 *    - The target field within `Row` to make the change
 * @param {XRegExp.match} match
 *    - A `XRegExp` `match` object
 * @param {string[]} groups -
 *    - An array of strings representing the `match` group names
 *      that make up the desired value of `to`
 * @param {string[]} targetGroup -
 *    - A string, or array of strings representing the `match`
 *      group names that need whitespace coersion.
 * @param {boolean} [withSpace=true] -
 *    - If `true`, expects one whitespace character. Otherwise,
 *      expects 0.
 */
function spacer ( item, to, match, groups, targetGroup, withSpace ) {
  // Coerce targetGroup to array.
  if ( typeof targetGroup === 'string' ) targetGroup = [ targetGroup ];

  // Error Handling
    // ~> to: TypeChecking
    if ( typeof to !== 'string' )
      throw new TypeError( "[spacer] 'to' argument is of invalid type. Expected string." );
    // ~> groups: TypeChecking
    if ( !Array.isArray( groups ) )
      throw new TypeError( "[spacer] 'groups' argument is of invalid type. Expected array." );
    // ~> groups: match contains all groups
    if ( !groups.every( group => match.hasOwnProperty( group ) ) )
      throw new Error( "[spacer] 'groups' argument contains items not found in 'match'." );
    // ~> targetGroup: TypeChecking
    if ( !Array.isArray( targetGroup ) )
      throw new TypeError( "[spacer] 'targetGroup' is of invalid type. Expected array." );
    // ~> targetGroup: match contains all targetGroup(s)
    if ( !targetGroup.every( group => match.hasOwnProperty( group ) ) )
      throw new Error( "[spacer] 'targetGroup' argument contains items not found in 'match'." );
    // ~> TypeChecking: item
    if ( !( item instanceof Row ) )
      throw new TypeError( "[spacer] 'item' argument is of invalid type. Expected a 'Row'." );
    // ~> TypeChecking: withSpace
    if ( typeof withSpace !== 'boolean' ) withSpace = true;
    // ~> targetGroups contain whitespace characters only
    if ( !targetGroup.every( group => /^\s*$/.test( match[group] ) ) )
      throw new Error( "[spacer] 'match' 'targetGroup' value(s) contain non-whitespace characters." );

    let spaceTarget = withSpace ? ' ' : '';

    targetGroup.forEach( tGroup => {
      if ( match[tGroup] !== spaceTarget ) {
        let msg;
        if ( withSpace ) msg = match[tGroup].length > 1 ? 'Removed extra space.' : 'Added extra space.';
        else msg = 'Removed extra space.';
        // Adjust match value
        match[tGroup] = spaceTarget;
        // Add HistoryItem to Row
        item.addChange( to, {
          action: 'spacer',
           value: groupConcat( match, groups ),
            type: 2,
            note: msg
        });
      }
    });

}

/**
 * Given a [X]RegExp `match` object, and an array of
 * strings representing the named capture groups within
 * the `match` object to build a string with. Concatenates
 * each match capture group's value into a string.
 * @param {XRegExp.match} match -
 *  The result of an XRegExp.exec call.
 * @param {string[]} groups -
 *  An array of named capture groups within the match object
 * @returns {string}
 */
function groupConcat( match, groups ) {
  return groups.reduce( ( acc, group ) => acc += match[group], '' );
}