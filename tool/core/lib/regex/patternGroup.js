const async = require('async');

const groupExe = function ( group, item, targets ) {
  let run = false;
  async.forEach( group, g => {
    if ( !run ) run = g.exec( item, targets );
  });
};

module.exports = groupExe;

// module.exports = function( group, item, targets ) {
//   return group.some(g => g.exec( item, targets ));
// };