const Mutator = require( './Mutator' ),
          Row = require('../../row/Row'),
      XRegExp = require('xregexp');

class Caser {

  constructor( caps ) {
    this.name = 'CaseCorrector';
    this.note = 'Corrected Case';
    this.type = '2';
    this.caps = caps;
  }

  exec( item, target, matchTargets ) {

    target = coerceArray( target );

    let m;

    if ( Array.isArray( item ) && !( item instanceof Row ) ) {
      [ item, m ] = item;
    }

    // console.log( m );

    for ( let iTarget of target ) {

      let value = item instanceof Row ? item.valueOf( iTarget ) : item[ iTarget ],
         change = {
          action: this.name,
            note: this.note,
            type: this.type,
            from: iTarget,
              to: iTarget
      };

      // console.log( value );

      if ( value === undefined || value === null ) continue;

      if ( matchTargets !== undefined ) {

        matchTargets = coerceArray( matchTargets );

        // console.log( matchTargets );

        for ( let mTarget of matchTargets ) {

          let matchVal = m[ mTarget ];

          // console.log( matchVal, matchMod );

          if ( matchVal === undefined || matchVal === null )
            continue;

          let matchMod = this.caps ?
            matchVal.toUpperCase() : !( /[a-z]+/.test( matchVal ) ) ?
              toTitleCase( matchVal ) : matchVal;

          change.value = XRegExp.replace( value, matchVal, matchMod );

          if ( value !== change.value ) {
            value = change.value;
            m[ mTarget ] = matchMod;
            item.addChange( iTarget, change );
          }

        }

      } else {

        change.value = this.caps ?
          value.toUpperCase() : !( /[a-z]+/.test( value ) ) ?
            toTitleCase( value ) : value;

        if ( value !== change.value ) {
          item.addChange( iTarget, change );
        }

      }
    }
  }
}

function coerceArray( arg ) {
  return typeof arg === 'string' ? [ arg ] : Array.isArray( arg ) ? [ ...arg ] : [ ];
}

function toTitleCase ( str ) {
  var smallWords = /^(a|an|and|as|at|but|by|en|for|if|in|nor|of|on|or|per|the|to|vs?\.?|via)$/i;
  return str.replace( /[A-Za-z0-9\u00C0-\u00FF]+[^\s-]*/g , function ( match, index, title ) {
    if ( index > 0 && index + match.length !== title.length &&
      match.search( smallWords ) > -1 && title.charAt( index - 2 ) !== ":" &&
      ( title.charAt( index + match.length ) !== '-' || title.charAt( index - 1 ) === '-') &&
      title.charAt( index - 1 ).search( /[^\s-]/ ) < 0 ) {
      return match.toLowerCase();
    }
    if ( match.substr( 1 ).search( /\../ ) > -1 ) {
      return match;
    }
    return match.charAt( 0 ).toUpperCase() + match.substr( 1 ).toLowerCase();
  });
}

module.exports = Caser;