const XRegExp = require('xregexp');
const Row = require('../../row/Row');

class Mutator {

  constructor(name, note, type, pattern) {
    if (typeof type !== 'string' && pattern === undefined) {
      pattern = type;
         type = undefined;
    }
    this.change = {
      action: `${name}`,
        type: type || '2'
    };
    if (note) this.change.note = note;
    if (Object.getPrototypeOf(pattern) !== 'XRegExp') {
      this.pattern = new XRegExp(pattern);
    } else {
      this.pattern = pattern;
    }
  }

  test(v) {
    return XRegExp.test(v, this.pattern);
  }

  match(v) {
    return XRegExp.exec(v, this.pattern);
  }

  exec(item, fromTargets, toTarget, strict, callback) {
    fromTargets = typeof fromTargets === 'string' ? [fromTargets] : Array.isArray(fromTargets) ? [...fromTargets] : [];
    for (let target of fromTargets) {
      if (item instanceof Row) {
        let targetValue = item.valueOf(target);
        if ((!strict || (strict && !item.getTarget(target))) && this.test(targetValue)) {
          let change = Object.assign(this.change);
          change.from = target;
          if (toTarget !== undefined && toTarget !== null) change.to = toTarget;
          return callback(item, change, target, toTarget, this.match( targetValue ) );
        }
      } else {
        let targetValue = item[target];
        if ( this.test( targetValue ) ) {
          let change = Object.assign( this.change );
          change.from = target;
          if ( toTarget !== undefined && toTarget !== null ) change.to = toTarget;
          return callback( item, change, target, toTarget, this.match( targetValue ) );
        }
      }
    }
  }
}

module.exports = Mutator;