const XRegExp = require('xregexp'),
      Mutator = require('./Mutator');
class Compressor extends Mutator {

  constructor(name, pattern, dictionary) {
    super(`${name} Compressor`, `Compressed ${name}`, '2', pattern);
    this.name = name;
    this.dictionary = dictionary;
  }

  exec(item, targets, callback) {
    return super.exec(item, targets, undefined, false, (item, change, from, to, match) => {
      if (typeof callback === 'function') return callback(item,change,from,to,match);
      let value = item.valueOf(from);
      if (this.dictionary === undefined) {
        change.value = XRegExp.replace(value, this.pattern, this.name).trim();
        item.addChange(from, change);
        return true;
      } else if (Array.isArray(this.dictionary)) {
        this.dictionary.some(v => {
          if (XRegExp.test(value, v.rx)) {
            change.value = XRegExp.replace(value, v.rx, v.xp).trim();
            item.addChange(from, change);
            return true;
          }
        });
      } else if (typeof this.dictionary == 'object') {
        Object.keys(this.dictionary).forEach( v => {
          if (XRegExp.test(item.valueOf(from), this.dictionary[v])) {
            change.value = XRegExp.replace(item.valueOf(from), this.dictionary[v], v).trim();
            item.addChange(from, change);
          }
        });
      }
    });
  }
}
module.exports = Compressor;