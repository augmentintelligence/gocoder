const Mutator = require('./Mutator');
const trim = require('../patterns/trim/_trim');

class Extractor extends Mutator {

  constructor(name, pattern, exe, extras) {
    super(`${name} Extractor`, undefined, '2', pattern);
    this.name = name;
    this.exe = typeof exe === 'function' ? exe : undefined;
    this.extras = Array.isArray(extras) ? extras :
      (typeof extras === 'object' || extras === 'trim') ? [extras] : undefined;
  }

  exec( item, targets, cb ) {
    return super.exec(item, targets, this.name, false, (item, change, from, to, match) => {
      if (this.exe) {
        this.exe( item, change, from, to, match );
      }
      else {
        cb( item, change, from, to, match );
      }
      // FireExtras
      if (this.extras) {
        for (let extra of this.extras) {
          if (extra instanceof Mutator)
            extra.exec(item, to);
          else if (extra === 'trim')
            trim(item, [to, from]);
        }
      }
      return true;
    });
  }
}

module.exports = Extractor;