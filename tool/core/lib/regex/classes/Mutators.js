(function () {
  exports.Mutator    = require( './Mutator'   );
  exports.Extractor  = require( './Extractor' );
  exports.Expander   = require( './Expander'  );
  exports.Compressor = require( './Compressor');
  exports.Cleaner    = require( './Cleaner'   );
  exports.Casers     = require( './Caser'     );
  exports.Caser      = {};
  exports.Tagger     = require( './Tagger'    );
}).call(this);