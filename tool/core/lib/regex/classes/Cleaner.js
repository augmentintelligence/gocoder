const XRegExp = require( 'xregexp' ),
      Mutator = require( './Mutator' );

class Cleaner extends Mutator {

  constructor( name, note, pattern ) {
    if ( typeof note !== 'string' && pattern === undefined ) {
      pattern = note;
      note = undefined;
    }
    super( name+'Cleaner', note || `Cleaned ${name}`, '2', pattern );
  }

  exec( item, targets ) {
    super.exec( item, targets, undefined, false, ( item, change, from, to, match ) => {
      change.value = XRegExp.replace( item.valueOf( from ), this.pattern, '' ).trim();
      item.addChange( from, change );
    });
  }
}

module.exports = Cleaner;