const XRegExp = require('xregexp'),
      Mutator = require('./Mutator');
class Tagger extends Mutator {

  constructor(name, pattern, casing) {
    super(`${name} Tagger`, `Added ${name} Tag`, '2', pattern);
    this.name = name;
    this.case = casing;
  }

  exec(item, targets) {
    return super.exec(item, targets, undefined, false, (item, change, from, to, match) => {
      let value = item.valueOf(from),
            tag = this.name;
      if (this.case == 'upper')
        tag = tag.toUpperCase();
      else if (this.case == 'lower')
        tag = tag.toLowerCase();
      else if (this.case == 'camel')
        tag = tag[0].toUpperCase() + tag.substr(1).toLowerCase();
      tag = this.pattern.source == '^' ? (tag + ' '):(' ' + tag);
      change.value = XRegExp.replace( value, this.pattern, tag );
      item.addChange(from, change);
    });
  }
}
module.exports = Tagger;