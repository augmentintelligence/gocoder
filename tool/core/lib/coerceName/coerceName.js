const groupExe = require( '../regex/patternGroup' ),
          NAME = require('../regex/patterns/name/_name');

const coerceName = function( row, opts ) {
  // [Feature Request] Coerce English_Name Not Null
  if ( opts.fields.Name === '' ) return;
  if ( typeof row.valueOf( opts.fields.Name ) !== 'string' || row.valueOf( opts.fields.Name ).length < 1 ) {
    if (
      row.keys.includes( 'ENGLISH_NAME_ALT' ) &&
      typeof row.valueOf( 'ENGLISH_NAME_ALT' ) === 'string' &&
      row.valueOf( 'ENGLISH_NAME_ALT' ).length
    ) {
        groupExe( NAME, row, 'ENGLISH_NAME_ALT' );
        row.appendShadows( opts.fields.Name, [ 'Name' ] );
    }
    else if (
      row.keys.includes( 'English_Name_Alt' ) &&
      typeof row.valueOf( 'English_Name_Alt' ) === 'string' &&
      row.valueOf( 'English_Name_Alt' ).length
    ) {
      groupExe( NAME, row, 'English_Name_Alt' );
      row.appendShadows( opts.fields.Name, [ 'Name' ] );
    }
    else if (
      row.keys.includes( 'FRENCH_NAME' ) &&
      typeof row.valueOf( 'FRENCH_NAME' ) === 'string' &&
      row.valueOf( 'FRENCH_NAME' ).length
    ) {
      groupExe( NAME, row, 'FRENCH_NAME' );
      row.appendShadows( opts.fields.Name, [ 'Name' ] );
    }
    else if (
      row.keys.includes( 'French_Name' ) &&
      typeof row.valueOf( 'French_Name' ) === 'string' &&
      row.valueOf( 'French_Name' ).length
    ) {
      groupExe( NAME, row, 'French_Name' );
      row.appendShadows( opts.fields.Name, [ 'Name' ] );
    }
    else if (
      row.keys.includes( 'FRENCH_NAME_ALT' ) &&
      typeof row.valueOf( 'FRENCH_NAME_ALT' ) === 'string' &&
      row.valueOf( 'FRENCH_NAME_ALT' ).length
    ) {
      groupExe( NAME, row, 'FRENCH_NAME_ALT' );
      row.appendShadows( opts.fields.Name, [ 'Name' ] );
    }
    else if (
      row.keys.includes( 'French_Name_Alt' ) &&
      typeof row.valueOf( 'French_Name_Alt' ) === 'string' &&
      row.valueOf( 'French_Name_Alt' ).length
    ) {
      groupExe( NAME, row, 'French_Name_Alt' );
      row.appendShadows( opts.fields.Name, [ 'Name' ] );
    }
    else {
      row.addChange( opts.fields.name, {
        action: 'CoerceNameNotNull',
         value: 'N/A',
          type: '1',
          note: 'No Substitutions',
          from: opts.fields.Name,
            to: opts.fields.Name
      });
    }
  }
};

module.exports = coerceName;