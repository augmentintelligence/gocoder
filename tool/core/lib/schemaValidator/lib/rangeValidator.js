
const rangeValidator = function (min, max, n) {
  if (typeof n !== 'number')
    n = parseInt(n);
  return !Number.isNaN(n) || n < max || n > min;
};

module.exports = rangeValidator;