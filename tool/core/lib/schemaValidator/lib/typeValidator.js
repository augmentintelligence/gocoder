(function(){

  /**
   * Returns `true`/`false` indicating whether a given
   * value can be interpreted as *any* of the specified
   * data type(s).
   *
   * Like `validType`, but allows for multiple type options,
   * and also optionally checks whether value is empty.
   *
   * Allows for specifying `array` types and, if not that
   * or the other primitives, falls back to checking with
   * `instanceof`.
   *
   * Definition of `type`s:
   * * **''/'*'/undefined**: true
   * * **String**: *`typeof 'string'`*
   * * **Number**: *`typeof 'number'` or `typeof 'string'` and !NaN*
   * * **Array**: *`Array.isArray`*
   * * **Object**: `typeof 'object'`
   * * **Other**: *`instanceof type`*
   *
   * @param {*} value -
   *   The value to test
   *
   * @param {string} [type] -
   *   The data type used to check against
   *
   * @returns {boolean} `true`/`false`
   */
  exports.validValue = function (value, types, notEmpty) {
    if (!Array.isArray(types) && types !== undefined)
      types = [types];
    return types.some(type => this.validType(value,type) && (!notEmpty || this.notEmpty(value,type)));
  };

  /**
   * Returns `true`/`false` indicating whether a given
   * value can be interpreted as the specified data type.
   * Allows for specifying `array` types and, if not that
   * or the other primitives, falls back to checking with
   * `instanceof`.
   *
   * Definition of `type`s:
   * * **''/'*'/undefined**: true
   * * **String**: *`typeof 'string'`*
   * * **Number**: *`typeof 'number'` or `typeof 'string'` and !NaN*
   * * **Array**: *`Array.isArray`*
   * * **Object**: `typeof 'object'`
   * * **Other**: *`instanceof type`*
   *
   * @param {*} value -
   *   The value to test
   *
   * @param {string} [type] -
   *   The data type used to check against
   *
   * @returns {boolean} `true`/`false`
   */
  exports.validType = function (value, type) {
    if (type === '' || type === '*' || type === undefined)
      return true;
    switch (type) {
      case 'number':
        return typeof value === 'number' ||
             ( typeof value === 'string' &&
               !Number.isNaN( parseInt( value ) )
             );
      case 'array':
        return Array.isArray( value );
      case 'string':
        return typeof value === type;
      case 'boolean':
        return typeof value === type;
      case 'object':
        return typeof value === type;
      default:
        return value instanceof type;
    }
  };

  /**
   * Returns `true`/`false` indicating whether a given value
   * is empty, where the definition of empty varies based on
   * what data type the value is.
   *
   * Definition of Empty by `type`:
   *
   * * **Any/undefined**: *`undefined` or `null`*
   * * **String**: *[empty string]*
   * * **Number**: *`0` or `NaN`*
   * * **Array**: `[]` or `[undefined||null]`
   * * **Object**: `{}`
   *
   * @param {*} value -
   *   The value to test
   *
   * @param {string} [type] -
   *   The data type used to define `empty`.
   *
   * @returns {boolean} `true`/`false`
   */
  exports.notEmpty = function (value, type) {
    if (value === undefined || value === null)
      return false;
    switch (type) {
      case 'string':
        return value !== '';
      case 'number':
        return !Number.isNaN(value);
      case 'array':
        return value.length !== 0 &&
            !((value[0] === undefined || value[0] === null) && value.length == 1);
      case 'object':
        return Object.keys(value).length === 0;
      default:
        return true;
    }
  };

}).call(this);