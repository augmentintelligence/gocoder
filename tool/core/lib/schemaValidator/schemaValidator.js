(function () {

  const typeValidator = require('./lib/typeValidator');

  /**
   * Given an `object`, and an object containing `property` name/data type pairs,
   * tests whether the object:
   *
   * * 1) Has (if strict is `false`, at least) the specified properties.
   * * 2) If data type(s) are provided, whether the value matches one of them.
   *
   * To specify whether a specific property should be non-empty,
   * prepend the property name with `!`. If it is optional,
   * (just want to check type if it exists) - prepend property
   * name with `?`.
   *
   * @param {object} object -
   *  The object to test.
   *
   * @param {string} propTypes -
   *  The property name / data type key/pairs.
   *
   * @param {string[]} [strict] -
   *  If `true`, the object *cannot* have properties not defined in propTypes.
   *
   * @returns {boolean} `true`/`false`
   */
  exports.validObject = function (object, propTypes, strict) {
    return typeof propTypes === 'object' && (!strict || this.checkKeys(object, propTypes)) &&
      Object.keys(propTypes).every(v => this.validProperty(object, v, propTypes[v]));
  };

  /**
   * Given an `object`, and a `schema` object containing
   * property name / data type pairs, tests whether the
   * object has **only** the schema's defined properties,
   * accounting for defined but optional properties.
   *
   * @param {object} object -
   *  The object to test.
   *
   * @param {object} schema -
   *  The property name / data type key/pairs.
   *
   * @returns {boolean} `true`/`false`
   */

  exports.checkKeys = function (object, schema) {
    let schemaKeys = Object.keys(schema),
             oKeys = Object.keys(object),
      requiredKeys = schemaKeys.filter(v => v.substr(0, 2).indexOf('?') === -1).map(stripTags),
      optionalKeys = schemaKeys.filter(v => v.substr(0, 2).indexOf('?') !== -1).map(stripTags),
    hasAllRequired = requiredKeys.every(key => oKeys.includes(key));

    if (hasAllRequired)

      return oKeys.filter(
        v => (optionalKeys.length === 0 || !optionalKeys.includes(v)) &&
             !requiredKeys.includes(v)).length === 0;

    else return false;
  };

  /**
   * Strips modifier characters from a property name.
   *
   * @param {string} prop -
   *   An object key in the form of a string.
   *
   * @returns {string} -
   *   The property name without modifier characters.
   */
  function stripTags(prop) {
    if (typeof prop === 'string' && prop.length > 1) {
      if (prop[0] === '!') prop = prop.substr(1);
      if (prop[0] === '?') prop = prop.substr(1);
      return prop;
    }
  }

  /**
   * Given an `object`, a `property` name, and optionally,
   * one or more data types, tests whether the object:
   *
   * * 1) Has the specified property.
   * * 2) If data type(s) are provided, whether the value matches one of them.
   * * 3) Is non-empty (based on type's definition) if nonEmpty is `true`.
   *
   * When used in conjunction with `validObject`:
   * To specify whether a specific property should be non-empty,
   * prepend the property name with `!`. If it is optional,
   * (just want to check type if it exists) - prepend property
   * name with `?`.
   *
   * @param {object} object -
   *  The object to test.
   *
   * @param {string} property -
   *  The property name.
   *
   * @param {string[]} [dataTypes] -
   *  One or more data types. (Can use `array`)
   *
   * @param {boolean} nonEmpty -
   *  If `true`, expects the value to not only be of the expected type,
   *  but non-empty as well. (Based on the type's definition of empty)
   *
   * @returns {boolean} `true`/`false`
   */
  exports.validProperty = function (object, property, types, optional, nonEmpty) {
    if (property[0] === '!') return this.validProperty(object, property.substr(1), types, optional, true);
    if (property[0] === '?') return this.validProperty(object, property.substr(1), types, true, nonEmpty);
    return (optional && !object.hasOwnProperty(property)) || (object.hasOwnProperty(property) && typeValidator.validValue(object[property], types, nonEmpty));
  };

}).call(this);