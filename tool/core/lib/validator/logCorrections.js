const logCorrections = function ( item, total, index, corrections ) {
  let correctionLog = `\n [${index+1}/${total}] > ${item._name}:\n`;
  for (let correction of corrections) {
    correctionLog += tabbit(1) + ' - ' + correction.field + ':\n';
    correctionLog += tabbit(2) + ' - Old: ' + correction.old + '\n';
    correctionLog += tabbit(2) + ' - New: ' + correction.new + '\n';
  }
  console.log(correctionLog);

  function tabbit(n=1) { return '  '.repeat(n > 0 ? n-1 : 0); }
};

module.exports = logCorrections;