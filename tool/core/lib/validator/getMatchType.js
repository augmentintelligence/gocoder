
const getMatchType = function ( vStreet ) {
  if ( /^[\d]/.test( vStreet ) )
    return 'streetAddress';
  else if ( /^R\.R\./.test( vStreet ) )
    return 'route';
  else
    return 'unknown';
};

module.exports = getMatchType;