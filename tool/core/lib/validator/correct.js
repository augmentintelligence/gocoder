module.exports = {
  StreetAddress( item, corrections, correction ) {
    corrections.push({
      field: item.options.fields.StreetAddress,
        old: item.valueOf( item.options.fields.StreetAddress ),
        new: correction
    });
  },
  PostalCode( item, corrections, correction ) {
    corrections.push({
      field: item.options.fields.PostalCode,
        old: item.nth( item.options.fields.PostalCode, 0 ).value,
        new: correction
    });
  },
  City( item, corrections, correction ) {
    corrections.push({
      field: item.options.fields.City,
        old: item.valueOf( item.options.fields.City ),
        new: correction
    });
  }
};