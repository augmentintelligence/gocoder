const correct = require( './correct' );

module.exports = {
  valid: {
    StreetAddress( item, address, corrections ) {
      // (Optional) Push Correction Logging
      if ( Array.isArray( corrections ) )
        correct.StreetAddress( item, corrections, address );
      item.addChange(item.options.fields.StreetAddress, {
        action: 'addressValidator',
          value: address,
          type: '3',
          note: 'Validator Correction',
          from: item.options.fields.StreetAddress,
            to: item.options.fields.StreetAddress
      });
    },
    PostalCode( item, postalCode, corrections ) {
      // (Optional) Push Correction Logging
      if ( Array.isArray( corrections ) )
        correct.PostalCode( item, corrections, postalCode );
      item.addChange( item.options.fields.PostalCode, {
        action: 'addressValidator',
         value: postalCode,
          type: 3,
          note: 'Validator Correction',
          from: item.options.fields.PostalCode,
            to: item.options.fields.PostalCode
      });
    },
    City( item, city, corrections ) {
      // (Optional) Push Correction Logging
      if ( Array.isArray( corrections ) )
        correct.City( item, corrections, city );
      item.addChange(item.options.fields.City, {
        action: 'addressValidator',
          value: city,
          type: '3',
          note: 'Validator Correction',
          from: item.options.fields.City,
            to: item.options.fields.City
      });
    }
  },

  invalid( item, field, corrections ) {
    if ( Array.isArray( corrections ) )
      correct[field]( item, corrections, 'INVALID DATA' );
    item.addChange( item.options.fields[field], {
      action: 'addressValidator',
       value: 'INVALID DATA',
        type: 3,
        note: 'Validator Invalidation',
        from: item.options.fields[field],
          to: item.options.fields[field]
    });
  },

  missing( item, field, corrections ) {
    if ( Array.isArray( corrections ) )
      correct[field]( item, corrections, 'MISSING DATA/CONFIDENTIAL' );
    item.addChange( item.options.fields[field], {
      action: 'addressValidator',
       value: 'MISSING DATA/CONFIDENTIAL',
        type: 3,
        note: 'Validator Missing Data Invalidation',
        from: item.options.fields[field],
          to: item.options.fields[field]
    });
  }
};