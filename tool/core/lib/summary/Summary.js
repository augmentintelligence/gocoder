(function(){

  module.exports = class Summary {

    constructor(name, report) {
      this.name = name;
      this.report = report;
    }

    get keys() {
      return Object.keys(this.report);
    }

    get toString() {
      let msg = `${this.name}:\n`;
      for (let col of this.keys) {
        msg += `  ${col}:\n`;
        for (let i = 0, n = this.report[col].length; i < n; i++) {
          let rCol = this.report[col][i];

          //!NOTE: Added condition to not print extraction/appends if they don't change
          if ( /* moves(this.report, col, i) || */ validExtraction(i, n, this.report[col]) && notEmpty(rCol.note)) msg += `${this.bullet(1, i)} ${rCol.note}\n`;

          //!NOTE: Added condition to only print non-empty values to prevent cluttering, add back if necessary.
          if (notEmpty(rCol.value) && (/* moves(this.report, col, i) || */ validExtraction(i, n, this.report[col]))) msg += `${this.bullet(2)} "${rCol.value}"\n`;

          //!NOTE: Added the rCol.shadow.length condition to prevent cluttering, add back if necessary.
          if (rCol.hasOwnProperty('shadow') && rCol.shadow !== col && rCol.shadow.length > 1 && validExtractionShadow(i, this.report[col])) {
            for (let s = 0, l = rCol.shadow.length; s < l; s++) {
              if ( notEmpty(rCol.shadow[s].note)  )
                msg += `${this.bullet(2, s)} ${rCol.shadow[s].note}\n`;
              if ( notEmpty(rCol.shadow[s].value) )
                msg += `${this.bullet(3)} "${rCol.shadow[s].value}"\n`;
            }
          }
        }
      }
      return msg;
    }

    bullet(depth, num) {
      return `    `.repeat(typeof depth == 'number' ? depth : 1) + (typeof num == 'number' ? num + ')' : '-');
    }

  };

  function validExtractionShadow(index, reportColumn) {
    return reportColumn[index].shadow[reportColumn[index].shadow.length - 1].value != reportColumn[index - 1].value;
  }


  // Tests whether there are changes between extraction & shadowAppend
  function validExtraction(index, reportLength, reportColumn) {
    return (
       (
         index === reportLength - 1 || index === 0 ) ||
         reportColumn[ index - 1 ].value !== reportColumn[ index + 1 ].value

       ) && (

         index === 0 ||
         reportColumn[ index - 1 ].value !== reportColumn[ index ].value

       ) && (

         index < 2 ||
         reportColumn[ index - 2 ].value !== reportColumn[ index ].value

       );
  }

  function notEmpty(value){
    return value !== '' && value !== undefined;
  }

  /* function moves(report, column, index) {
    let col = report[column], rCol = col[index];
    if (!rCol.hasOwnProperty('shadow'))
      return true;
    let shadow = rCol.shadow;
    return shadow[shadow.length - 1].to !== column;
  } */

}).call(this);