
const ReportItem = function (historyItem, isShadow) {
  this.note = historyItem.note;
  this.value = historyItem.value;
  if (historyItem.to !== historyItem.from) {
    if (!isShadow)
      this.shadow = historyItem.to;
    else this.to = historyItem.to;
  }
};

module.exports = ReportItem;