(function(){

  const ReportItem = require('./ReportItem'),
         sValidate = require('../../schemaValidator/schemaValidator'),
            schema = require('../data/HistoryItemSchema');

  const HistoryItem = function (changeObject, isShadow) {
    if (sValidate.validObject(changeObject,schema)) {
      this.action = changeObject.action;
       this.value = changeObject.value;
        this.type = parseInt(changeObject.type);
        this.note = changeObject.note;
        this.from = changeObject.from;
          this.to = changeObject.to;
      this.report = new ReportItem(changeObject, Boolean(isShadow));
    } else {
      let requirements = ['action', 'value', 'type', 'note', 'from', 'to'];
      for (let r of requirements) {
        if (!changeObject.hasOwnProperty(r)) console.log(`[HistoryItem] Missing ${r}`);
      }
      return null;
    }
  };

  module.exports = HistoryItem;

}).call(this);