(function(){
  const History = require('./History'),
    HistoryItem = require('./lib/HistoryItem');

  /**
   * **HistoryShadow Class:**
   * Like `History`, creates a change-history tracking
   * wrapper around a given value.
   *
   * A shadow differs from a regular value in that; for
   * the purpose of distilling fine-grain sub-components
   * from a desired output (ie. `Address Line 2` contains
   * a mixed bag of elements) - sub-components that can be
   * defined but are not their own output field are moved
   * to temporary columns that are not part of the output
   * (shadows).
   *
   * This makes it easier to perform validation/mutation
   * operations on them in an isolated environment.
   *
   * The constructor is mostly the same, the difference
   * being that it also accepts a function that defines how
   * the subcomponent gets re-added back into a non-shadow
   * field.
   *
   * **Example**:
   * * Shadow field `UNIT`, Value: `'2A'` => `'UNIT 2A'`
   *
   * Also, has a dedicated property to mark which of the
   * non-shadow properties/fields this value was extracted
   * from.
   *
   * As with the `History` class, adds methods for outputting,
   * validating, and managing these changes throughout
   * the history of the element's value.
   *
   * @extends History
   *
   * @param {string}  value -
   * The element's initial value.
   *
   * @param {string}  [name] -
   * An optional name/descriptor for the element.
   *
   * @param {boolean} [strict=true] -
   * If `false`, accepts changes to this element's history
   * even if the value hasn't changed.
   *
   * @returns {History} -
   * A class object to manage changes for an object property.
   */
  class HistoryShadow extends History {
    constructor( name, value, from, appendRule ) {
      super( name, value, true );
      this.history = ( value instanceof History ) ?
        value.history : [new HistoryItem({
          action: 'constructor',
           value: value,
            type: '0',
            note: 'Initial Value',
            from: from,
              to: name
        }, true)];
      this.append = appendRule || function ( v ) { return v; };
    }

    /**
     * Adds a history item to this element's history.
     * Accepts an object that looks like:
     *
     ** **action**: *string*,
     **  **value**: *,
     **   **type**: *number*,
     **   **note**: *string*,
     **   **from**: *string*,
     **     **to**: *string*
     *
     * If this element History has its `strict` property set to true,
     * only accepts a change if the `value` differs from the most
     * recent/previous `value`.
     *
     * @param {object} change -
     * A valid history item.
     */
    addChange( change ) {
      change = new HistoryItem( change, true );
      if ( Object.keys( change ).length && ( !this.strict || this.value !== change.value ) )
        this.history.push( change );
    }
  }

  module.exports = HistoryShadow;

}).call(this);