const HistoryItem = require('./lib/HistoryItem'),
           schema = require('./data/HistoryItemSchema'),
        sValidate = require('../schemaValidator/schemaValidator'),
       validRange = require('../schemaValidator/lib/rangeValidator');

module.exports = class History {

  /**
   * **History Class:**
   * Creates a change-history tracking wrapper around a given value.
   * Adds methods for outputting, validating, and managing these
   * changes throughout the history of the element's value.
   *
   * @param {string}  value -
   * The element's initial value.
   *
   * @param {string}  [name] -
   * An optional name/descriptor for the element.
   *
   * @param {boolean} [strict=true] -
   * If `false`, accepts changes to this element's history
   * even if the value hasn't changed.
   *
   * @returns {History} -
   * A class object to manage changes for an object property.
   */
  constructor( name, value, strict ) {
    this.strict = strict === undefined ? true : Boolean( strict );
    this.history = ( value instanceof History ) ?
      value.history : [ new HistoryItem({
          action:'constructor',
           value: value,
            type: '0',
            note: 'Initial Value',
            from: name,
              to: name
      })];
  }

  /**
   * Validates history items. Returns `true` if `obj` has **all**
   * of the following properties with their values matching the
   * corresponding data types:
   *
   ** **action**: *string*,
   **  **value**: *,
   **   **type**: *number*,
   **   **note**: *string*,
   **   **from**: *string*,
   **     **to**: *string*
   *
   * @param {object} obj -
   *   An object to test whether it's a valid input to be a HistoryItem.
   */
  static validHistoryItem( obj ) {
    return sValidate.validObject( obj, schema, true );
  }

  /**
   * Returns the last/latest value.
   * @readonly
   */
  get value () {
    return this.nth().value;
  }

  /**
   * Returns `true` if this element's history has a length
   * greater than 1, and if the initial & final values are
   * **different**, even if there are mutations between.
   *
   * If the only change is a case correction, it does not
   * count as a change (reduce noise)
   * @readonly
   */
  get hasChange () {
    return this.history.length > 1 &&
      this.nth( 0 ).value !== this.nth().value &&
      String(this.nth( 0 ).value).toUpperCase() !== String(this.nth().value).toUpperCase();
  }

  /**
   * Returns the highest numeric `type` value from
   * this element's history.
   * @readonly
   */
  get highestChange() {
    return this.hasChange ?
      Math.max( ...this.historyFor( 'type' ) ) : 0 ;
  }

  /**
   * Returns an array of objects containing all historic
   * values & change notes for this element.
   * @returns {{note:string,value:string}[]}
   * @readonly
   */
  get changeReport() {
    return this.history.map( historyItem => historyItem.report );
  }

  //! Added filtering for quieting CaseCorrector
  get stats () {
    let stat = { name: this.nth( 0 ).to, change: false };
    if ( this.hasChange ) {
      stat.change = true;
      stat.operations = this.historyFor( 'action' ).filter( v => v !== 'constructor' && v !== 'ShadowAppend' && v !== 'CaseCorrector' && !( /Extractor/.test( v ) ) );
    }
    return stat;
  }

  nth( n ) {
    return this.history[
      validRange( 0, this.history.length - 1, n ) ?
        parseInt( n ) : this.history.length - 1
    ];
  }

  /**
   * Given a valid `HistoryItem` `property` name, returns an array
   * containing all historic values for that property.
   * @param {string} property -
   * The `HistoryItem` property name from which to get all values of.
   * @returns {(string|number)[]}
   */
  historyFor( p ) {
    return !this.history[0].hasOwnProperty( p ) ?
      console.error( `[History.historyFor] Notice - invalid property: ${p}, ${this.history}` ) :
      this.history.map( v => v[ p ]);
  }

  /**
   * Adds a history item to this element's history.
   * Accepts an object that looks like:
   *
   ** **action**: *string*,
   **  **value**: *,
   **   **type**: *number*,
   **   **note**: *string*,
   **   **from**: *string*,
   **     **to**: *string*
   *
   * If this element History has its `strict` property set to true,
   * only accepts a change if the `value` differs from the most
   * recent/previous `value`.
   *
   * @param {object} change -
   * A valid history item.
   */
  addChange( change ) {
    change = new HistoryItem( change );
    if ( Object.keys( change ).length && ( !this.strict || this.value !== change.value ) )
      this.history.push( change );
  }

};