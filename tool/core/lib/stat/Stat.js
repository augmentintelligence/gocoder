const operationClass = require( './lib/operationClass' ),
        shiftArgsFwd = require( '../util/shiftArgsFwd' ),
            getMoves = require( './lib/getMoves' );

class Stat {

  constructor( Row ) {
    this.name   = Row._name;
    this.fields = Row.options.targetFields.map( key => Row[key].stats )
                       .concat(Row.shadows.map( key => Row.shadow[key].stats ));

    let moves = getMoves(Row);
    // console.log(moves);
    if (moves) moves.forEach( move => {

      // console.log(this.fields.filter(field => field.name == move.from)[0].operations);

      //! TODO: Figure out why this needed to be changed - Pharmacies.5 (Only when Validation is run for some reason)
      //? I mean... this way is more foolproof but it shouldn't need to be

      // Add Move Operation to Destination Field
      let dest = this.fields.filter( field => field.name == move.to )[0];
      if ( dest ) {
        if ( !dest.hasOwnProperty( 'operations' ) ) dest.operations = [];
        dest.operations.push( move.item + 'Move' );
      }

      // Re-Add Filtered-out Extract Operation to Source Field
      let source = this.fields.filter( field => field.name == move.from )[0];
      if ( source ) {
        if ( !source.hasOwnProperty( 'operations' ) ) source.operations = [];
        source.operations.push( move.item + 'Extract' );
      }

    });

    // Flag Main Fields
    Row.options
       .targetFields
       .forEach( target => {
          this.fields.filter( field => field.name == target )[0].main = true;
        });

    // Flag Shadow Fields
    Row.shadows
       .forEach( sKey => {
          this.fields.filter( field => field.name == sKey )[0].main = false;
        });
  }

  /**
   * Returns `true` if any of its fields have a change
   * @returns {boolean}
   */
  get changed() {
    return this.fields.some( field => field.change );
  }

  /**
   * Returns an array of field names. Optionally,
   * filter this list to get just main fields, or
   * just shadow fields, and only changed fields,
   * or only unchanged fields.
   *
   * @param {(boolean|null)} [main=null] -
   *   - If not defined, get all field names
   *   - If `true`, get main field names only
   *   - If `false`, get shadow field names only
   *
   * @param {(boolean|null)} [change=null] -
   *   - If not defined, get all fields regardless of change
   *   - If `true`, get fields with changes only
   *   - If `false`, get fields with no changes only
   *
   */
  fieldList( fields, main = null, change = null ) {

    let targetFields = [];

    // Coerce to array
    if ( typeof fields == 'string' )
      fields = [ fields ];

    // If filtered manually, make sure they're valid names
    if ( Array.isArray( fields ) ) {
      targetFields = this.fields.filter( field => fields.includes( field.name ) );
      if ( !targetFields.length ) targetFields = [];
    } else {
      // If fields was not meant to be defined, shift Arguments  Forward
      if ( fields !== undefined )
        [ fields, main, change ] = shiftArgsFwd( [ fields, main, change ] );
      // Default to all available fields
      targetFields = this.fields;
    }

    return targetFields ?
      targetFields.filter( field => main   == null || ( main ? field.main : !field.main ) )
      .filter( field => change == null || ( change ? field.change : !field.change ) )
         .map( field => field.name ) : [];
  }

  /**
   * Given an array of field names (or all if `fieldList`
   * is `undefined`) - returns an array of operations performed
   * on each. Option to only provide uniques.
   *
   * @param {string[]} fieldList -
   *   One or more field names to search in
   *
   * @param {boolean} unique -
   *   If `true`, only provides unique operation names -
   *   the alternative is used for counting occurrences.
   */
  operationList( fieldList ) {
    let list = [];

    // Secure Argument
    fieldList = (typeof fieldList == 'string' || Array.isArray( fieldList )) ?
      this.fieldList( fieldList ) : this.fieldList();

    // Validate Targets
    let targetFields = this.fields.filter( field => fieldList.includes( field.name ) && field.hasOwnProperty( 'operations' ) );

    // Make List
    for ( let field of targetFields ) {
      list = list.concat( field.operations );
    }

    return list;
  }

  classList( operationList ) {
    if ( !Array.isArray( operationList ) ) operationList = this.operationList();
    return operationList.map( operation => operationClass( operation ) );
  }

  get moveList() {
    return this.operationList().filter( operation => /Move/.test( operation ) ).map( o => o.replace( /Move/, '' ) );
  }


  /**
   * Given a field's name, returns the stat
   * object for it.
   * @param {string} name -
   *   The field name found in fields[x].name
   */
  getField( name ) {
    name = typeof name == 'string' ? name : name.hasOwnProperty( 'name' ) ? name.name : null;
    if ( name ) return this.fields.filter( field => field.name == name )[0];
  }

}

module.exports = Stat;