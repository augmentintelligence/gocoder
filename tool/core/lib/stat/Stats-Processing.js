const listAll   = require( './lib/listAll' );

const RowStats = require( './process/RowStats' ),
fieldCategories = require( './process/fieldCategories' ),
    fieldNames = require('./process/fieldNames'),
operationStats = require('./process/operationStats');

module.exports = function( stats ) {
  let fieldList = listAll.fieldsList( stats );
  return {
    row: new RowStats( stats, fieldList ),
    field: {
      category: fieldCategories( stats, fieldList, [ 'all', 'main', 'shadow' ] ),
          name: fieldNames( stats, fieldList )
    },
    operation: operationStats( stats, fieldList )
  };
};