const listAll = require( '../lib/listAll' ),
  listToCount = require('../lib/listToCount'),
    opToClass = require('../lib/operationClass'),
    toPercent = require('../lib/toPercent');

const stats = require('../../../testObjects-stats.json');

let out = '';

out += header( 'Summary', 1 );
out += header( 'Changes', 2 );
// out += line( '- ' + stats.row.ratio + ' of rows were changed (' + stats.row.changed + '/' + stats.row.total + ')' );
out += line( '- Rows:       ' + stats.row.ratio + '  (' + stats.row.changed + '/' + stats.row.total + ')' );
out += line( '- Fields:     ' + stats.field.category.all.ratio + ' (' + stats.field.category.all.changed+'/'+stats.field.category.all.total + ')' );
out += line( '- Operations: ' + stats.row.operations.total );
out += line( '- Moves:' );

for ( let move of Object.keys( stats.row.moves ) ) {
  out += line( '  - ' +  move + ':   ' + stats.row.moves[move].moves + '/' + stats.row.moves[move].total + ' (' + stats.row.moves[move].ratio + ')' );
}

let classes = Object.keys( stats.row.operations.byClass );
let ops = Object.keys( stats.row.operations.byOperation );

// for ( let c of classes ) {
//   out += line('  - ' + c + ':'+  ' '/* .repeat( Math.abs(4/(c.length-1)) * 3 ) */ + stats.row.operations.byClass[c] + ' (' + toPercent(stats.row.operations.byClass[c] / stats.row.operations.total ) + ')');
//   let classOps = ops.filter( o => opToClass(o) == c );
//   for ( let o of classOps ) {
//     out += line('    - ' + o + ':' + ' ' + stats.row.operations.byOperation[o] + ' (' + toPercent(stats.row.operations.byOperation[o] / stats.row.operations.total ) + ')' )
//   }
//   out += '\n';
// }

console.log(out);


function header( header, level ) { return '#'.repeat( typeof level !== 'number' ? 1 : level ) + ' ' + header + '\n'; }
function line ( line ) { return line + '\n'; }