
/**
 * Given a list of items, counts the number of occurrences
 * of each unique value.
 * [`CountObject`]
 * @param {string[]} list -
 *  The list to count occurrences of unique values for.
 *
 * @returns {Object.<string, number>} -
 * { `itemName`/`occurenceCount` pairs }
 */
const listToCount = function( list ) {
  let counts = {};
  for ( let item of list ) {
    counts[ item ] = !Object.keys( counts ).includes( item ) ? 1 : ++counts[ item ];
  }
  return counts;
};

module.exports = listToCount;