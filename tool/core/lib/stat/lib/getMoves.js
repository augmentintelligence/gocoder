
const getMoves = Row => {
  let moves = [];
  Row.shadows.forEach( sKey => {
    let from = Row.nth(sKey,0).from,
          to = Row.keys.filter( key => Row[key].historyFor('from').includes(sKey))[0];
    if ( from !== to ) moves.push( {item:sKey, from: from, to: to } );
  });
  return moves;
};

module.exports = getMoves;