/**
 * Given a number value, converts it to a ###.# percentile string.
 * @param {number} value -
 *  The value to convert to a percentage.
 * @returns {string} -
 *  The `value` converted into a percentage rounded to the
 *  nearest tenth decimal.
 */
const toPercent = function( value ) {
  return String(Math.ceil(value * 1000) / 10) + '%';
};

module.exports = toPercent;