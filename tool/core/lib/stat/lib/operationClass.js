
// Get an operation's class type
function operationClass(operation) {
  if (/Cleaner/.test(operation))
    return 'Cleaner';
  if (/Expander/.test(operation))
    return 'Expander';
  if (operation === 'CaseCorrector')
    return 'Caser';
  if (/Tagger/.test(operation))
    return 'Tagger';
  if (/Move/.test(operation))
    return 'Mover';
  if (/Extract/.test(operation))
    return 'Extractor';
  if (/Valid/.test(operation))
    return 'Validator';
  else return 'Other';
}

module.exports = operationClass;