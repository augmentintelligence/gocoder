
/**
 * Given a `countObject`, sums all their values
 * into one number.
 *
 * @param {Object.<string,number>} countObject -
 *  An object describing list occurrence counts as `listItem`/`count` pairs.
 *
 * @returns {number} -
 *  The sum of all counts in the object.
 *
 */
const sumCounts = function( countObject ) {
  return Object.keys( countObject ).reduce( ( count, field ) => count += countObject[ field ], 0 );
};

module.exports = sumCounts;