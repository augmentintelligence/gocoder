( function ( ) {

  // Dependencies
  const arr = require( '@3lessthan/arraytools' );
  const arrayFlatten = arr.flatten,
         arrayUnique = arr.unique;

  const countList = require( './countAll' ).countList;
  const ratioList = require( './fieldPercent' );

  // Main Function
  const listAll = ( stats, handler ) => arrayFlatten( stats.map( stat => handler( stat ) ) );

  /**
   * Given a set of stats, provides a list of fields between
   * them all. Provides filtering options.
   *
   * @param {Stat[]} stats -
   *   An array of `Stat` objects.
   *
   * @param {(string[]|boolean)} [fields] -
   *  [**Optional**] Filter list to only include specific fields.
   *  If you don't want to use this - you can use the desired
   *  value for `change` and it will register.
   *
   * @param {boolean|null} [change=null] -
   *  - `null`: Includes all fields regardless of whether it changed.
   *  - `true`: Only includes fields that have changed.
   *  - `false`: Only includes fields that have **not** changed.
   *
   * @param {boolean|null} [main] -
   *  - `null`: Includes both *main* and *shadow* fields.
   *  - `true`: Only includes *main* fields.
   *  - `false`: Only includes *shadow* fields.
   */
  const fields = ( stats, fields, main = null, change = null ) => listAll (
    stats, stat => stat.fieldList( fields, main, change )
  );

  /**
   * Given a set of stats and the target type, provides a set
   * of lists for all / changed / unchanged fields, both unique
   * and full for each.
   *
   * @param {Stat[]} stats -
   *   An array of `Stat` objects
   *
   * @param {(null|boolean)} [main=null] -
   *   - `null` : Both *main* and *shadow* fields
   *   - `true` : *Main* fields only
   *   - `false`: *Shadow* fields only
   */
  const fieldsGroup = function ( stats, main ) {
    let group = Object.create({}),
       titles = [ 'all', 'changed', 'unchanged' ];
    for ( let title of titles ) {
      let changed = title == 'all' ? null : ( title == 'changed' );
      let fullList = fields( stats, undefined, main, changed );
      group[ title ] = { full: fullList, unique: arrayUnique( fullList ) };
      group[ title ].counts = countList( group[ title ] );
      group[ title ].counts.total = fullList.length;

    } return ratioList( group );
  };

  const fieldsList = function ( stats ) {
    let list = {}, titles = [ 'all', 'main', 'shadow' ];
    for ( let title of titles ) {
      let main = title == 'all' ? null : ( title == 'main' );
      list[ title ] = fieldsGroup( stats, main );
    } return list;
  };

  /**
   * Given a set of stats, and a `fieldList` -
   * provides a list of operations performed between all of them.
   * Provides optional filtering for a unique-only list.
   *
   * @param {Stat[]} stats -
   *   An array of `Stat` objects.
   *
   * @param {string[]} [fieldList] -
   *  - If no `fieldList` is provided, uses all fields.
   *  - Otherwise, only checks fields whose names are within the list.
   *
   */
  const operations = ( stats, fieldList ) => listAll (
    stats, stat => stat.operationList( fieldList )
  );

  /**
   * Given a set of stats, and an `operationList` -
   * maps each operation in the list to its associated class.
   * Provides optional filtering for a unique-only list.
   *
   * @param {Stat[]} stats -
   *   An array of `Stat` objects.
   *
   * @param {string[]} [operationList] -
   *  - If no `operationList` is provided, uses all operations within `stats`.
   *  - Otherwise, only checks operations whose names are within the list.
   */
  const classes = ( stats, operationList ) => listAll (
    stats, stat => stat.classList( operationList )
  );

  const opToClasses = (stats,operationList) => stats[0].classList( operationList );

  /**
   * Given a set of stats, provides a list of *shadow* field names
   * that have had their value moved from one *main* field to another.
   * Provides optional filtering for a unique-only list.
   *
   * @param {Stat[]} stats -
   *   An array of `Stat` objects.
   */
  const moves = ( stats ) => listAll ( stats, stat => stat.moveList );

  exports.listAll     = listAll;
  exports.fields      = fields;
  exports.operations  = operations;
  exports.classes     = classes;
  exports.moves       = moves;
  exports.fieldsGroup = fieldsGroup;
  exports.fieldsList  = fieldsList;
  exports.opToClasses = opToClasses;

}).call(this);