const countList = function( list ) {
  let counts = {};
  list.unique.forEach( item => counts[item] = 0 );
  list.full.forEach( item => ++counts[item] );
  return counts;
};

const sumCounts = function( countObject ) {
  return Object.keys(countObject).reduce( (count,key) => count += countObject[key], 0 );
};

exports.countList = countList;
exports.sumCounts = sumCounts;