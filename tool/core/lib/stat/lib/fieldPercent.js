const toPercent = require( './toPercent' );

const fieldRatio = ( group, changed, target ) => {
  return toPercent( group[ changed ? 'changed' : 'unchanged' ].counts[ target ] / group.all.counts[ target ] );
};

const fieldsRatio = ( group, changed ) => {
  let fields = Object.create({});
  group[ changed ? 'changed' : 'unchanged' ].unique.forEach( field => fields[field] = fieldRatio( group, changed, field ) );
  return fields;
};

const fieldsGroupRatio = ( group ) => {
  let sections = [ 'changed', 'unchanged' ];
  sections.forEach( section => {
    group[ section ].ratios = fieldsRatio( group, section == 'changed' );
  });
  return group;
};

module.exports = fieldsGroupRatio;