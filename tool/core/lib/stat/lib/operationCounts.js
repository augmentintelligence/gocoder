const toPercent = require('./toPercent');

function operationCounts(opListCounts, listTotal) {
  Object.keys(opListCounts).forEach(op => {
    opListCounts[op] = {
      count: opListCounts[op],
      ratio: toPercent(opListCounts[op] / listTotal)
    };
  }); return opListCounts;
}

module.exports = operationCounts;