const toPercent = require( '../lib/toPercent' ),
    listToCount = require( '../lib/listToCount' ),
        listAll = require( '../lib/listAll' ),
       countAll = require( '../lib/countAll' ),
  operationCounts = require( '../lib/operationCounts' );

function fieldNames(stats, fieldList) {
  let fields = {},
    fieldNames = fieldList.all.all.unique;

  fieldNames.forEach(field => {
    fields[field] = fieldName(stats, fieldList, field);
  });

  return fields;
}

function fieldName(stats, fieldList, fieldName) {
  let fieldChangeTotals = fieldList.all.changed.counts,
            fieldTotals = fieldList.all.all.counts[fieldName],
                  field = {};

  field.total = fieldTotals;
  field.changed = fieldChangeTotals.hasOwnProperty(fieldName) ? fieldChangeTotals[fieldName] : 0;
  field.ratio = toPercent(field.changed / field.total);

  let operationList = listAll.operations(stats, fieldName),
    operationClassList = listAll.opToClasses(stats, operationList),
    operationListCounts = listToCount(operationList),
    operationClassListCounts = listToCount(operationClassList),
    opListTotals = countAll.sumCounts(operationListCounts);

  field.operations = {
    total: opListTotals,
    byClass: operationCounts(operationClassListCounts, opListTotals),
    byOperation: operationCounts(operationListCounts, opListTotals)
  };

  return field;
}

module.exports=fieldNames;