const toPercent = require( '../lib/toPercent' ),
    listToCount = require( '../lib/listToCount' ),
        listAll = require( '../lib/listAll' ),
       countAll = require( '../lib/countAll' ),
  operationCounts = require( '../lib/operationCounts' );

function fieldCategories(stats, fieldList, categories) {
  let out = {};
  for (let category of categories) {
    out[category] = fieldCategory(stats, fieldList[category]);
  } return out;
}

function fieldCategory(stats, fieldListbyCategory) {
  let total = fieldListbyCategory.all.counts.total,
    changed = fieldListbyCategory.changed.counts.total;

  let opFieldList = fieldListbyCategory.all.unique,
    opList = listAll.operations(stats, opFieldList),
    opClassList = listAll.opToClasses(stats, opList),
    opListCounts = listToCount(opList),
    opClassListCounts = listToCount(opClassList),
    opListTotal = countAll.sumCounts(opListCounts);


  return {
    total: total,
    changed: changed,
    ratio: toPercent(changed / total),
    operations: opListTotal
  };
  // return {
  //   total: total,
  //   changed: changed,
  //   ratio: toPercent(changed / total),
  //   operations: {
  //     total: opListTotal,
  //     byClass: operationCounts( opClassListCounts, opListTotal ),
  //     byOperation: operationCounts( opListCounts, opListTotal )
  //   }
  // };
}

module.exports = fieldCategories;