const toPercent = require( '../lib/toPercent' ),
    listToCount = require( '../lib/listToCount' ),
        listAll = require( '../lib/listAll' ),
       countAll = require( '../lib/countAll' );

const RowStats = function ( stats, fieldList ) {

  this.total = stats.length;
  this.changed = stats.filter(stat => stat.changed).length;
  this.ratio = toPercent( this.changed / this.total );

  let operationsList = listAll.operations(stats);
  let operationsCount = listToCount(operationsList);

  this.operations = {
    total: countAll.sumCounts(operationsCount),
    byClass: listToCount(listAll.classes(stats)),
    byOperation: operationsCount
  };

  let allMoves = listAll.moves(stats);
  let movesCount = listToCount(allMoves);
  Object.keys(movesCount).forEach(item => {
    movesCount[item] = {
      total: fieldList.all.all.counts[item],
      moves: movesCount[item],
      ratio: toPercent(movesCount[item] / fieldList.all.all.counts[item])
    };
  });

  this.moves = movesCount;

};

module.exports=RowStats;


    /* rows: {
      total: totalRows,
    changed: totalRowsChanged,
      ratio: toPercent( totalRowsChanged / totalRows ),
 operations: {
            total: countAll.sumCounts( listToCount( listAll.operations( stats ) ) ),
          byClass: listToCount( listAll.classes( stats ) ),
      byOperation: listToCount( listAll.operations( stats ) )
      },
      moves: movesCount( stats, fieldList )
    }, */