const listAll = require( '../lib/listAll' ),
  listToCount = require( '../lib/listToCount' ),
    opToClass = require( '../lib/operationClass' ),
    toPercent = require( '../lib/toPercent' );

/**
 * {
 *   total: {number},
 *   class: {
 *     Extractor: {
 *       total: {number},
 *       ratio: {string},
 *       fields: {
 *         {fieldName}: {
 *           total: {number},
 *           ratio: {string}
 *         }
 *       }
 *     }
 *   },
 *   name: {
 *     UnitExtract: {
 *       total: {number},
 *       ratio: {string},
 *       fields: {
 *         {fieldName}: {
 *           total: {number},
 *           ratio: {string}
 *         }
 *       }
 *     }
 *   }
 * }
 */
function operationsMain( stats, fieldList ) {

  // Initialize Object + Populate Total
  let operations = {
    total: listAll.operations( stats ).length
  };

  // Populate `class` & `name` with embedded function
  operations.class = operationTypes( 'classes' );
  operations.name  = operationTypes( 'operations' );

  // console.log( fieldList.all.changed.unique );
  // console.log( stats[3].fields );
  // console.log( listAll.operations( stats, [ 'Unit' ] ));
  // console.log(listToCount(listAll.classes(stats)));

  return operations;

  function operationTypes( type ) {

    // Get the appropriate list ( 'classes' or 'operations' )
    let typeList       = listAll[ type ]( stats ),
        typeListCounts = listToCount( typeList );

    // Instead of making a new object to contain more than just
    // Class|Operation:Count pairs, directly modify the counts object.
    Object.keys( typeListCounts ).forEach( typeListName => {

      // Instead of making another function outside the scope of this one and
      // passing the original arguments, nest it here to only pass the new required
      // Arguments ( item, total ) [The original key/value pair in `typeListCounts`]
      typeListCounts[ typeListName ] = ( ( item, total ) => {


        // This also has the added benefit of being able to reference
        // the total operations count in the parent object without
        // having to recalculate or pass its value as an additional argument
        let opObj = {
          total: total,
          ratio: toPercent( total / operations.total )
        };

        // Here we have another embedded function to save on argument
        // passing and namespace cluttering(ish)*
        //? Previously, this was 3 functions but only the topmost was exported
        opObj.fields= ( ( item, total ) => {
          let opO = Object.create({});

          // `fieldList.all.changed.unique` is an array of all unique fieldNames that were changed
          // This, depending on the `type` (classes|operations), filters the list according to whether
          // the operation matches the current `typeList` item (operation|class)

          // We want only field names that have the desired `typeList` `item`
          // Start with the list of changed fieldNames
          fieldList.all.changed.unique
            // Filter that by: Getting its list of operations
            .filter( field => listAll.operations( stats, field ).some( filterByTypeListType ) )
            // Then, for each of those fields, make the operation/class count per field object.
            .forEach( field => {
              let fieldTotal = listAll.operations( stats, field ).filter( filterByTypeListType ).length;
              opO[ field ] = {
                total: fieldTotal,
                ratio: toPercent( fieldTotal / total )
              };
            });
          return opO;
          function filterByTypeListType( operation ) {
            return type === 'classes' ? (opToClass( operation ) == item) : (operation == item);
          }
        })( item, total );
        return opObj;
      })( typeListName, typeListCounts[ typeListName ] );
    });
    return typeListCounts;
  }
}

module.exports=operationsMain;