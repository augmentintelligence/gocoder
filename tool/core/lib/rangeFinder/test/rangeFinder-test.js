const rf = require('../rangeFinder');



let rng1 = ['123A', '123B'];

rf.isSeries('123a', '123b');//?
rf.isSeries('123b', '123a');//?

rf.isSeries('123a', '124a');//?
rf.isSeries('124a', '123a');//?


rf.isSeries('123a', '123c');   //?
rf.isSeries('123a', '125a');   //?
rf.isSeries('123a', '124c');   //?

rf.isSeries('123av', '124xx'); //?
rf.isSeries('123av', '124xx'); //?


rf.isRange('123a', '123b');//?
rf.isRange('123b', '123a');//?

rf.isRange('123a', '124a');//?
rf.isRange('124a', '123a');//?

rf.isRange('123a', '123c');   //?
rf.isRange('123a', '125a');   //?
rf.isRange('123a', '124c');   //?

rf.isRange('123av', '124xx'); //?
rf.isRange('123av', '124xx'); //?

rf.isRange('1234', '124');   //?
rf.isRange('1234x', '124x'); //?
rf.isRange('1234x', '124');  //?

rf.isSeries('9a', '10a');//?
rf.isSeries('a', 'b');//?