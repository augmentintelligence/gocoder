(function(){

  const numRx = new RegExp(/[\d]+/),
        abcRx = new RegExp(/[a-z]+/i),
        othRx = new RegExp(/[^\da-z]+/i);
  const alpha = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];

  const isAll = function (rx, str) {
    // If the string is not entirely numbers/strings
    if ( othRx.test( str ) )
      return false;
    // If testing for all numbers...
    if ( rx === numRx )
      return numRx.test( str ) && !abcRx.test( str );
    // If testing for all letters...
    if ( rx === abcRx )
      return !numRx.test( str ) && abcRx.test( str );
    // Otherwise, null (exception)
    return null;
  };

  exports.charRange = characterRange;
  function characterRange( x, y ) {
    if ( x === undefined || y === undefined )
      return null;
    if ( isAll( numRx, x ) && isAll( numRx, y ) ) {
      return Math.abs( x - y );
    } else if ( isAll( abcRx, x ) && isAll( abcRx, y ) ) {
      if ( x.length === 1 && y.length === 1 )
        return Math.abs( alpha.indexOf( x ) - alpha.indexOf( y ) );
      else console.error(`[rng(${ x }, ${ y })] Cannot call on more than one character per argument!`);
    }
  }

  const stripNum = str => numRx.test(str) ? str.replace(numRx, '') : str,
       stripChar = str => !Number.isNaN(Number.parseInt(str)) ? String(Number.parseInt(str)) : str;

  const isXX = ( cType, rType, x, y ) => ( x, y ) => {
    let r = characterRange( ( cType === 'num' ? stripChar( x ) : stripNum( x ) ), ( cType === 'num' ? stripChar( y ) : stripNum( y ) ) );
    return r === null ? null : rType === 'series' ? r === 1 : rType === 'range' ? r > 1 : rType === 'same' ? r === 0 : undefined;
  };

  const isNumSeries = (x, y) => isXX('num', 'series')(x, y),
         isNumRange = (x, y) => isXX('num', 'range' )(x, y),
          isNumSame = (x, y) => isXX('num', 'same'  )(x, y);

const isAbcSeries = (x, y) => isXX('abc', 'series')(x, y),
       isAbcRange = (x, y) => isXX('abc', 'range' )(x, y),
        isAbcSame = (x, y) => isXX('abc', 'same'  )(x, y);

  const isSeriesAlpha = ( a1, a2 ) => {
    a1 = stripNum(a1);
    a2 = stripNum(a2);
    if (a1.length === a2.length) {
      switch (a1.length) {
        case 0:
          return undefined;
        case 1:
          return isAbcSeries(a1,a2);
        default:
          let code = [];
          for (let i = 0, n = a1.length; i < n; i++) {
            code.push( ( isAbcSame(a1[i], a2[i]) ? String(0) : (isAbcSeries(a1[i], a2[i]) ? String(1) : (isAbcRange(a1[i], a2[i]) ? String(2) : String(3)))) );
          } return !code.includes(3) && !code.includes(2) && (
                     ( code[code.length - 1] === 1 && code.slice(0, code.length - 1).every(v => v == 0)) ||
                     ( code[0] === 1 && code.slice(1, code.length).every(v => v == 0) )
                   );

      }
    } else return null;
  };

  exports.isSeries = ( x, y ) => {
    let numSeries = isNumSeries( x, y ),
        abcSeries = isSeriesAlpha( x, y );
    if ( othRx.test( x ) || othRx.test( y ) || x === y || numSeries === null || abcSeries === null )
      return null;
    if ( isAll( numRx, x ) && isAll( numRx, y ) ) {
      return numSeries;
    } else if ( isAll( abcRx, x ) && isAll( abcRx, y ) ) {
      return abcSeries;
    } else if ( abcSeries ) {
      return isNumSame( x, y );
    } else if ( numSeries ) {
      return isAbcSame( x, y );
    } else return false;
  };

  exports.isRange = ( x, y ) => {
    let s = this.isSeries( x, y ),
       nr = isNumRange( x, y );
    if ( s === null ) {
      return nr === false ? null : stripNum( x ).length === stripNum( y ).length || null;
    } else return !s;
  };

}).call(this);