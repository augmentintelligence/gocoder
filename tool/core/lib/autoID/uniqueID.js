const arr = require( '@3lessthan/arraytools' );

/**
 * Given a JSON representing a dataset, and a field name in the form of a string
 * representing the ID field; scans the JSON to ensure that the values in the ID
 * field are unique throughout. Returns True/False.
 * @param {Object[]} json -
 *   A JSON object representing rows of data within a dataset.
 * @param {string} idField -
 *   The name of the property/field containing a supposed unique ID.
 */
const isUniqueID = function( json, idField ) {
  return json.length === arr.unique( json.map( row => row[ idField ] ) ).length &&
         json.every( row => row[ idField ] !== '' );
};
module.exports = isUniqueID;