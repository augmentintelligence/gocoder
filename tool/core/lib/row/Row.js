(function () {

  const History = require('../history/History'),
  HistoryShadow = require('../history/HistoryShadow'),
        Summary = require('../summary/Summary'),
           Stat = require('../stat/Stat');

  module.exports = class Row {

    /**
     * **Class**: ***Row***
     *
     * Provides a class object construction that
     * accepts an Object and provides convenience
     * methods to normalize the modification of its
     * content irrespective of the original field
     * names using `History` wrappers on each
     * property with usage of `HistoryShadow`s for
     * further sub-components.
     *
     * Optionally, provide a `name` for this row for
     * identification in Summary report outputs.
     *
     * @param {object} object -
     *   An object representing a row of data from a
     *   table.
     *
     * @param {string} [name] -
     *   A string to represent this row for summary
     *   report outputs.
     */
    constructor( options, object, strict, name ) {
      this.options = options;
      if ( name === undefined && typeof strict === 'string' ) {
        name = strict;
        strict = true;
      }
      for ( let key of Object.keys( object ) ) {
        this[ key ] = new History( key, object[ key ], strict );
      }

      // Name is defined, PrimaryKey_Name, PrimaryKey or undefined
      this.shadow = {
        _name: name ||
          [
            ( object.hasOwnProperty( options.fields.PrimaryKey ) && object[ options.fields.PrimaryKey ] !== '' ?
              object[ options.fields.PrimaryKey ] : ''
            ),
            ( object.hasOwnProperty(options.fields.Name) && object[options.fields.Name] !== '' ?
              object[ options.fields.Name ] : ''
            )
          ].filter( v => v !== '' ).join( '_' )
      };
    }

    /**
     * Sets the name of this `Row` to identify it in
     * summary reports. Places it in the shadow
     * property so that it does not get included in
     * row data outputs.
     *
     * @param {string} name -
     *   A string to represent this row for summary
     *   report outputs.
     */
    set _name( name ) {
      this.shadow._name = name;
    }

    /**
     * Returns a string of this row's name descriptor
     * that was either manually defined, defaulted to
     * `[PRIMARY_KEY]_[ENGLISH_NAME]` or `undefined`.
     *
     * @readonly
     * @returns {string} -
     *   A string to represent this row for summary
     *   report outputs.
     */
    get _name() {
      return this.shadow._name;
    }

    /**
     * Returns an array of this `Row`'s non-shadow
     * property keys.
     *
     * @readonly
     * @returns {string[]} -
     *  An array of row's non-shadow property
     *  (field/column) names/keys.
     */
    get keys() {
      return Object.keys( this )
                   .filter( key => key !== 'shadow' && key !== 'options' );
    }

    /**
     * Returns an array of this `Row`'s shadow
     * property keys.
     *
     * @readonly
     * @returns {string[]} -
     *  An array of row's shadow property
     *  (field/column) names/keys.
     */
    get shadows() {
      return Object.keys( this.shadow )
                   .filter( v => v !== '_name'  );
    }

    /**
     * Returns a string of all non-shadow properties'
     * highest change `type` value.
     *
     * @readonly
     * @returns {string}
     */
    get colString() {
      return this.keys
                 .map( key => String( this[ key ].highestChange ) ).join( '' );
    }

    /**
     * Returns an object of the original's
     * format with the most recent updated values.
     *
     * @readonly
     * @returns {Object.<string,string>}
     */
    get data() {
      let data = {};
      for ( let key of this.keys ) {
        data[ key ] = this[ key ].value;
      }
      data.CHANGES = this.colString;
      return data;
    }

    /**
     * Returns the original object.
     *
     * @readonly
     * @returns {Object.<string,string>}
     */
    get original() {
      let data = {};
      for ( let key of this.keys ) {
        data[ key ] = this.nth( key, 0 ).value;
      }
      return data;
    }

    /**
     * Returns `true` if any of the non-shadow
     * elements have a history length greater than 1,
     * and if the initial & final values are **different**,
     * even if there are mutations between.
     *
     * @readonly
     * @returns {boolean}
     */
    get hasChange() {
      return this.keys
                 .some( key => this[ key ].hasChange );
    }

    /**
     * Returns a 'Summary.toString()' of all
     * contained `History` objects.
     *
     * @readonly
     * @returns {string}
     */
    get summary() {
      if ( !this.hasChange ) return;
      let report = {};
      this.keys.forEach( key => {
        if ( this[ key ].hasChange )
          report[ key ] = this[ key ].changeReport;
        // Add Shadow History
        if ( report.hasOwnProperty( key ) ) {
          for ( let item of report[ key ] ) {
            if ( item.hasOwnProperty( 'shadow' ) && this.isShadow( item.shadow ) )
              item.shadow = this.shadow[ item.shadow ].changeReport;
          }
        }
      });
      return new Summary( this._name, report );
    }

    /**
     * Returns a 'Summary.toString()' of all
     * contained `History` objects.
     *
     * @readonly
     * @returns {string}
     */
    get log( ) {
      let log = [];
      if ( !this.hasChange ) return log;
      this.keys.forEach( key => {
        if ( !this[ key ].hasChange ) return;
        for ( let h of this[ key ].history ) {
          if ( h.action === 'constructor' && h.value === '' ) continue;
          log.push( {
            Record: this._name,
              Field: key,
            Shadow: false,
              Value: h.value,
            Action: h.action,
              Type: h.type,
              Note: h.note,
              From: h.from,
                To: h.to
          });
          if (
            h.to !== h.from &&
            !this.keys.includes( h.to ) &&
            this.shadows.includes( h.to ) &&
            ( ( this.shadow[ h.to ] instanceof HistoryShadow ) || ( this.shadow[ h.to ] instanceof History ) ) &&
            this.shadow[ h.to ].hasOwnProperty( 'history' )
          ) {
            for ( let s of this.shadow[ h.to ].history ) {
              if ( s.action === 'constructor' && s.value === '' ) continue;
              log.push( {
                Record: this._name,
                  Field: h.to,
                Shadow: true,
                  Value: s.value,
                Action: s.action,
                  Type: s.type,
                  Note: s.note,
                  From: s.from,
                    To: s.to
              } );
            }
          }
        }
      });
      return log;
    }

    get stats() {
      return new Stat( this );
    }

    /**
     * Given an existing property or shadow
     * element name within the Row, returns
     * its `n`th value. If `n` is negative,
     * exceeds the length, is undefined, or
     * NaN - returns the last element.
     *
     * @param {string} prop -
     *   The property name to retrieve a
     *   value from.
     *
     * @param {number} n -
     *   The index of the History item's
     *   value to retrieve.
     */
    nth( prop, n ) {
      return this.hasOwnProperty( prop ) ? this[ prop ].nth( n ) :
               this.isShadow( prop ) ? this.shadow[ prop ].nth( n ) :
                 errMsg( 'Row.nth','' )( `${prop} doesn't exist!` );
    }

    valueOf( prop ) {
    	if ( prop !== '' && this.nth( prop ) === undefined ) {
    		console.log( JSON.stringify(this.data) );
    		throw new Error( 'Cannot read property "value" of undefined' );
    	} else
      return prop !== '' ? this.nth( prop ).value : '';
    }

    /**
     * Returns `true` if a given property
     * exists as a shadow element, rather
     * than a returnable one.
     *
     * @param {string} prop -
     *   The property name to check its
     *   existence in the shadow elements.
     */
    isShadow( prop ) {
      return this.shadows.includes( prop );
    }

    getTarget( prop ) {
      return this.keys.includes( prop ) ? this[ prop ] : this.shadow[ prop ];
    }

    addChange( target, change, toValue, appendRule ) {
      if ( typeof toValue === 'function' && appendRule === undefined ) {
        appendRule = toValue;
           toValue = undefined;
      }
      if ( typeof target !== 'string' && change === undefined ) {
        change = target;
        target = undefined;
      }

      let eChange = errMsg( 'Row.addChange', 'Invalid Change' ),
       ftDefaults = {
        from: defaultChangeValue( 'from', target ),
          to: defaultChangeValue( 'to', target )
      };

      if ( typeof target === 'string' && ( typeof change === 'object' && !Array.isArray( change ) ) )
        setDefaults( ftDefaults, change );

      if ( Array.isArray( change ) && change.length > 1 ) {
        for ( let n = 0; n < 2; n++ ) {
          let c = change[ n ];
          defaultChangeValue( 'from', target )( c );
          let t = c.to === c.from ? c.from : n == 0 ? c.from : !c.hasOwnProperty( 'to' ) ? change[0].to : c.to;
          // defaultChangeValue('to', target)(c);
          this.addChange( t, change[ n ], n == 0 ? undefined : change[ 1 ].value, appendRule );
        }
      } else {
        if ( Array.isArray( change ) ) change = change[0];

        // No Move
        if ( change.from === change.to ) {
          let t = this.getTarget( change.from );
          defaultChangeValue( 'type', '2' )( change );
          if ( !History.validHistoryItem( change ) )
            return eChange( `${change}` );
          if ( t === undefined )
            return eChange( `'from' doesn't exist - ${ change.from }` );
          else {
            //!Caps Detection
            if ( this.options.caps && typeof change.value === 'string' ) {
              change.value = change.value.toUpperCase();
            }
            t.addChange( change );
          }

        } else {
          if ( !target )
            return eChange( `Missing target in move operation!` );
          let defaults = {
           action: defaultChangeFork( 'action', [ this.keys.includes( change.to ), `${change.from}Append`, `${change.to}Extraction` ] ),
             type: defaultChangeValue( 'type', '2' ),
             note: defaultChangeFork( 'note', [ this.keys.includes( change.to ), `Appended ${change.from}`, `Extracted ${change.to}` ] )
          }, shortCircuit = false;

          if ( target === change.from ) {

            setDefaults( defaults, change );

            if ( !change.hasOwnProperty( 'value' ) ) {
              change.value = '';
              if ( toValue === undefined ) {
                toValue = this.valueOf( target );
                shortCircuit = true;
              }
            }

            // Don't double the msg when appending to non-shadow properties
            if (!this.isShadow( change.from ) ) {
              //!Caps Detection
              if (this.options.caps) {
                change.value = change.value.toUpperCase();
              }
              this[ change.from ].addChange( change );
            }
            // Non-Array Version
            if ( toValue !== undefined || shortCircuit ) {
              change.value = toValue;
              this.addChange( change.to, change, undefined, appendRule );
            }

          // Target is 'to'
          } else {

            setDefaults( defaults, change );
            let t = this.getTarget( target );
            if ( t !== undefined ) {
              //!Caps Detection
              if ( this.options.caps ) {
                change.value = change.value.toUpperCase();
              }
              change.value = String( t.value ).trim() !== '' ? [ t.value.trim(), change.value ].join( appendRule === undefined ? ', ' : appendRule ) : change.value;
              t.addChange( change );

            } else {
              //!Caps Detection
              if ( this.options.caps ) {
                change.value = change.value.toUpperCase();
              }
              this.shadow[ target ] = new HistoryShadow( target, change.value, change.from, appendRule );
            }
          }
        }
      }
    }


    /**
     * Given an existing property name within the Row, and an existing shadow property name,
     * appends the latest shadow property value to the end of the root property value.
     * If it is not blank, prepends a ', ' and adds a change.
     * @param {string} prop -
     * The property name to append value to its content.
     * @param {string} shadow -
     * The shadow property name for which to append its latest value.
     */
    appendShadow( prop, shadow, join ) {
      if ( this.hasOwnProperty( prop ) && this.isShadow( shadow ) ) {
        let s = this.getTarget( shadow );
        // console.log(`firing appendShadow with: (${prop}, ${shadow}, ${join})`);
        this.addChange( shadow, [{
          action: 'ShadowAppend',
           value: '',
            type: '2',
            from: shadow,
              to: prop
        },{
            action: 'ShadowAppend',
             value: s.append( s.value ),
              type: s.highestChange,
              from: shadow,
                to: prop
        }], undefined, join );
      }
    }

    appendShadows( target, shadows, join ) {
      for ( let shadow of shadows ) {
        this.appendShadow( target, shadow, join );
      }
    }

  };

  function defaultTo( o, p, cb ) {
    return o.hasOwnProperty( p ) ? o[ p ] : cb();
  }

  function defaultValue( o, p, v ) {
    return defaultTo( o, p, () => v );
  }

  function defaultFork( o, p, test, ifTrue, ifFalse ) {
    return defaultTo( o, p, () => test ? ifTrue : ifFalse );
  }

  function defaultChangeValue( p, v, c ) {
    return c => c[ p ] = defaultValue( c, p, v );
  }
  function defaultChangeFork( p, v, c ) {
    return c => c[ p ] = defaultFork( c, p, v[0], v[1], v[2] );
  }

  function setDefaults( defaults, object ) {
    Object.keys( defaults ).forEach( d => {
      defaults[ d ]( object );
    });
  }

  function errMsg( funcName, type, msg ) {
    return msg => console.error( `[${funcName}] ${type !== '' ? type + ' ' : ''}Error: ${msg}` );
  }

}).call(this);