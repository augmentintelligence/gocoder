const uniqueID = require( '../../../core/lib/autoID/uniqueID' );
const keyShift = require( '../../../core/lib/util/keyShift' );
/**
 * Given a json-like array representing rows of data from a CSV, Global Options (`gOptions`) generated
 * from scanning the input CSV-as-JSON file, and optionally -
 * the preferred name for a primary key field, and a starting integer for the autoID:
 * Determines whether gOptions.PrimaryKey is unique, if it isn't, preserves the values in it by different
 * means depending on the supplied arguments and forces the PrimaryKey value to be unique.
 *
 * @param {Object[]} json -
 *  A JSON-like array of repeating objects representing rows in a table.
 * @param {gOptions} gOptions -
 *  A Global-Options object generated from the preceding `json` argument.
 * @param {string} [keyPref] -
 *  Optionally define the preferred ID field name, regardless of the current ID mapped in gOptions.
 * @param {number} [start] -
 *  The number to start from for the incrementing autoID.
 */
const autoID = function( json, gOptions, keyPref, start, verbose ) {

  // Get target ID field from Global Options
  let idField = gOptions.fields.PrimaryKey;

  // Coerce valid starting integer for autoID
  start = start === undefined || typeof start !== 'number' ? 1 : Math.floor( start );

  // 1. If target ID is unique...
  if ( uniqueID( json, idField ) ) {
    if ( verbose ) console.log( '[INFO] AutoID: Scanned key is unique...' );
    // ... Target ID matches preferred ID name or no ID is preferred..
    //    ~~> No action necessary
    if ( keyPref === undefined || typeof keyPref !== 'string' || idField === keyPref ) {
      if (verbose) {
        if ( keyPref === undefined || typeof keyPref !== 'string' )
          console.log( '[INFO] AutoID: No preferred key defined...' );
        else console.log( '[INFO] AutoID: Scanned key is preferred key...' );
        console.log( '[OK] AutoID: No IDs Generated' );
      } return;
    } else {
      // ... Target ID does not match preferred ID name ...
      if (verbose) console.log( '[INFO] AutoID: Scanned key is not preferred key...' );
      // ~~> Change Global Options PK Map
      gOptions.fields.PrimaryKey = keyPref;

      //  ... preferred ID name does not exist in object ...
      if ( !json[0].hasOwnProperty( keyPref ) ) {
        json.forEach( row => {
          row[ keyPref ] = row[ idField ];
          delete row[ idField ];
          keyShift.toFirst( row, keyPref );
        });
        if ( verbose ) {
          console.log( '[INFO] AutoID: Preferred key does not exist...' );
          console.log( '[INFO] AutoID: Renamed scanned key to preferred key...' );
          console.log( '[INFO] AutoID: Remapped PK to preferred key...' );
          console.log( '[OK] AutoID: No IDs generated' );
        }
      // ...Preferred ID exists in object ...
      //   ... and it is unique ...
      } else if ( uniqueID( json, keyPref ) ) {
        if ( verbose ) {
          console.log( '[INFO] AutoID: Preferred key exists...' );
          console.log( '[INFO] AutoID: Preferred key is unique...' );
          console.log( '[INFO] AutoID: Remapped PK to preferred key...' );
          console.log( '[OK] AutoID: No IDs generated' );
        } return;
      //   ... and it is not unique ...
      } else {
        json.forEach( row => {
          row[ "Original_" + keyPref ] = row[ keyPref ];
          row[ keyPref ] = start++;
          keyShift.toFirst( row, "Original_" + keyPref );
          keyShift.toFirst( row, keyPref );
        });
        if ( verbose ) {
          console.log( '[INFO] AutoID: Preferred key exists...' );
          console.log( '[INFO] AutoID: Preferred key is not unique...' );
          console.log( '[INFO] AutoID: Preserving preferred key under a different name...' );
          console.log( '[INFO] AutoID: Remapped PK to preferred key...' );
          console.log( '[OK] AutoID: Generated AutoIDs under preferred key' );
        }
      }
    }
  // 2. If target ID is NOT unique...
  } else {
    if (verbose) console.log( '[INFO] AutoID: Scanned key is not unique...' );
    // ... target ID === preferred ID name
    if ( idField === keyPref ) {
      json.forEach( row => {
        // Copy ID value to renamed field at the end
        row[ "Original_" + keyPref ] = row[ keyPref ];
        // Repopulate field with autoID
        row[ keyPref ] = start++;
        keyShift.toFirst( row, "Original_" + keyPref );
        keyShift.toFirst( row, keyPref );
      });
      if ( verbose ) {
        console.log( '[INFO] AutoID: Scanned key is preferred key...' );
        console.log( '[INFO] AutoID: Preserving preferred key under a different name...' );
        console.log( '[OK] AutoID: Generated AutoIDs under preferred key' );
      }

    // ... target ID !== preferred ID ...
    } else {
      // ~~> Change Global Options PK Map
      gOptions.fields.PrimaryKey = keyPref;

      if ( verbose ) {
        console.log( '[INFO] AutoID: Scanned key is not preferred key...' );
        console.log( '[INFO] AutoID: Remapped PK to preferred key...' );
      }


      // ... preferred ID does not exist ...
      if ( !json[0].hasOwnProperty( keyPref ) ) {
        json.forEach( row => {
          row[ keyPref ] = start++;
          keyShift.toFirst( row, keyPref );
        });
        if ( verbose ) {
          console.log( '[INFO] AutoID: Preferred key does not exist...' );
          console.log( '[OK] AutoID: Generated AutoIDs under preferred key' );
        }
      // ... preferred ID exists ...
      } else {
        if ( verbose ) console.log( '[INFO] AutoID: Preferred key exists...' );
        // ... it is unique ...
        if ( uniqueID( json, keyPref) ) {
          // ~~> Do nothing, options changed
          if ( verbose ) {
            console.log( '[INFO] AutoID: Preferred key is unique...' );
            console.log( '[OK] AutoID: No IDs generated' );
          }
        }
        // ... it is not unique ...
        else {
          json.forEach( row => {
            // Copy ID value to renamed field at the end
            row[ "Original_" + keyPref ] = row[ keyPref ];
            // Repopulate field with autoID
            row[ keyPref ] = start++;
            keyShift.toFirst( row, "Original_" + keyPref );
            keyShift.toFirst( row, keyPref );
          });
          if ( verbose ) {
            console.log( '[INFO] AutoID: Preferred key is not unique...' );
            console.log( '[INFO] AutoID: Preserved preferred key under a different name...' );
            console.log( '[OK] AutoID: Generated AutoIDs under preferred key' );
          }
        }
      }
    }
  }
  return true;
};
module.exports = autoID;