const makeFieldSet = require('../../../../core/lib/bin/options/lib/makeFieldSet'),
targetFieldsFilter = require('../../../../core/lib/bin/options/lib/targetFieldsFilter'),
           XRegExp = require('xregexp');

const GlobalOptions = function ( json, options ) {

    this.caps = typeof options.caps === 'boolean' ? options.caps :
      !( json.some (
        row => Object.keys( row ).some(
          col => /[a-z]+/.test( row[ col ] )
      )
    ));

    this.fields = makeFieldSet( json[0], [ options.lat, options.lon ]  );
    this.targetFields = targetFieldsFilter( this.fields );
    this.validate = options.validate;
    this.clean = options.clean;
    this.postalCodeLimit = options.hasOwnProperty('postalLimit') && options.postalLimit === 'ON' ? /[KLMNP]/i :
          options.hasOwnProperty('postalLimit') ? XRegExp.build( options.postalLimit ) : undefined;

    // console.log(this);
};

module.exports = GlobalOptions;