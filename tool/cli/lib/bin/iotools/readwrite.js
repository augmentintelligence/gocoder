(function() {
  const path = require('path'),
          fs = require('fs'),
           _ = require('underscore'),
     Promise = require('promise'),
    readFile = Promise.denodeify(require('fs').readFile);

  let options = {
    verbose: false,
    importDir: '../../',
    exportDir: '../../'
  };

 /**
  * Sets options for the module. Available parameters are:
  * `verbose`:   If `true`, logs actions to console. [Default: `false`]
  * `importDir`: A string defining which directory `importData` uses as its root. [Default: `'../../src/'`]
  * `exportDir`: A string defining which directory `exportData` uses as its root. [Default: `'../../dist/'`]
  *
  * @param {object} opts The object-literal containing one or more module options to override.
  */
  exports.setOptions = function(opts) {
    return _.extend(options, opts);
  };

  exports.importJSON = function (directory, filename, callback) {
    return readFile(path.join(directory, filename), 'utf8').then(JSON.parse).nodeify(callback);
  };

  /**
   * Reads a file synchronously, returns the file in utf8 format.
   *
   * @param   {string} directory Starting in src, the path to the file to be read
   * @param   {string} filename  The filename to read
   * @returns {string}           The file contents in utf8 format
   */
  exports.importData = function(directory, filename) {
    let data = fs.readFileSync(path.join(directory, filename), 'utf8');
    if (options.verbose) console.log(directory, filename, 'was successfully read!');
    return data;
  };

  /**
   * Writes a file asynchronously. Provides an optional callback with the filename written.
   *
   * @param {string}   content      The content to write to file
   * @param {string}   directory    Starting in 'dist', the path to the file to be written
   * @param {string}   filename     The filename to write
   * @param {Function} [callback]   (Optional) callback to run when function completes
   */
  exports.exportData = function(content, directory, filename) {
    fs.writeFileSync( path.join( directory, filename ), '\ufeff'+content, 'utf8' );
    if (options.verbose) console.log( directory, filename, ' was successfully written!');
  };

}).call(this);