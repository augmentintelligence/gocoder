module.exports = function ( type, answers, options ) {
  // Imports - 3rd Party
  const async = require('async');
  // Imports - Application
  const scan = require('../../../src/scan'),
       clean = require('../../../src/clean'),
    validate = require('../../../src/validate'),
     compile = require('../../../src/compile'),
    readjson = require('../../../src/readjson'),
   csvtojson = require('../../../src/csvtojson'),
   jsontocsv = require('../../../src/jsontocsv'),
  writeFiles = require('../../../src/writefiles');

  let waterfallChain = [ scan, clean, validate, compile, jsontocsv ];
  waterfallChain.unshift( type == 'csv' ? csvtojson( answers.srcPath, options ) : readjson( answers, options ) );
  async.waterfall( waterfallChain, writeFiles( answers ) );
};