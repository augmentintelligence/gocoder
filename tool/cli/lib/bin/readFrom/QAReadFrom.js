const QAList = require('../qaList/QAList');

const QA = function ( cwd, type ) {
  return new QAList(
    // Answers
    {
      targetPath: '',
         srcPath: '',
             cwd: cwd
    },
    // Questions
    [
      {
        name: 'srcPath',
        init: 'Path to ' + type.toUpperCase() + ': ',
         err: [
          '[Error: Source path cannot be empty]',
          '[Error: Source file is not a .' + type + ']',
          '[Error: Source file does not exist]'
        ],
        next: [ 'targetDir' ]
      },
      {
        name: 'targetPath',
        init: 'Path to Output Directory: ',
         err: [ '[Error: Target path does not exist or is not writeable]' ]
      }
    ]
  );
};

module.exports = QA;