const fs = require('fs');

const endsWithNot = {
   csv: new RegExp(/\.(?:js(?:on)?|txt|html|png|jpe?g|numbers|min)$/),
  json: new RegExp(/\.(?:csv|js|txt|html|png|jpe?g|numbers|min)$/)
},
      endsWith = {
  json: new RegExp(/\.json$/),
   csv: new RegExp(/\.csv$/)
};


module.exports = function ( type ) {
  this.type = type;
  this.rx = {
    relativeBackFilePath: new RegExp(/^[\.]{2}\//),
    relativeFilePath: new RegExp(/^[\.]\//),
    filename: new RegExp(/^[^\/\.\~]/),
    endsWithNot: endsWithNot[type],
    endsWith: endsWith[type]
  };

  // Returns: [ exitCode(0 is success), valid/clean answer, [nextQuestionIndex] (Default is 0) ]
  this.srcPath = function ( answer, QAList ) {

    let cwd = typeof QAList === 'string' ?
              QAList : QAList.answers.cwd;

    // Error: Source cannot be empty
    if ( answer === '' )
      return [1];
    // Error: Must be correct file type
    if ( this.rx.endsWithNot.test( answer ) )
      return [2];

    // If relative path, convert to absolute
    if ( this.rx.relativeFilePath.test( answer ) )
      answer = cwd + answer.substr(2);
    else if ( this.rx.filename.test( answer ) )
      answer = cwd + answer;
    else if ( this.rx.relativeBackFilePath.test( answer ) )
      answer = this.getRelativeFrom( cwd, answer );

    // If no filetype is provided, append '.[type]'
    if ( !this.rx.endsWith.test( answer ) )
      answer += '.' + this.type;

    // Success: If file exists
    try {
      // Check readable
      fs.accessSync( answer, fs.constants.R_OK );
      // Return no error + built answer
      return [ null, answer ];
    }
    // Error: File does not exist
    catch (err) {
      return [3];
    }

  };

  this.targetPath = function ( answer, QAList ) {

    let cwd = typeof QAList == 'string' ?
              QAList : QAList.answers.cwd;

    // If blank, use current directory
    if ( answer === '' )
      answer = cwd;

    // If relative path, convert to absolute
    else if ( this.rx.relativeFilePath.test( answer ) )
      answer = cwd + answer.substr(2);
    else if ( this.rx.relativeBackFilePath.test( answer ))
      answer = this.getRelativeFrom( cwd, answer );

    // If path does not end in a '/', add one.
    if ( answer[ answer.length - 1 ] !== '/')
      answer += '/';

    try {
      // Check writeable
      fs.accessSync( answer, fs.constants.W_OK );
      return [ null, answer ];
    }
    // Error: Directory doesn't exist/not writeable
    catch (err) {
      return [1, answer];
    }

  };

  this.getRelativeFrom = function ( cwd, path ) {
    let newWD = cwd + '/';
    newWD = newWD.split('/');
    while ( this.rx.relativeBackFilePath.test( path ) ) {
      path = path.replace(/[\.]{2}[\/]/, '');
      newWD = newWD.slice(0, newWD.length - 2);
    }
    return newWD.join('/') + '/' + path;
  };

};