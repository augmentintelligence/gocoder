(function () {

  const { spawn } = require( 'child_process' );

  /**
   * Gets the current working directory using the
   * platform-appropriate method and passes it to
   * the callback as a string, with err as the
   * first argument.
   *
   * @param {function} callback -
   *   The callback to pass the results to
   *
   */
   const pwd = function ( callback ) {

    let isWin, getWD;

    // Check for Windows
    isWin = process.platform === "win32";

    // Spawn the appropriate process
    getWD = isWin ?
      spawn( 'cmd.exe', [ '/c', 'cd' ] ) :
      spawn( 'pwd' );

    // Monitor Successful Output
    getWD.stdout.on( 'data', data => {
      callback( null, `${ data }` );
    });

    // Monitor Error Output
    getWD.stderr.on( 'data', data => {
      callback( true, `${ data }` );
    });

  };

  module.exports = pwd;

}).call(this);