class QAList {

  constructor( answers, questions ) {
    this.done = false;
    // Answers
    this.answers = {};
    if ( Array.isArray( answers ) ) {
      for ( let a of answers ) {
        this.answers[ a ] = '';
      }
    } else if ( typeof answers == 'object' ) {
      let keys = Object.keys( answers );
      for ( let key of keys ) {
        this.answers[ key ] = answers[ key ];
      }
    }

    // Questions
    if ( !Array.isArray( questions ) ) {
      if ( Array.isArray( answers ) ) {
        for ( let a of answers ) {
          this.questions.push({
            name: a,
            init: '',
            next: []
          });
        }
      } else if ( typeof answers == 'object' ) {
        let keys = Object.keys( answers );
        for ( let key of keys ) {
          this.questions.push({
            name: key,
            init: '',
            next: []
          });
        }
      }
    } else this.questions = questions;
  }

  get questionList() {
    return this.questions.map( v => v.name );
  }

  q ( questionName ) {
    return this.questions.filter( v => v.name == questionName )[0];
  }

  answer( question, answer ) {
    if ( this.answers.hasOwnProperty( question ) )
      this.answers[ question ] = answer;
  }

  next( currentQ, nextIndex ) {
    if ( !this.q( currentQ ).hasOwnProperty( 'next' ) ) {
      this.done = true;
      return null;
    }
    else return this.q( currentQ ).next[ nextIndex ];
  }

}

module.exports = QAList;