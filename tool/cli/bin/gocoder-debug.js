const readline = require('readline'),
        events = require('events'),
       emitter = new events.EventEmitter(),
            rl = readline.createInterface({
              input:  process.stdin,
              output: process.stdout,
              terminal: true,
              crlfDelay: Infinity,
              removeHistoryDuplicates: true
            }),
    filenames = require('../../core/lib/bin/util/filenames'),
          cwd = require('../lib/bin/fs/pwd'),
     readFrom = require('../lib/bin/readFrom/readFrom'),
   ReadFromQA = require('../lib/bin/readFrom/QAReadFrom'),
   ReadFromValidators = require('../lib/bin/readFrom/ValidatorsReadFrom');

let command, qa, validator, input, output, options = {};

command = 'json';
output  = './test/output/';
input   = './test/data/test-set.json';
options.validate    = true;
options.clean       = true;
options.caps        = false;
options.lat         = 'GEO_LAT';
options.lon         = 'GEO_LON';
options.postalLimit = 'ON';

cwd( ( err, res ) => {
  if ( err ) throw err;
  emitter.emit( 'cwdGet', res );
});

emitter.on( 'cwdGet', res => {
  let cwd = res.trim() + ( process.platform === "win32" ? '\\' : '/');
  qa = new ReadFromQA( cwd, command );
  validator = new ReadFromValidators( command );

  if ( !input ) {

    currentQ = qa.questionList[0];
    emitter.emit( 'questionInit' );

  } else {

    let srcPath = validator.srcPath( input, qa ),
    targetPath = validator.targetPath( ( output ? output : '' ), qa );

    if ( srcPath[0] == null && targetPath[0] == null ) {
      qa.answer( 'srcPath', srcPath[1] );
      qa.answer( 'targetPath', targetPath[1] );
      qa.done = true;
   }

    else if ( srcPath[0] != null )
      console.log( qa.q('srcPath').err[srcPath[0] - 1] );
    else
      console.log( qa.q('targetPath').err[targetPath[0] - 1] + ': ' + targetPath[1]);
    rl.close();
  }
});

// Process
rl.on( 'close', () => {
  if ( qa && qa.done ) {
    let answers = filenames( command, qa.answers );
    readFrom( command, answers, options );
  }
  else console.log( ( qa ? '\n' : '' ) + '[INFO] Execution cancelled, exiting.' );
});