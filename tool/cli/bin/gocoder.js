const readline = require('readline'),
       program = require('commander'),
        events = require('events'),
       emitter = new events.EventEmitter(),
            rl = readline.createInterface({
              input:  process.stdin,
              output: process.stdout,
              terminal: true,
              crlfDelay: Infinity,
              removeHistoryDuplicates: true
            }),
    filenames = require('../../core/lib/bin/util/filenames'),
          cwd = require('../lib/bin/fs/pwd'),
     readFrom = require('../lib/bin/readFrom/readFrom'),
   ReadFromQA = require('../lib/bin/readFrom/QAReadFrom'),
   ReadFromValidators = require('../lib/bin/readFrom/ValidatorsReadFrom');

let command, qa, validator, currentQ, input, output, options = {
     clean: true,
  validate: true,
      caps: null,
       lat: 'GEO_LAT',
       lon: 'GEO_LON'
};

// function parseInput() {}
// function parseOutput() {}

// // Main
// program
//   .version( '0.1.0', '-v, --version' )
//   .usage( '[options] <file...> | [cmd]' )
//   .option( '-i, --input <file>', 'An input file', parseInput )
//   .option( '-o, --output <dir>', 'An output directory', parseOutput )
//   .parse( process.argv );
//   .command( 'from [file]', 'Clean either a csv or a json file' )
//   .command( 'fromCSV [csvFile]', 'Clean a csv file. Leave argument blank to specify output.')

program
  .version( '0.1.0', '-V, --version' )
  .command( 'fromCSV [file]' )
  .description( 'use a csv as the source file' )
  .usage( '[options] <file>' )
  .option( '-c, --clean',        'Clean Only' )
  .option( '-v, --validate',     'Validate Only' )
  .option( '-u, --uppercase',    'Override Case-Detection to UpperCase' )
  .option( '-U, --no-uppercase', 'Override Case-Detection to TitleCase')
  .option( '-t, --lat <lat>',    'Define Output Latitude Field' )
  .option( '-n, --lon <lon>',    'Define Output Longitude Field' )
  .option( '-o, --output <dir>', 'Define output directory' )
  .option( '-p, --pLimit <pLimit>', 'Limit First Character of Postal Code to Character-set')
  .parse( process.argv )
  .action( function ( file, cmd ) {

    if ( cmd.clean ) options.validate = false;
    if ( cmd.validate ) options.clean = false;
    if ( typeof cmd.uppercase === 'boolean' ) options.caps = cmd.uppercase;
    if ( cmd.lat ) options.lat = cmd.lat;
    if ( cmd.lon ) options.lon = cmd.lon;
    if ( cmd.pLimit ) options.postalLimit = cmd.pLimit;

    command = 'csv';
     output = cmd.output ? cmd.output.trim() : options.output;
      input = file ? file.trim() : file;

    cwd( function ( err, res ) {
      if ( err ) throw err;
      emitter.emit( 'cwdGet', res );
    });

  });

program
  .version( '0.1.0', '-V, --version' )
  .command( 'fromJSON [file]' )
  .description( 'use a json type as the source file' )
  .usage( '[options] <file>' )
  .option( '-c, --clean',        'Clean Only' )
  .option( '-v, --validate',     'Validate Only' )
  .option( '-u, --uppercase',    'Override Case-Detection to UpperCase' )
  .option( '-U, --no-uppercase', 'Override Case-Detection to TitleCase')
  .option( '-t, --lat <lat>',    'Define Output Latitude Field' )
  .option( '-n, --lon <lon>',    'Define Output Longitude Field' )
  .option( '-o, --output <dir>', 'Define output directory' )
  .option( '-p, --pLimit <pLimit>', 'Limit First Character of Postal Code to Character-set')
  .parse( process.argv )
  .action( function ( file, cmd ) {

    if ( cmd.clean ) options.validate = false;
    if ( cmd.validate ) options.clean = false;
    if ( typeof cmd.uppercase === 'boolean' ) options.caps = cmd.uppercase;
    if ( cmd.lat ) options.lat = cmd.lat;
    if ( cmd.lon ) options.lon = cmd.lon;
    if ( cmd.pLimit ) options.postalLimit = cmd.pLimit;

    command = 'json';
     output = cmd.output ? cmd.output.trim() : options.output;
      input = file ? file.trim() : file;

    cwd( function ( err, res ) {
      if ( err ) throw err;
      emitter.emit( 'cwdGet', res );
    });

  });

program.parse( process.argv );

emitter.on( 'cwdGet', res => {
  let cwd = res.trim() + ( process.platform === "win32" ? '\\' : '/' );
  qa = new ReadFromQA( cwd, command );
  validator = new ReadFromValidators( command );

  if ( !input ) {

    currentQ = qa.questionList[0];
    emitter.emit( 'questionInit' );

  } else {

    let srcPath = validator.srcPath( input, qa ),
     targetPath = validator.targetPath( ( output ? output : '' ), qa );

    if ( srcPath[0] == null && targetPath[0] == null ) {
      qa.answer( 'srcPath', srcPath[1] );
      qa.answer( 'targetPath', targetPath[1] );
      qa.done = true;
    }
    else if ( srcPath[0] != null )
      console.log( qa.q('srcPath').err[srcPath[0] - 1] );
    else
      console.log( qa.q('targetPath').err[targetPath[0] - 1] + ': ' + targetPath[1]);
    rl.close();
  }
});



emitter.on( 'questionInit', () => {
  ask( null, qa.q( currentQ ).init );
});

// Valid Answer: Save Response, Update CurrentQ
emitter.on( 'validAnswer', ( answer, nextIndex ) => {
  qa.answer( currentQ, answer );
  currentQ = qa.next( currentQ, nextIndex );
  if ( currentQ )
    ask( null, qa.q( currentQ ).init );
  else
    rl.close();
  });

// Invalid Answer: Produce Error Message + Repeat
emitter.on( 'invalidAnswer', err => {
  ask( qa.q( currentQ ).err[ err - 1 ] + '\n', qa.q( currentQ ).init );
});

// New Input Line: Validate Answer & Call (in)validAnswer
rl.on( 'line', answer => {
  let res = validator.hasOwnProperty( currentQ ) ?
            validator[ currentQ ]( answer.trim(), qa ) : [ 0, answer.trim() ];
  if ( !res[0] )
    emitter.emit( 'validAnswer',   res[1], res[2] === undefined ? 0 : res[2] );
  else
    emitter.emit( 'invalidAnswer', res[0] );
});

// Process
rl.on( 'close', () => {
  if ( qa && qa.done ) {
    let answers = filenames( command, qa.answers );
    readFrom( command, answers, options );
  }
  else {
    /* console.log ( options );
    console.log( qa.answers ); */
    console.log( ( qa ? '\n' : '' ) + '[INFO] Execution cancelled, exiting.' );
  }
});

// Asks Question & Provides Error Message (If Applicable)
function ask( err, question ) {
  rl.setPrompt( ( !err ? '' : err ) + question );
  rl.prompt();
}