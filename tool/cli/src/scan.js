const GlobalOptions = require('../lib/bin/options/globalOptions');
const autoID = require( '../lib/autoID/autoID' );
const sanitize = require( '../lib/sanitizeFields/sanitizeFields' );

const scan = function ( json, options, callback ) {

  // Create GlobalOptions
  let gOptions = new GlobalOptions( json, options );

  // Sanitize Fields in Data
  json.forEach( row => { sanitize.fields( row ); });

  // Sanitize Fields in GlobalOptions
  Object.keys( gOptions.fields ).forEach( field => {
    gOptions.fields[ field ] = sanitize.field( gOptions.fields[field] );
  });
  gOptions.targetFields = gOptions.targetFields.map( field => sanitize.field( field ) );

  // Ensure field names are legal
  if ( !sanitize.fieldsLegal( json[ 0 ] ) ) throw new Error('Error: Fields contain reserved words.');
  else console.log( '[OK] SanitizeFields: Field names sanitized & valid' );

  // Run AutoID...
  autoID( json, gOptions, 'MOH_SERVICE_PROVIDER_IDENT', 9999999990, true );

  // DONE SCAN
  callback( null, json, gOptions );

};

module.exports = scan;