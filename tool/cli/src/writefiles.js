const readwrite = require('../lib/bin/iotools/readwrite');

module.exports = function ( answers, err, res ) {
  return function ( err, [ rows, csv, report, logs, stats ] ) {
    wrtieFiles([{
            data: csv,
             dir: answers.targetPath,
        filename: answers.cleanCSV
      },{
            data: JSON.stringify( rows ),
             dir: answers.targetPath,
        filename: answers.cleanJSON
      },{
            data: report,
             dir: answers.targetPath,
        filename: answers.cleanReport
      },{
            data: logs,
             dir: answers.targetPath,
        filename: answers.cleanLog
      },{
            data: JSON.stringify( stats ),
             dir: answers.targetPath,
        filename: answers.cleanStats
      }]);
  };
};

function wrtieFiles ( fileInfo ) {
  for (let i = 0, n = fileInfo.length; i < n; i++) {
    readwrite.exportData( fileInfo[i].data, fileInfo[i].dir, fileInfo[i].filename );
    console.log( ( i === 0 ? '\n' : '' ) + `[OK] WriteFile: ${fileInfo[i].filename}` + ( i === n -1 ? '\n' : '' ) );
  }
}