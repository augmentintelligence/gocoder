const csvtojson = require('csvtojson');
module.exports = function( path, options, callback ) {
  return function( callback ) {
    let rows = [], header = [];
    if ( process.platform === "win32" )
    csvtojson().fromFile( path )
      .then(function(rows){ callback(null,rows,options); });
    else
    csvtojson().fromFile( path )
      .on('header', headers => { header = headers; })
      .on('json',       obj => { rows.push( obj ); })
      .on('done',         e => { if (e) throw e; callback( null, rows, options ); });
  };
};