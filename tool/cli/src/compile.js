const statsProcessing = require( '../../core/src/stats' );
module.exports = function ( rows, callback ) {
  let ROWS = rows.map( row => row.data ),
    REPORT = rows.filter( row => row.hasChange )
                    .map( row => row.summary.toString ),
      LOGS = rows.reduce( ( acc, val ) => acc.concat( val.log ), [] ),
     STATS = statsProcessing( rows.map( row => row.stats ) );
  callback( null, [ ROWS, REPORT.join('\n'), LOGS, STATS ] );
};