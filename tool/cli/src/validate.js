const logCorrections = require( '../../core/lib/validator/logCorrections' );
const History = require('../../core/lib/history/History');
const ZERO_RESULTS = require('../../core/lib/validator/classes/ZERO_RESULTS');
const E500 = require('../../core/lib/validator/classes/E500');
const fill = require( '../../core/lib/validator/fill' );
const addressValidator = require('address-validator'),
               Address = addressValidator.Address,
                     _ = require('underscore'),
                 async = require('async');

addressValidator.setOptions({
    key: "AIzaSyAGcLS1l7m-1s8cVe10aAaGpmyT6rHCVJ0",
  // proxy: "http://204.40.130.129:3128"
});


// streetAddress, route, city, state, country, unknown
//!TODO: Make a version that tries with match.streetAddress, and if it doesn't produce a lat/lon, try again with match.unknown

module.exports = function ( rows, callback ) {

  if ( !rows[0].options.validate ) return callback( null, rows );
  else return async.eachOfSeries( rows, ( item, index, next ) => {

    // Add GEOLOC_LAT/LON Properties
    addGeocodeFields( item );

    // Make Address Object
    let address = makeAddress( item );

    // If no address...
    if ( address === false ) {
      fill.missing( item, 'StreetAddress' );
      fill.missing( item, 'PostalCode' );
      next();
    } else {
      // let matchType = getMatchType(item.valueOf('STREET_ADDRESS'));
      try {
        validate( rows, address, item, index, next );
      }
      catch ( e ) {
        fill.invalid( item, 'StreetAddress' );
        fill.invalid( item, 'PostalCode' );
        writeGeocode( item, item.options.fields.Lat, '' );
        writeGeocode( item, item.options.fields.Lon, '' );
        next();
      }
    }
  }, function() {
    callback( null, rows );
  });
};

function validate( rows, address, item, index, callback ) {
  addressValidator.validate (
    address,
    addressValidator.match.unknown,
    function( err, exact, inexact ) {
      // If Error is Timeout, retry
      if ( err && err instanceof E500 ) {
        validate( rows, address, item, index, callback );
      // Otherwise...
      } else {
        // If Error...
        if ( err ) {
          // Display Logs
          itemError( err, item );
          // If Zero Results...
          if ( err instanceof ZERO_RESULTS ) {
            fill.invalid( item, 'StreetAddress' );
            fill.invalid( item, 'PostalCode' );
            writeGeocode( item, item.options.fields.Lat, '' );
            writeGeocode( item, item.options.fields.Lon, '' );
            callback();
          }
          /* else throw ( err ); */
        // No Error
        } else {
          let match = _.map( exact, a => a.toString() ),
            noMatch = _.map( inexact, a => a.toString() ),
                  f = item.options.fields;

          // Exact Match
          if ( match != '' ) {

            // Logging
            console.log( `\n [${index+1}/${rows.length}] > ${item._name}: OK` );

            // Missing Postal Code + Available Postal Code
            if ( item.valueOf( f.PostalCode ) === '' && exact[0].postalCode ) {
              // Postal Code Limitation is Defined
              if ( item.options.postalCodeLimit ) {
                // Postal Code is Valid (in Ontario)
                if ( item.options.postalCodeLimit.test( exact[0].postalCode.charAt(0) ) ) {
                  fill.valid.PostalCode( item, exact[0].postalCode.replace(' ', '') );
                  writeGeocode( item, f.Lat, exact[0].location.lat );
                  writeGeocode( item, f.Lon, exact[0].location.lon );
                  callback();
                } else {
                  // Invalid Ontario Postal Code
                  fill.invalid( item, 'StreetAddress' );
                  fill.invalid( item, 'PostalCode' );
                  writeGeocode( item, f.Lat, '' );
                  writeGeocode( item, f.Lon, '' );
                  callback();
                }
              } else {
                // Postal Code Limitation not Defined
                fill.valid.PostalCode( item, exact[0].postalCode.replace( ' ', '' ) );
                writeGeocode( item, f.Lat, exact[0].location.lat );
                writeGeocode( item, f.Lon, exact[0].location.lon );
                callback();
              }
            } else {
              // Legacy Geocode
              writeGeocode( item, f.Lat, exact[0].location.lat );
              writeGeocode( item, f.Lon, exact[0].location.lon );
              callback();
            }

          // Inexact Match
          } else if ( noMatch != '' ) {

            let v_street_address = inexact[0].streetNumber ? inexact[0].streetNumber + ' ' : '';
            if ( item.options.caps ) {
              v_street_address += inexact[0].street ? inexact[0].street.toUpperCase() : '';
            } else {
              v_street_address += inexact[0].street ? inexact[0].street : '';
            }

            let v_community = inexact[0].city ?
              ( item.options.caps ? inexact[0].city.toUpperCase() : inexact[0].city ) : '';

            let corrections = [];

            // If Validation Street Address is not Empty & does not match input Street_Address...
            if ( v_street_address !== item.valueOf(f.StreetAddress) && v_street_address !== '' ) {

              // Missing Postal Code + Available Postal Code
              if ( item.valueOf( f.PostalCode ) === '' && inexact[0].postalCode ) {

                // Postal Code Limitation is Defined
                if ( item.options.postalCodeLimit ) {
                  // Valid Ontario Postal Code
                  if ( item.options.postalCodeLimit.test( inexact[0].postalCode.charAt(0) ) ) {

                    fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                    fill.valid.StreetAddress( item, v_street_address, corrections );

                    if ( v_community !== item.valueOf( f.City ) && v_community !== '' )
                      fill.valid.City( item, v_community, corrections );

                    if ( corrections.length )
                      logCorrections( item, rows.length, index, corrections );

                    writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                    writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );

                    callback();
                  } else {
                  // Invalid Ontario Postal Code
                    fill.invalid( item, 'PostalCode', corrections );
                    fill.invalid( item, 'StreetAddress', corrections );
                    if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                    writeGeocode( item, f.Lat, '' );
                    writeGeocode( item, f.Lon, '' );
                    callback();
                  }
                } else {
                // No Postal Code Limitation Defined
                  fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                  fill.valid.StreetAddress( item, v_street_address, corrections );
                  if (v_community !== item.valueOf(f.City) && v_community !== '' )
                    fill.valid.City( item, v_community, corrections );
                  if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                  writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                  writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                  callback();
                }
              } else {
                fill.valid.StreetAddress( item, v_street_address, corrections );
                if (v_community !== item.valueOf(f.City) && v_community !== '' )
                    fill.valid.City( item, v_community, corrections );
                if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                callback();
              }
            // JUST DO COMMUNITY
            } else {

              // Missing Postal Code + Available Postal Code
              if ( item.valueOf( f.PostalCode ) === '' && inexact[0].postalCode ) {
                // Postal Code Limitation is Defined
                if ( item.options.postalCodeLimit ) {
                  // Valid Ontario Postal Code
                  if ( item.options.postalCodeLimit.test( inexact[0].postalCode.charAt(0) ) ) {
                    fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                    if ( v_community !== item.valueOf( f.City ) && v_community !== '' )
                      fill.valid.City( item, v_community, corrections );
                    if ( corrections.length )
                      logCorrections( item, rows.length, index, corrections );
                    writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                    writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                    callback();
                  } else {
                  // Invalid Ontario Postal Code
                    fill.invalid( item, 'PostalCode', corrections );
                    fill.invalid( item, 'StreetAddress', corrections );
                    if ( corrections.length ) logCorrections( item, rows.length, index, corrections );
                    writeGeocode( item, f.Lat, '' );
                    writeGeocode( item, f.Lon, '' );
                    callback();
                  }
                } else {
                // Postal Code Limitation is not Defined
                  fill.valid.PostalCode( item, inexact[0].postalCode.replace( ' ', '' ), corrections );
                  if (v_community !== item.valueOf(f.City) && v_community !== '' )
                    fill.valid.City( item, v_community, corrections );
                  if ( corrections.length )
                  logCorrections( item, rows.length, index, corrections );

                  writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                  writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                  callback();
                }
              } else {
              // Has Postal code or No Postal Code Available
                if ( item.valueOf( f.PostalCode ) === '' )
                  fill.missing( item, 'PostalCode', corrections );

                if (v_community !== item.valueOf(f.City) && v_community !== '' )
                  fill.valid.City( item, v_community, corrections );

                if ( corrections.length )
                  logCorrections( item, rows.length, index, corrections );

                writeGeocode( item, f.Lat, inexact.filter( v => v.location.lat !== '' )[0].location.lat );
                writeGeocode( item, f.Lon, inexact.filter( v => v.location.lon !== '' )[0].location.lon );
                callback();
              }
            }
          }
        }
      }
    });
}

function makeAddress ( item ) {
  if (
    typeof item.valueOf( item.options.fields.StreetAddress ) !== 'string' ||
    item.valueOf( item.options.fields.StreetAddress ) === ''
  ) return false;
  return new Address({
    street: item.valueOf( item.options.fields.StreetAddress ),
      city: item.valueOf( item.options.fields.City ),
     state: "ON",
   country: "CA"
    });
}

function addGeocodeFields( item ) {
  item[ item.options.fields.Lat ] = new History( item.options.fields.Lat, '', true );
  item[ item.options.fields.Lon ] = new History( item.options.fields.Lon, '', true );
}

function writeGeocode( item, target, value ) {
  let change = {
    action: 'Geocoder',
      type: '1',
     value: value,
      note: 'Geocoded ' + ( target == item.options.fields.Lat ? 'Latitude' : 'Longitude' ),
      from: target,
        to: target
  };
  item.addChange( target, change );
}

function itemError (e, item) {
  console.log(e);
  console.log('Error: Primary Key - ' + item.valueOf(item.options.fields.PrimaryKey));
  console.log('Error: Name - ' + item.valueOf(item.options.fields.Name));
  console.log('Error: Address_Line_1 - ' + item.valueOf(item.options.fields.StreetAddress));
  console.log('Error: Community - ' + item.valueOf(item.options.fields.City));
  console.log('Error: Postal Code - ' + item.valueOf(item.options.fields.PostalCode));
}