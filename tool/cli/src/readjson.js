const readwrite = require('../lib/bin/iotools/readwrite');

module.exports = function ( answers, options, callback ) {
  return function ( callback ) {
    readwrite.importJSON(
      answers.fromDir,
      answers.filename,
      ( err, res ) => {
        if (err) throw err;
        callback( null, res, options );
      }
    );
  };
};