const json2csv = require('json2csv').parse;

module.exports = function( [ rows, report, logs, stats ], callback ) {

  const csvFields = Object.keys( rows[0] ),
          csvOpts = { csvFields };

  const csv = json2csv( rows, csvOpts );

  const logFields = Object.keys( logs[0] ),
          logOpts = { logFields };

  logs = json2csv(logs, logOpts);
  callback(null, [ rows, csv, report, logs, stats ]);
};