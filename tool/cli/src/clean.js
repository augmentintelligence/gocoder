const  Row = require( '../../core/lib/row/Row'),
  groupExe = require( '../../core/lib/regex/patternGroup'),
  exeChain = require( '../../core/lib/bin/exechain/exechain'),
coerceName = require( '../../core/lib/coerceName/coerceName' ),
   OTHER_1 = require( '../../core/lib/regex/patterns/other/_other1'),
   OTHER_2 = require( '../../core/lib/regex/patterns/other/_other2'),
     POSTC = require( '../../core/lib/regex/patterns/postalcode/_postalcode'),
       SST = require( '../../core/lib/regex/patterns/siteServiceType/_sst' );

const Mutator = require( '../../core/lib/regex/classes/Mutators' );

const dataClean = function ( json, opts, callback ) {

  // If Options dictate no cleaning is necessary, skip to just creating Row Objects
  if ( !opts.clean ) callback( null, json.map( row => new Row( opts, row ) ) );
  else { // Otherwise...

    let ROWS = [];

    // Initialize Caser
    Mutator.Caser = new Mutator.Casers( opts.caps );

    // Loop through rows
    for ( let row of json ) {

      row = new Row( opts, row );

      groupExe( POSTC, row, opts.fields.PostalCode );

      // Option to restrict Postal Codes
      if ( opts.postalCodeLimit && opts.postalCodeLimit.source === '[KLMNP]' ) {
        let localPostalFocal = new Mutator.Cleaner(
          'LocalPostal',
          'Removed out-of-bounds Postal Code',
          /^[^KLMNP].+/ig
        );
        localPostalFocal.exec( row, [ opts.fields.PostalCode ] );
      }

      for ( let g of exeChain ) {
        groupExe( g, row, [ opts.fields.StreetAddress, opts.fields.AddressLine2 ] );
      }

      groupExe( OTHER_1, row, opts.fields.StreetAddress );
      groupExe( OTHER_2, row, opts.fields.AddressLine2 );

      row.appendShadows( opts.fields.StreetAddress, [ 'Street' ] );

      if ( row.shadows.includes( 'RR' ) ) {
        if ( row.valueOf( opts.fields.StreetAddress ) == '')
          row.appendShadows( opts.fields.StreetAddress, [ 'RR' ]);
        else
          row.appendShadows( opts.fields.AddressLine2, [ 'RR' ]);
      }

      row.appendShadows( opts.fields.AddressLine2,
        [
          'Lot',
          'Concession',
          'Unit',
          'Suite',
          'Building',
          'Section',
          'Wing',
          'Level',
          'Floor',
          'Room',
          'Postal_Box',
          'Postal_Bag',
          'Code_Postal',
          'Reservation',
          'Island',
          'Other_1',
          'Other_2'
        ]);

      Mutator.Caser.exec( row, [ opts.fields.Name, opts.fields.City ]);
      coerceName( row, opts );

      new Mutator.Expander('Health',   /\bHLTH\b/i)
        .exec( row, opts.fields.Name );
      new Mutator.Expander('Pharmacy', /\bPhm\b|\bPharmcy\b/i)
        .exec( row, [ opts.fields.Name, opts.fields.AddressLine2 ] );
      new Mutator.Expander('Hospital', /\bHosp\b\.?|\bHosptl\b/i)
        .exec( row, [ opts.fields.Name ]);
      new Mutator.Expander('Saint',    /\bSt\b\.?/i)
        .exec( row, opts.fields.City );

      if ( row.keys.includes( 'SITE_SERVICE_TYPE' ) )
        SST.exec( row, [ 'SITE_SERVICE_TYPE' ]);

      // Push cleaned row to rows array.
      ROWS.push( row );

    }
    callback( null, ROWS );

  }
};

module.exports = dataClean;